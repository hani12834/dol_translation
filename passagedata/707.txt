<<effects>>

You follow the path west. Wild grasses and other plants intrude on it, sometimes swallowing it all, but it's not hard to find again. It always hugs the cliff.
<br><br>
You see the town in the distance.
<br><br>

<<link [[Next|Coast Path]]>><</link>>
<br>