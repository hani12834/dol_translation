<<set $outside to 0>><<effects>>

Avery pulls up outside the mansion proper, an opulent building even by the standards of this part of town. Another servant opens the door. Avery throws them the keys as <<he>> loops around the front of the vehicle.
<br><br>

<<if $pronoun is "m">>
	<<He>> holds out <<his>> arm.
	<br><br>

	<<link [[Take it|Avery Party Arm]]>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
	<br>
	<<link [[Refuse|Avery Party Arm Refuse]]>><<def 1>><<npcincr Avery rage 5>><<npcincr Avery love -1>><<set $endear -= 5>><</link>><<garage>><<llove>><<lendear>>
	<br>
<<else>>
	<<He>> hooks <<his>> arm around yours, and tries to lead you toward the door.
	<br><br>

	<<link [[Allow|Avery Party Arm]]>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
	<br>
	<<link [[Push away|Avery Party Arm Refuse]]>><<def 1>><<npcincr Avery rage 5>><<npcincr Avery love -1>><<set $endear -= 5>><</link>><<garage>><<llove>><<lendear>>
	<br>
<</if>>
<br><br>