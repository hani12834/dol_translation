<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">><<person1>>
<<if $phase is 0>>
	Uncertain, but fearing that the <<person>> may have been kidnapped or abused, you approach and gently extract the gag. <<He>> retches and coughs.
	<br>
	"What are you doing in my house?!" <<his>> voice is shrill. "Get out! Get the fuck out!"
	<br>
	Too much noise. You start to replace the gag. <<He>> struggles, trying to turn <<his>> head away, even while bound at the neck.
	<br>
	"No!" <<He>> tries to scream, but you manage to shove the 'gag' back into place. There is a clip to lock it in. Leaving <<him>> chained to the bed and quiet works nicely for your skulduggerous intents.
	<br><br>
<<elseif $phase is 1>>
	This looks like S&M play. <<His>> partner ties <<him>> up and abandons <<him>> for an hour or so in this humiliating state. Why interrupt their play? In any case, having <<him>> conveniently locked in place and quiet works nicely for your skulduggerous needs.
	<br><br>
<<elseif $phase is 2>>
	You rush over to help the poor victim, quickly easing the cruel dildo-gag out from <<his>> throat.
	<br>
	"What are you doing in my house?!" <<his>> voice is shrill. "Get out! Get the fuck out!"
	<br>
	You stare at <<him>> in shock.
	<br>
	"Stop looking at me! Get out of my house?! Are you..? Just fuck off! Right now!"
	<br>
	Looking at the open cupboard, you see whips and sex toys. Pictures of people tied up and spanked. You'd heard people do this kind of thing, but are shocked to come face to face with it.
	<<gawareness>><<awareness 3>>
	<br>
	But if this is what people like...
	<br><br>
	You try to push the dildo-gag back into <<his>> throat but <<he>> resists, screaming obscenities at you.
	<br>
<</if>>
Looking around the room, you can see a number of valuables including BDSM gear and leather, erotic art, jewellery, electronics and some very high-end sex-toys.
<br><br>

<<link [[Rob them|Danube House S&M Steal]]>><<crimeup 120>><</link>><<crime>>
<br>
<<link [[Leave|Danube Street]]>><<endevent>><</link>>
<br>