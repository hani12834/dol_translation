<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $rescue is 1 and $alarm is 1>>
	<<if $NPCList[0].stance is "top">>
		<<set $rescued += 1>>
		<<clothesontowel>>
		<<endcombat>>
		<<generate1>><<person1>>A caretaker emerges from the outbuilding to see what the fuss is about. <<He>> stops in <<his>> tracks when <<he>> sees the dog humping you, mouth agape. <<He>> breaks free from <<his>> stupor and drags the beast off the top of you. <<He>> seems almost as embarrassed as you are, and says nothing as <<he>> locks the dog in its kennel and returns to the outbuilding. <<tearful>> you to return to the classroom.
		<<famebestiality 10>>
		<br><br>
		<<link [[Next|English Lesson]]>><<endevent>><</link>>
		<br>
	<<else>>
		<<set $rescued += 1>>
		<<clothesontowel>>
		<<endcombat>>
		<<generate1>><<person1>>A caretaker emerges from the outbuilding to see what the fuss is about. "Bad dog! Bad!" The beast backs off, ears and tail drooping. "I'm so sorry, it's just a bit too friendly," <<he>> says as <<he>> returns the dog to its kennel. "No harm done though, right?" <<He>> heads back to the outbuilding. <<tearful>> you to return to the classroom.
		<br><br>
		<<link [[Next|English Lesson]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	The beast grunts in satisfaction, and leaves you lying on the grass. <<tearful>> you dust yourself off and head back to the classroom.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|English Lesson]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The beast yelps and flees, its tail between its legs. <<tearful>> you dust yourself off and head back to the classroom.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|English Lesson]]>><</link>>
	<br>
<</if>>