<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<pass 5>>
You arrive at your destination, a stately manor on Danube Street. You knock on the door and a <<generate1>><<generate2>><<person2>><<person>> answers. "Ah, my <<person1>><<if $pronoun is "m">>lord<<else>>lady<</if>> has been expecting you," <<person2>><<he>> says. "Please come in."
<br><br>

You enter a lavish antechamber as a <<person1>><<person>> descends a staircase. <<He>> looks at the envelope. "Good, we've been expecting you," <<he>> says as you hand it to <<himstop>> <<He>> gestures at the <<person2>><<person>> as <<he>> produces a small knife from a pocket. "Pay the <<girlstop>>"
<br><br>

<<if $phase is 1>>

<<He>> hands you the £5 you were promised, and wordlessly guides you outside.
<br><br>

<<set $money += 500>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

<<elseif $phase is 2>>
<<set $skulduggerydifficulty to 100>><<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>>

	<<He>> hands you the £5 you were promised, and wordlessly guides you outside.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<set $money += 500>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

	<<else>>

	<<He>> starts to hand you the money, but the <<person1>><<person>> <<person2>>interrupts <<himstop>> "This envelope has been opened," <<person1>><<he>> says, staring at you. "It's good I don't think you could read the contents, or you'd be in real trouble." <<He>> nods at the <<person2>><<personcomma>> who shoves you outside without paying you.
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

	<</if>>
<<elseif $phase gte 3>>
	<<if $phase is 3>>
	<<set $skulduggerydifficulty to 300>>
	<<elseif $phase is 4>>
	<<set $skulduggerydifficulty to 500>>
	<<elseif $phase is 5>>
	<<set $skulduggerydifficulty to 700>>
	<</if>>
<<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>>

	<<He>> hands you the £5 you were promised, and wordlessly guides you outside.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>
<<set $money += 500>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

	<<else>>

	<<He>> starts to hand you the money, but the <<person1>><<person>> <<person2>>interrupts <<himstop>> "This envelope has been opened," <<person1>><<he>> says, staring at you. "And something taken. I think you need a lesson in manners." <<He>> nods at the <<person2>><<personcomma>> who moves between you and the door.

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

	<br><br>

	The two of them start grabbing your clothing. "Now, where have you hidden it?"
	<br><br>

<<link [[Next|Danube Delivery Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

	<</if>>
<</if>>