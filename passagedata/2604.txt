<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<famerape 10>>
	"I got a bit carried away there." You hear the <<person>> mutter as <<he>> heads back to <<his>> seat.
	<br><br>
	<<exposure>>
	<<if $exposed gte 1>>
		Winter returns to the class, and sees you in your undressed state. The teacher conceals your body with a towel while freeing you from the pillory. "Who's responsible for this?" Everyone looks at the <<personcomma>> who shrinks lower into their seat. "Stay behind after class." <<tearful>> you walk back to your desk, and ease yourself down.
	<<else>>
		<<endevent>><<npc Winter>><<person1>>Winter returns to the class. "Sorry, but we won't have time for this now," <<he>> says as <<he>> releases you from the pillory. <<He>> sounds genuinely disappointed. <<tearful>> you walk back to your desk, and ease yourself down.
	<</if>>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<elseif $enemyhealth lte 0>>
The <<person>> recoils in pain, and the class turn against <<himcomma>> laughing at <<his>> inability to properly molest a bound <<girlstop>> <<He>> returns to <<his>> seat, looking diminished.
<br><br>

<<exposure>>
	<<if $exposed gte 1>>
		Winter returns to the class, and sees you in your undressed state. The teacher conceals your body with a towel while freeing you from the pillory. "Who's responsible for this?" Everyone looks at the <<personcomma>> who shrinks lower into their seat. "Stay behind after class." <<tearful>> you walk back to your desk, and ease yourself down.
	<<else>>
		Winter returns to the class. "Sorry, but we won't have time for this now," <<he>> says as <<he>> releases you from the pillory. <<He>> sounds genuinely disappointed. <<tearful>> you walk back to your desk, and ease yourself down.
	<</if>>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>
<<set $rescued += 1>>
	Footsteps can be heard rapidly approaching the classroom. The <<person>> rushes back to <<his>> desk. Winter arrives, looking angry. "I can't leave you alone for five minutes, can I?"
	<br><br>
	<<exposure>>
	<<if $exposed gte 1>>
		Winter conceals your body with a towel while freeing you from the pillory. "Who's responsible for this?" Everyone looks at the <<personcomma>> who shrinks lower into their seat. "Stay behind after class." <<tearful>> you walk back to your desk, and ease yourself down.
	<<else>>
		<<endevent>><<npc Winter>><<person1>>"Sorry, but we won't have time for this now." Winter says as <<he>> releases you from the pillory. <<He>> sounds genuinely disappointed. <<tearful>> you walk back to your desk, and ease yourself down.
	<</if>>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<<else>>
	Footsteps can be heard approaching the classroom. The <<person>> rushes back to <<his>> desk. The door opens to reveal Winter,
	<br><br>
	<<exposure>>
	<<if $exposed gte 1>>
		who sees you in your undressed state. The teacher conceals your body with a towel while freeing you from the pillory. "Who's responsible for this?" Everyone looks at the <<personcomma>> who shrinks lower into their seat. "Stay behind after class." <<tearful>> you walk back to your desk, and ease yourself down.
	<<else>>
		<<endevent>><<npc Winter>><<person1>>"Sorry, but we won't have time for this now," <<he>> says as <<he>> releases you from the pillory. <<He>> sounds genuinely disappointed. <<tearful>> you walk back to your desk, and ease yourself down.
	<</if>>
	<<clothesontowel>>
	<<endcombat>>
	<<link [[Next|History Lesson]]>><</link>>
<</if>>
<<unbind>>