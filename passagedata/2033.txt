<<effects>>

<<if $submissive gte 1150>>
	"Thank you for offering," you say. "But I've just finished."
<<elseif $submissive lte 850>>
	"You're a tad too late," you say. "Just finished."
<<else>>
	"Thank you for offering," you say. "But I've just finished."
<</if>>
<br><br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 40>>
	Robin nods, and walks back towards the orphanage.
<<else>>
	Robin nods. "Okay. I'll be inside if you need me."
<</if>>
<br><br>

<<link [[Next|Garden Flowers]]>><</link>>
<br>