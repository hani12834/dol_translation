<<effects>>

<<if $livestock_dig gte 25 or $livestock_dig gte 24 and $physiquesize lte 12000 or $livestock_dig gte 23 and $physiquesize lte 10000 or $livestock_dig gte 22 and $physiquesize lte 6000>>
	You crawl inside your tunnel, and get to digging. You pull free a large stone, and earth collapses on your head. You brush it aside, and glimpse daylight above.
	<br><br>

	You peek your head out. You're on the other side of the fence. You could escape, though you'll still be in the middle of the countryside, and Remy will soon notice your absence. It could be dangerous.

<<elseif $livestock_dig gte 20>>
	You crawl inside your tunnel, and get to digging. You think the far end may be beneath the other side of the fence, and start to curve upwards.
<<elseif $livestock_dig gte 12>>
	The hole is more like a tunnel now. You drop inside and get to digging, emerging to scatter earth amongst the grass. You're careful not to be seen.
<<elseif $livestock_dig gte 6>>
	You kneel in the grass and lean into the hole, now deep enough to fit half your body. You remove more earth, curving it toward the fence.
<<elseif $livestock_dig gte 1>>
	You crouch in the grass and get to work. You hope it looks like you're just grazing. You deepen and widen the hole.
<<else>>
	Farmhands sometimes patrol the perimeter, so you choose a spot a little way away from the fence where the tall grass should conceal your mischief. You start digging. The ground is hard. It's tiring and slow with only your hands as digging implements, but bit by bit you move the earth aside.
	<br><br>

	An hour passes. You've made a hole. It's too thin and far too shallow, but with effort this will work.

<</if>>
<br><br>

<<if $livestock_dig gte 25>>
	<<link [[Escape|Livestock Dig Escape]]>><</link>>
	<br>
	<<link [[Wait|Livestock Field]]>><</link>>
	<br>
<<elseif $livestock_dig gte 24 and $physiquesize lte 12000 or $livestock_dig gte 23 and $physiquesize lte 10000 or $livestock_dig gte 22 and $physiquesize lte 6000>>
	<<link [[Escape|Livestock Dig Escape]]>><</link>><<small_text>>
	<br>
	<<link [[Wait|Livestock Field]]>><</link>>
	<br>
<<else>>
	<<if $rng gte 51>>
		<<livestock_overhear>>

		<<link [[Next|Livestock Field]]>><</link>>
		<br>
	<<elseif $rng gte 31>>
		<<generate1>><<person1>>
		"What you up to down there?" says a voice from the fence.
		<br><br>

		<<if $cow gte 6>>
			<<link [[Moo|Livestock Field Dig Moo]]>><<livestock_obey 1>><<transform cow 1>><</link>><<gobey>>
			<br>
		<<else>>
			<<link [[Pretend to moo|Livestock Field Dig Moo]]>><<livestock_obey 1>><<transform cow 1>><</link>><<gobey>>
			<br>
		<</if>>
		<<link [[Distract|Livestock Field Dig Distract]]>><</link>><<exhibitionist1>>
		<br>
		<<link [[Insult|Livestock Field Dig Insult]]>><<livestock_obey -1>><<stress -6>><</link>><<lobey>><<lstress>>
		<br>
		<<link [[Just keep digging|Livestock Field Dig Keep]]>><<livestock_obey -1>><</link>><<lobey>>
		<br>
	<<else>>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>
	<</if>>
<</if>>