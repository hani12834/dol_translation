<<effects>>

<<if $worn.face.type.includes("gag")>>
	You try to protest, but the $worn.face.name renders the words unintelligible. You shake your head instead.
<<elseif $submissive gte 1150>>
	"I-I don't want to," you say.
<<elseif $submissive lte 850>>
	"Fuck off, pervert," you say. "Let me go at once."
<<else>>
	"No," you say. "You need to let me go."
<</if>>
<br><br>

<<generate2>><<generate3>>

Remy calls for help, and two farmhands rush to <<his>> side. "If you're going to be a stubborn beast," <<he>> says, unfurling <<his>> whip. "Then we do this the hard way."
<br><br>

<<link [[Next|Livestock Fight]]>><<set $fightstart to 1>><</link>>
<br>