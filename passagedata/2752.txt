<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>
<<npc Leighton>><<person1>><<set $rng to random(1, 100)>>
<<set $detentionattended to 1>>

<<if $detention lte 10>>
	<<if $rng gte 50>>
		You enter Leighton's office. <<He>> takes <<his>> seat in front of <<his>> desk and beckons you to sit at a small desk in the corner.
		<br><br>
		"It's only a minor infringement, I won't keep you long." <<He>> turns to <<his>> computer and seemingly forgets about you.
		<br><br>
		Ten minutes pass, and <<he>> looks at the clock on the wall. "That should do it." <<He>> gestures for you to leave.
		<<pass 10>>
		<br><br>
	<<else>>
		You enter Leighton's office. <<He>> sits at <<his>> desk and looks up at you.
		<<if $delinquency gte 400>>
			"Everyday the same faces," <<he>> sighs. "So what now?"
		<<else>>
			"You? What did you do to end up here?"
		<</if>>
		<br><br>
		<<He>> looks at <<his>> computer screen for a moment. Clicks a few times.
		<br><br>
		<<if $delinquency gte 400>>"Looks like only a minor infringement today," <<he>> stares you down. "How kind of you. Well...<<else>>"Looks like it's only a minor infringement," <<he>> says. "<</if>>Might as well make yourself useful. Deliver these notes to staff and bring back any replies. Shouldn't take 10 minutes."
		<br><br>
		Sure enough, 10 minutes later you're back in Leighton's office with a couple of replies. Leighton nods and gestures for you to leave.
		<<pass 10>>
		<br><br>
	<</if>>

	<<link [[Next|Hallways]]>><<endevent>><</link>>

<<elseif $detention lte 20>>
	You enter Leighton's office. <<He>> takes <<his>> seat in front of <<his>> desk, then points at the whiteboard opposite <<himstop>> "Write 'I will obey the rules,' repeatedly. Don't stop until I say stop."
	<br><br>
	<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		You turn to show Leighton your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. <<His>> eyes flash. For a moment you think you see lust. "How did that happen?" <<he>> asks, walking behind you. "Actually, never mind. I don't want to know."
		<br><br>
		<span class = "green">You feel your limbs come loose.</span><<unbind>>
		<br><br>
		"Now get on with it," <<he>> says.
		<br><br>
	<</if>>

	<<link [[Work diligently (0:20)|School Detention Lines]]>><<set $phase to 0>><<tiredness 4>><<pass 20>><</link>><<gtiredness>>
	<br>
	<<link [[Work slowly (0:40)|School Detention Lines]]>><<set $phase to 1>><<pass 40>><</link>>

<<elseif $detention lte 30>>
	<<generates2>>You enter Leighton's office. A <<person2>><<person>> stands sheepishly in the corner. "Two of you today." <<person1>>Leighton says, taking the seat at <<his>> desk. <<He>> points at the whiteboard opposite <<himstop>> "Write 'I will obey the rules,' repeatedly. Both of you. Don't stop until I say stop."
	<br><br>
	<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		You turn to show Leighton your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. <<His>> eyes flash. For a moment you think you see lust. "How did that happen?" <<he>> asks, walking behind you. "Actually, never mind. I don't want to know."
		<br>
		<span class = "green">You feel your limbs come loose.</span><<unbind>>
		<br>
		"Now get on with it," <<he>> says.
		<br><br>
	<</if>>
	Shortly after you start writing on the board, you feel someone groping your butt. The <<person2>><<person>> is clearly a pervert.
	<br><br>

	<<link [[Endure it (0:30)|School Detention Grope]]>><<set $phase to 0>><<pass 30>><<trauma 3>><<stress 3>><<arousal 300>><</link>><<gtrauma>><<gstress>><<garousal>>
	<br>
	<<link [[Tell Leighton|School Detention Grope]]>><<set $phase to 1>><</link>>
	<br>

<<elseif $detention lte 40>>
	You enter Leighton's office. "I have something special I need you to do today." <<He>> points out the window, at <<his>> car. "It needs a wash. I left everything you'll need out there, I'll keep an eye on you from here. You'll want to strip down to your underwear for this job, no sense getting your clothes wet and dirty."
	<br><br>

	You look at <<him>> with incredulity. <<He>> chuckles. "Don't worry, it's an enclosed area so no one else will see you. Now, hop to it."
	<br><br>
	<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		You turn to show Leighton your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. <<His>> eyes flash. For a moment you think you see lust. "How did that happen?" <<he>> asks, walking behind you. "Actually, never mind. I don't want to know."
		<br>
		<span class = "green">You feel your limbs come loose.</span><<unbind>>
		<br><br>
		"You'll need your hands to clean my car." <<he>> says.
		<br><br>
		<<if $worn.under_lower.type.includes("naked")>>
			"There's something else too," you say.
		<</if>>
	<</if>>
	<<if $worn.under_lower.type.includes("naked")>>
		You avoid looking <<him>> in the eyes. "I'm n-not wearing any underwear." You say.
		<br><br>
		You think you catch a smile start to form on <<his>> face, but it's gone so fast you may have imagined it. "I'll overlook that rule violation, but you still need to wash my car. Maybe next time you'll dress appropriately for school.
		<br><br>
	<</if>>

	<<link [[Strip and wash the car (0:40)|School Detention Car]]>><<pass 40>><<trauma 4>><<stress 4>><<arousal 400>><<upperstrip>><<lowerstrip>><</link>><<gtrauma>><<gstress>><<garousal>>
	<br>
	<<link [[Refuse|School Detention Refuse]]>><<set $detention += 40>><<detention 6>><</link>><<gdelinquency>>
	<br>

<<elseif $detention lte 50>>
	You enter Leighton's office. <<He>> sits at <<his>> desk and beckons you over. When you're close, <<he>> grabs you by the arms and bends you over <<his>> knee.
	<br><br>

	<<link [[Next|School Detention Spank]]>><<set $phase to 2>><<set $molestationstart to 1>><</link>>
	<br>

<<elseif $detention lte 60>>
	You enter Leighton's office. <<He>> sits at <<his>> desk and beckons you over. When you're close, <<he>> grabs you by the arms and bends you over <<his>> knee.
	<br><br>

	<<link [[Next|School Detention Spank]]>><<set $phase to 1>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>
	You enter Leighton's office.
	"Lock the door," <<he>> demands. "You're in big trouble."
	You comply.
	"Give me your clothes. All of them. Now. I'm searching them for drugs. And yes, I'm serious."
	<br><br>

	<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		You roll your eyes and turn to show <<him>> your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>.

		<<His>> eyes flash. For a moment you think you see lust. "How did you..? Never mind," <<he>> walks over to you. "I really don't want to know."
		<br><br>

		You feel your limbs come loose.<<unbind>>
		<br><br>
		"Continue," <<he>> says. "Your clothes."
		<br><br>
	<</if>>
	You remove your $worn.upper.name and your $worn.lower.name and hand them to <<himstop>> <<He>> makes a show of searching through them.
	<br><br>
	<<upperstrip>><<lowerstrip>>
	<<if !$worn.under_lower.type.includes("naked")>>
		"I said everything," <<he>> states, pointing at your $worn.under_lower.name.
		"Hurry up."
		<br>
		Hiding any feelings, you strip your<<if !$worn.under_upper.type.includes("naked")>> $worn.under_lower.name and $worn.under_upper.name handing <<underlowerit>> to <<himstop>><<else>> $worn.under_lower.name and hand <<underlowerit>> to <<himstop>><</if>> <<He>> examines <<underlowerit>> closely.
		"Drugs," <<he>> says. "I smell drugs."
		<<He>> holds your $worn.under_lower.name to <<his>> nose and inhales deeply several times. "I'm confiscating these for drug testing."
		<br><br>
		<<underlowerruined>><<underupperruined>><<set $phase to 4>>
	<<else>>
		"No underwear?" <<he>> comments. "That's against the rules,
		young<<if $player.gender is "m">> man.<<elseif $player.gender is "h" and $player.gender_appearance is "m">>... man?<<elseif $player.gender is "h" and $player.gender_appearance is "f">>... lady?<<else>> lady.<</if>>"
		<br><br>
	<</if>>
	<<if $worn.genitals.type.includes("chastity")>>
		<<He>> does a double-take. "Oh, you're wearing a $worn.genitals.name. Okay, let me see that." <<He>> crouches by you, closely examining and probing around your $worn.genitals.name with <<his>> fingers. "Doesn't feel like you're hiding anything," <<he>> says.
		<br><br>
	<</if>>
	<<He>> sits back at <<his>> desk and beckons you over. When you're close, <<he>> grabs you by the arms and bends you over <<his>> knee.
	<br><br>

	<<link [[Next|School Detention Spank]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>
<<set $detention to 0>>