<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
"Please," the <<person>> sobs. The orderlies pay no heed as they tear <<his>> clothing off <<his>> body and shove <<him>> to the window. One of them knocks on it, until everyone outside is watching. You hear whistling through the glass.
<br><br>
"<<He>> doesn't have to suffer so," The Doctor whispers to you. "<<He>> chooses to. <<Hes>> in no danger. But you've done your part. You can go."
<br><br>
You leave the room.
<br><br>
<<link [[Next|Asylum]]>><</link>>
<br>