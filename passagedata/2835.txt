<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<set $pain += 1>>
The <<person2>><<person>> undoes the buttons on <<his>> shirt, and holds it open so that no one but Leighton and <<person1>><<his>>
camera can see. The headteacher takes a picture. "Good," <<he>> says, moving down the line. Most students look uncomfortable as it
approaches their turn, and relieved following. Even boys who are used to being seen topless while swimming fidget and blush under Leighton's gaze.
<br><br>

<<He>> soon arrives beside you. You follow the <<person2>><<persons>> lead and hold open your shirt so that only Leighton can see.
You feel your nipples stand on end. <<person1>><<He>> takes a picture.

<<if $breastsize gte 8>>
	<<if $player.gender_appearance is "m">>
		<<if $genderknown.includes("Leighton") and $player.gender isnot "m">>
			"You might be the biggest in the class," <<he>> smirks. "Very impressive for a 'boy'."
			<<insecurity "breasts_big" 20>><<ginsecurity "breasts_big">><<fameexhibitionism 30>>
		<<else>>
			"You might be the biggest in the class," <<he>> says. "Very impressive for a boy."
			<<insecurity "breasts_big" 20>><<ginsecurity "breasts_big">><<fameexhibitionism 30>>
		<</if>>
	<<else>>
		"You might be the biggest in the class," <<he>> says.
		<<insecurity "breasts_big" 20>><<ginsecurity "breasts_big">><<fameexhibitionism 30>>
	<</if>>
<<elseif $breastsize gte 6>>
	<<if $player.gender_appearance is "m">>
		"Nice pair," <<he>> says.
		<<if $player.gender is "m">><<insecurity "breasts_big" 20>><<ginsecurity "breasts_big">><</if>><<fameexhibitionism 30>>
	<<else>>
		"Nice pair," <<he>> says.
	<<if $player.gender is "m">><<insecurity "breasts_big" 20>><<ginsecurity "breasts_big">><</if>><<fameexhibitionism 30>><</if>>
<<elseif $breastsize gte 1>>
	<<if $player.gender_appearance is "m">>
		"Cute pair," <<he>> says.
		<<insecurity "breasts_small" 20>><<ginsecurity "breasts_small">><<fameexhibitionism 30>>
	<<else>>
		"Cute pair," <<he>> says.
		<<insecurity "breasts_small" 20>><<ginsecurity "breasts_small">><<fameexhibitionism 30>>
	<</if>>
<<else>>
	<<if $player.gender_appearance is "m">>
		<<fameexhibitionism 10>>
	<<else>>
		"Don't be ashamed about being flat," <<he>> says while leering at you.
		<<insecurity "breasts_tiny" 20>><<ginsecurity "breasts_tiny">><<fameexhibitionism 30>>
	<</if>>
<</if>>
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<link [[Next|Breast Inspection End]]>><</link>>
<br>