<<effects>><<set $location to "sewers">>
You've read about the old sewers beneath the storm drains. They haven't been used in over a century, and are said to be dangerous. There are rumours of monsters and treasure.
<br><br>
<<link [[Descend|Sewers Descend]]>><</link>>
<br>
<<if $bus is "residentialdrain">>
	<<link [[Leave|Residential Drain]]>><</link>>
	<br>
<<elseif $bus is "commercialdrain">>
	<<link [[Leave|Commercial Drain]]>><</link>>
	<br>
<<else>>
	<<link [[Leave|Industrial Drain]]>><</link>>
	<br>
<</if>>