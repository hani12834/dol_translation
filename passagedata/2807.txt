<<effects>>

<<if $worn.lower.skirt is 1>>
You lift the hem of your $worn.lower.name, just short of showing off your <<undiesstop>>
	<<if $submissive gte 1150>>
	"You think I'm cute?," you ask.
	<<elseif $submissive lte 850>>
	"You're all talk," you say.
	<<else>>
	"You look okay too," you say.
	<</if>>

<<else>>
You lean against a locker.
	<<if $submissive gte 1150>>
	"You think I'm cute?," you ask.
	<<elseif $submissive lte 850>>
	"You're all talk," you say. "Wanna prove me wrong?"
	<<else>>
	"You look okay too," you say.
	<</if>>
<</if>>
<<promiscuity1>>

<<if $rng gte 51>>
The <<person>> tries to speak, but stutters, taken aback by your forwardness. <<He>> turns and scurries away.
<br><br>

<<link [[Next|Hallways]]>><<endevent>><<set $eventskip to 1>><</link>>
<<else>>
The <<person>> leers at your body, and takes a step closer. "Let's go somewhere together," <<he>> says. <<His>> intentions are clear.
<br><br>

<<link [[Refuse|Hallways Cupboard Refuse]]>><</link>>
<br>
	<<if $promiscuity gte 15>>
	<<link [[Accept|Hallways Cupboard Sex]]>><<set $sexstart to 1>><<set $phase to 0>><</link>><<promiscuous2>>
	<br>
	<</if>>
<</if>>