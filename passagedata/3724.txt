<<set $outside to 0>><<set $location to "town">><<effects>>
You convince the <<monks>> to follow you through the alleyways. The garden fence proves troublesome, but the <<monks>> are determined. They carry the smaller goods over first, then work to haul the bigger appliances. They're all much stronger than their habits and meek demeanours would suggest.
<br><br>
Once inside the <<monks>> follow you through the main hall and up the stairs. A couple of orphans stop and gawk. The lead <<monk>> smiles at them, and places a finger against <<his>> lips.
<br><br>
The ladder to the loft proves even more troublesome than the fence, but the <<monks>> aren't thwarted.
<br><br>
<<link [[Next|River Loft 4]]>><</link>>
<br>