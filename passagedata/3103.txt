<<set $outside to 0>><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "under_lower">>
	<<else>>
		<<shopbuy "under_lower" "reset">>
	<</if>>
<<else>>
	<<shopbuy "under_lower" "reset">>
<</if>>
<<clothingShop "under_lower">>
<br>

<<link [[Back to shop|Clothing Shop]]>><<unset $clothes_choice>><</link>>