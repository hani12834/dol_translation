<<widget "kylargag">><<nobr>>
<<set $mouthuse to "gagged">>
<<facewear 4>>
<<if $pronoun is "m">><<He>> pulls a knife from <<his>> pocket.<<else>><<He>> pulls a knife from beneath <<his>> skirt.<</if>> <<His>> smile doesn't falter. "You're just grouchy because we've been apart too long. Here." <<He>> cuts a piece from a roll of tape at <<his>> side, and sticks it over your mouth.
<br><br>

"There," <<he>> says. "Are you feeling better?" You can only mumble. <<He>> scrambles over to the table, and pulls a laptop out from beneath it. You can't see the screen, but a moment later a voice emerges. Your voice.
<br><br>

"I am better," it says. Each word has a different tone, recorded in different places and stitched together.
<br><br>

Kylar beams at you. "I knew it," <<he>> says.
<br><br>

<<He>> clicks again. "I love you," says your voice.
<br><br>

Kylar clutches <<his>> cheeks. "I know. You tell me every day."
<br><br>

<</nobr>><</widget>>

<<widget "kylarangry">><<nobr>>
	<<if $submissive gte 1150>>
	"You're being horrible," you say. "Let me go."
	<<elseif $submissive lte 850>>
	"You creep," you say. "Let me go this instant."
	<<else>>
	"You're awful," you say. "Let me go."
	<</if>>
	<br><br>
<</nobr>><</widget>>

<<widget "kylaroptions">><<nobr>>

<<kylaroptionstext>>

<<if $bus is "park" and $daystate isnot "day">>
	<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
	<<He>> looks at <<his>> watch. "I need to go," <<he>> says. "They're waiting for me." <<He>> keeps looking over <<his>> shoulder as <<he>> walks.
	<br><br>
	<<else>>
	<<He>> looks at <<his>> watch, mumbles something, then walks away.
	<br><br>
	<</if>>
<<kylaroptionsleave>>
<<elseif $location is "school" and $hour isnot 12>>
	<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
	<<He>> looks at <<his>> watch. "I need to go," <<he>> says. "I'll be late for my lesson." <<He>> keeps looking over <<his>> shoulder as <<he>> walks.
	<br><br>
	<<else>>
	<<He>> looks at <<his>> watch, mumbles something, then walks away.
	<br><br>
	<</if>>
<<kylaroptionsleave>>
<<elseif $bus is "starfish" and $daystate isnot "day">>
	<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
	<<He>> looks at <<his>> watch. "I need to go," <<he>> says. "They're waiting for me." <<He>> keeps looking over <<his>> shoulder as <<he>> walks.
	<br><br>
	<<else>>
	<<He>> looks at <<his>> watch, mumbles something, then walks away.
	<br><br>
	<</if>>
<<kylaroptionsleave>>
<<else>>

<<link [[Chat (0:15)|Kylar Chat]]>><<pass 15>><<npcincr Kylar love 1>><<npcincr Kylar rage -1>><<stress -2>><</link>><<lstress>><<glove>><<lsuspicion>>
<br>
<<link [[Tease (0:15)|Kylar Tease]]>><<pass 15>><<npcincr Kylar love 1>><<npcincr Kylar lust 1>><<stress -2>><</link>><<promiscuous1>><<lstress>><<glove>><<glust>>
<br>
<<if $bus is "starfish">>
<<link [[Play a game (0:15)|Kylar Game]]>><<pass 15>><<stress -2>><<npcincr Kylar love 1>><</link>><<glove>><<lstress>>
<br>
<</if>>
<<if $loft_known is 1 and $loft_kylar isnot 1 and $NPCName[$NPCNameList.indexOf("Kylar")].love gte 90 and $loft_option_block isnot 1>>
<<link [[Tell Kylar about the orphanage loft|Kylar Loft]]>><</link>>
<br>
<</if>>

	<<if random(1, 100) gte 61 and $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50 and $kylarstop isnot 1>>
	<<link [[Leave|Kylar Stop]]>><<set $kylarstop to 1>><</link>>
	<br>
	<<else>>
	<<kylaroptionsleave>>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "kylaroptionsleave">><<nobr>>

<<if $kylarstop isnot 1>>
	<<if $bus is "park">>
	<<link [[Leave|Park]]>><<endevent>><</link>>
	<br>
	<<elseif $bus is "schoollibrary">>
	<<link [[Leave|School Library]]>><<endevent>><</link>>
	<br>
	<<elseif $bus is "starfish">>
	<<link [[Leave|Arcade]]>><<endevent>><</link>>
	<<else>>
	<<link [[Leave|School Rear Playground]]>><<endevent>><</link>>
	<br>
	<</if>>
<</if>>
<<set $kylarstop to 0>>
<</nobr>><</widget>>

<<widget "kylaroptionstext">><<nobr>>
<<set $rng to random(1, 100)>>
<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
	<<if $kylarchatintro isnot 1>>
	<<set $kylarchatintro to 1>>
	<<He>> clenches <<his>> fists and lifts <<his>> head, looking you straight in the eye. "H-hey," <<he>> says.
		<<if $kylarhomeinterrupted is 1>>
		"I-I'm sorry about the thing in your bedroom."
			<<if $kylarignored isnot 1>>
			<<He>> gulps. "I just wanted to thank you for helping me." <<He>> looks away.
			<<else>>
			<<He>> gulps. "I just wanted to t-tell you you're very <<if $player.gender_appearance is "m">>handsome<<else>>pretty<</if>>" <<He>> looks away.
			<</if>>
		<<else>>
			<<if $kylarignored isnot 1>>
			"Thank you for helping me." <<He>> looks away.
			<<else>>
			"You're very <<if $player.gender_appearance is "m">>handsome<<else>>pretty<</if>>" <<He>> looks away.
			<</if>>
		<</if>>
	<br><br>

	<<elseif $famesex gte 100 and $rng gte 91 and $kylarfameconfront isnot 1 and $kylarfamestage is undefined>>
	<<set $kylarfameconfront to 1>><<set $kylarfamestage to 0>>
	"I-I found a strange picture of you," <<he>> says. "Someone doctored it to make it look like you were doing lewd things." <<He>> laughs. "Some people have no shame."
	<<npcincr Kylar rage 3>><<gksuspicion>>
	<br><br>
	<<elseif $famesex gte 300 and $rng gte 91 and $kylarfameconfront isnot 1 and $kylarfamestage is 0>>
	<<set $kylarfameconfront to 1>><<set $kylarfamestage to 1>>
	"I-I found another strange picture of you," <<he>> laughs. "Doctored to make it look like you were doing lewd things." <<He>> laughs again. "You must be popular with perverts."
	<<npcincr Kylar rage 10>><<ggksuspicion>>
	<br><br>
	<<elseif $famesex gte 500 and $rng gte 91 and $kylarfameconfront isnot 1 and $kylarfamestage is 1>>
	<<set $kylarfameconfront to 1>>
	"Y-you'll never guess what I found," <<he>> laughs. "More doctored pictures of you doing perverted things." <<His>> laughter reaches a hysterical pitch. "S-Someone spent a lot of time on them. There are so many."<<gggksuspicion>><<npcincr Kylar rage 30>>
	<br><br>
	<<elseif $NPCName[$NPCNameList.indexOf("Kylar")].rage gte 50>>
	"You came to see me," <<he>> says. "Of course you did. Why wouldn't you." <<He>> laughs.
	<br><br>
	<<else>>
	<<He>> smiles at you.
	<br><br>
	<</if>>
<<else>>
<<He>> blushes and looks away.
<br><br>
<</if>>

<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 90 and $spray lt $spraymax and $kylarspray isnot 1>><<set $kylarspray to 1>>
"I made you something," <<he>> says as <<he>> rummages in <<his>> bag. <<He>> hands you a charge for your pepper spray. "I made it myself. It'll keep sluts and perverts away." <<He>> beams.
<<spray 1>><<gspray>>
<br><br>
<</if>>

<</nobr>><</widget>>