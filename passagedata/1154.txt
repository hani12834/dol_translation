<<effects>>

You brace yourself for the blow, but little could prepare you for the ordeal. The whip bites into the unprotected skin of your <<bottomcomma>> sending an unbearable jolt of pain up your body.

<<if $masochism_level gte 4>>
	You cry out, but more in pleasure than pain. The <<person2>><<person>> and <<person3>><<person>> grin as they watch.
	<<gggpain>><<pain 20>>
	<br><br>
	Again and again Remy's whip strikes your skin, each blow pushing you closer to the edge.
	<<if $arousal gte 10000>>
		The last strike is too much. Your body trembles in response.
		<<orgasmpassage>>
		<br><br>

		The <<person2>><<person>> and <<person3>><<person>> look at each other, astonished at your response. If Remy is disappointed however, <<person1>><<he>> doesn't show it.
		<br><br>
	<</if>>
<<elseif $masochism_level gte 1>>
	You cry out, but there's pleasure mixed with the pain. The <<person2>><<person>> and <<person3>><<person>> grin as they watch.
	<<gggpain>><<pain 20>>
	<br><br>
	Again and again Remy's whip strikes your skin, each blow pushing you closer to the edge.
	<<if $arousal gte 10000>>
		The last strike is too much. Your body trembles in response.
		<<orgasmpassage>>
		<br><br>

		The <<person2>><<person>> and <<person3>><<person>> look at each other, astonished at your response. If Remy is disappointed however, <<person1>><<he>> doesn't show it.
		<br><br>
	<</if>>

<<else>>
	You cry out in pain. The <<person2>><<person>> and <<person3>><<person>> grin as they watch. Again and again Remy's whip strikes your skin, each blow eliciting an involuntary cry of pain.
	<<gggpain>><<pain 20>>
	<br><br>
<</if>>

Remy grasps your collar, and leads you back to the barn proper. You're led to the same cell as before. <<He>> holds the leash close to your collar and guides you inside. The door clanks shut.
<br><br>

<<endevent>>
<<link [[Next|Livestock Cell]]>><</link>>
<br>