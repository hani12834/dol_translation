<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<if $rng gte 81 or ($museumAntiques.antiques.antiquebrassstatuette isnot "found" and $museumAntiques.antiques.antiquebrassstatuette isnot "talk" and $museumAntiques.antiques.antiquebrassstatuette isnot "museum")>>
	You reach in, and touch something sharp. You withdraw your hand.
	<<gpain>><<pain 4>><<gstress>><<stress 6>>
	<br><br>
	You reach in again, gingerly this time, and wrap your fingers around something cool and hard. You bring it out into the light. <span class="gold">It's a brass statuette,</span> moulded in the shape of an eagle. You caught your hand on its beak.
	<br><br>
	It looks old.
	<<set $antiquemoney += 120>><<museumAntiqueStatus "antiquebrassstatuette" "found">>
	<br><br>
	<<link [[Next|Temple Quarters]]>><</link>>
	<br>
<<elseif $rng gte 61 or $stone_pendant_found is undefined>>
	You reach in. The hole is deep. You lie on your front and shuffle beneath the bed, allowing you to fit more of your arm down the hole.
	<br><br>
	<<if $stone_pendant_found is undefined>><<set $stone_pendant_found to 1>>
		Your fingers brush against something cold and hard. You pull it out, and find a <span>stone in the shape of an old religious symbol.</span> It's attached to a looped string. You wear it around your neck.<<neckwear 5>>
		<br><br>
	<<elseif random(1, 100) gte 51>>
		Your fingers brush against fabric. You pull out a small pouch. Inside is <span class="gold">£20</span>.
		<<set $money += 2000>>
		<br><br>
	<<else>>
		<span class="red">Something grasps your wrist.</span> You yank your arm away, breaking its grip, and shuffle away from the hole.
		<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
		<br><br>
	<</if>>
	<<link [[Next|Temple Quarters]]>><</link>>
	<br>
<<elseif $rng gte 41>>
	You reach in, but find only dust.
	<br><br>
	<<link [[Next|Temple Quarters]]>><</link>>
	<br>
<<elseif $rng gte 21>>
	You reach in, and touch something sharp. You withdraw your hand.
	<<gpain>><<pain 4>><<gstress>><<stress 6>>
	<br><br>
	You reach in again, gingerly this time, and wrap your fingers around something cool and hard. You bring it out into the light.
	<br><br>
	It's just a piece of broken grey stone, with a sharp edge pointing up.
	<br><br>
	<<link [[Next|Temple Quarters]]>><</link>>
	<br>
<<else>>
	You reach in, and touch something soft. You try to grasp it, but it squirms into life and wriggle away.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>
	<<link [[Next|Temple Quarters]]>><</link>>
<</if>>