<<effects>>

<<set _temp_minute to (60 - $minute)>>
<<pass _temp_minute>>

You wait outside the changing rooms, until the sound of lockers opening and closing fills the room. They become more rapid and panicked. It isn't long before you hear murmurs.
<br><br>

<<link [[Peek on the boys|School Changing Swap Boys]]>><</link>>
<br>
<<link [[Peek on the girls|School Changing Swap Girls]]>><</link>>
<br>