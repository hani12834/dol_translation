<<effects>>

<<if $phase is 1>>
	<<npc Alex>><<person1>>
	You approach Alex.
	<<if $weather is "clear">>
		<<He>> reclines in the shade beneath the boughs of a tree.
	<<elseif $weather is "rain">>
		<<He>> shelters from the rain beneath the boughs of a tree.
	<<else>>
		<<He>> reclines beneath the boughs of a tree.
	<</if>>
	<<He>> flicks up the brim of <<his>> hat when <<he>> hears you approach.
	<<if $exposed gte 1>>
		<<covered>> <<He>> looks away.
	<</if>>
	<br><br>

	<<if $NPCName[$NPCNameList.indexOf("Alex")].love gte 10>>
		<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte 10>>
			"Feel free to take a break," <<he>> says. "You've earned it."
		<<elseif $NPCName[$NPCNameList.indexOf("Alex")].dom lte -10>>
			"I'm glad to see you," <<he>> says. "Would you like to sit with me?"
		<<else>>
			"I was hoping you'd swing by," <<he>> says, gesturing at the ground beside <<him>>.
		<</if>>
	<<elseif $NPCName[$NPCNameList.indexOf("Alex")].love lte -10>>
		<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte 10>>
			"What now?" <<he>> sighs.
		<<elseif $NPCName[$NPCNameList.indexOf("Alex")].dom lte -10>>
			"Now what?" <<he>> asks.
		<<else>>
			"Something the matter?" <<he>> asks.
		<</if>>
	<<else>>
		<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte 10>>
			"Let me know if you need a hand with anything." <<he>> says.
		<<elseif $NPCName[$NPCNameList.indexOf("Alex")].dom lte -10>>
			"A break's fine now and then," <<he>> says.
		<<else>>
			"Feel free to join me," <<he>> says. "A break now and then is fair."
		<</if>>
	<</if>>
	<br><br>
<<else>>
	<<farm_work_update>>
<</if>>

You sit and lean against the tree.

<<if $rng gte 91>>
	Alex chats with you, and talks about <<his>> family. "I've twelve siblings," <<he>> says at one point. "<<if $pronoun is "m">>Mum<<else>>Dad<</if>> doesn't know what to do with us all."
<<elseif $rng gte 81>>
	Alex chats with you, and talks about the farm's pigs. "Hauling all their food over is rough," <<he>> says at one point. "The truffles are worth it though."
<<elseif $rng gte 71>>
	Alex chats with you, and talks about the farm's horses. "Most of their owners are rich townies," <<he>> says. "They need to be kept active and healthy."
<<elseif $rng gte 61>>
	Alex chats with you, and talks about the farm's cattle. "They're big buggers," <<he>> says at one point. "But docile. Wish the other animals were as easy to manage."
<<elseif $rng gte 51>>
	Alex chats with you, and talks about the farm's dogs. "They aren't pets," <<he>> says at one point. "Don't be afraid to keep them in line."
<<elseif $rng gte 41>>
	Alex chats with you, and talks about the fields. "There are nine in total," <<he>> says at one point. "Or were. The forest has reclaimed most of them. It's almost like trees have sprung up over night." <<He>> laughs.
<<elseif $rng gte 31>>
	Alex chats with you, and asks about life in the town. "I don't think I'd like the noise," <<he>> says at one point. "I guess you get used to it."
<<elseif $rng gte 21>>
	Alex chats with you, and asks about school. "Parents taught me," <<he>> says at one point. "Sometimes my older siblings helped."
<<elseif $rng gte 11>>
	Alex chats with you, and talks about the moor. "Don't go there at night," <<he>> warns. "Or at all if you can help it. People have disappeared."
<<else>>
	Alex chats with you, and talks about the sea. "You don't need to walk far," <<he>> says. "If you know the paths."
<</if>>
<br><br>

<<set $danger to random(1, 10000)>>
<<if $danger gte (9900 - $allure) and $farm_stage gte 4>>
	<<if $farm_relax_drink is "accept">>
		"Another drink?" <<he>> asks, producing another bottle from the hiding place.
	<<elseif $farm_relax_drink is "refuse">>
		"You sure I can't interest you in a drink?" <<he>> asks, pulling the bottle from its hiding place again.
	<<else>>
		"Fancy a drink?" <<he>> asks out of nowhere. <<He>> pulls a bottle from an alcove beneath a thick root, and offers it to you. "They say you shouldn't drink alone."
	<</if>>
	<br><br>
	
	<<link [[Drink|Farm Rest Drink]]>><<set $drunk += 120>><<stress -6>><<npcincr Alex love 1>><</link>><<glove>><<lstress>>
	<br>
	<<link [[Refuse|Farm Rest Refuse]]>><</link>>
	<br>
<<else>>
	<<farm_relax_end>>
<</if>>