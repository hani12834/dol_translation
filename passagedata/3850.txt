<<effects>>

<<if $submissive gte 1150>>
"B-but," you say. "I want to choose my own food. Please."
<<elseif $submissive lte 850>>
"I can choose my own food," you say. "Thank you very much." You hold out your hand for the menu.
<<else>>
"I want to choose my own food," you say.
<</if>>
<br><br>

You order a sandwich. Avery seems irritated, but doesn't say anything. <<He>> orders a fish soup for <<himselfstop>>
<br><br>

<<He>> leans back and regards the restaurant.

<<if $rng gte 51 and $NPCName[$NPCNameList.indexOf("Avery")].love gte 20>>

Something prods your groin. Then again. It rubs up and down. You glance under the table. Avery has removed <<his>> shoe and is molesting you with <<his>> foot. <<His>> face gives no clue of what <<hes>> up to.
<br><br>

<<link [[Enjoy it|Avery Date Enjoy]]>><<set $endear += 10>><</link>><<promiscuous1>><<gendear>>
<br>
	<<if $promiscuity gte 15>>
	<<link [[Return the favour|Avery Date Return]]>><<npcincr Avery love 1>><</link>><<promiscuous2>><<glove>>
	<br>
	<</if>>
<<link [[Kick away|Avery Date Kick]]>><<set $submissive -= 1>><<npcincr Avery love -1>><<set $endear -= 5>><<npcincr Avery rage 5>><</link>><<garage>><<llove>><<lendear>>
<br>

<<else>>

<<He>> starts talking about all the paperwork <<he>> has to do for work. It's not very interesting.
<br><br>

<<link [[Change the subject|Avery Date Change]]>><<npcincr Avery love -1>><</link>><<llove>>
<br>
<<link [[Smile and nod|Avery Date Nod]]>><<npcincr Avery love 1>><<set $endear += 10>><<tiredness 1>><</link>><<gtiredness>><<glove>><<gendear>>
<br>
<<link [[Fidget|Avery Date Fidget]]>><<stress -6>><<set $endear -= 5>><<npcincr Avery rage 1>><</link>><<garage>><<lendear>><<lstress>>

<</if>>