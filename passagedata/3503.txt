<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You struggle against the <<persons>> grip.
<<if $physique gte random(1, $physiquemax)>>
<<famescrap 1>>
	<<if $devlevel lte 10>>
		<<Hes>> bigger than you, but <span class="green">you manage to twist <<his>> arm and break free.</span>
	<<else>>
		<span class="green">You manage to overpower <<him>> and break free.</span>
	<</if>>
	<br><br>
	<<He>> staggers away from you, livid and clutching <<his>> sore arm. <<He>> barks curses, and a promise of retribution, as <<he>> leaves the club. Security look amused, and more than a little relieved.
	<br><br>

	You return to work. The rest of the shift passes uneventfully.
	<<tipreceive>>
	<br><br>

	<<if !$per_npc.bartend and random(1, 2) is 2>>
	<<saveNPC 0 bartend>>
	<</if>>

	<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>
	<br>
<<else>>

	<<if $devlevel lte 10>>
		<<Hes>> bigger than you, <span class="red">and you're unable to break <<his>> grip.</span>
	<<else>>
		<<Hes>> stronger than you, <span class="red">and you're unable to break free.</span>
	<</if>>
	<<He>> drags you from the club, shutting the escape door behind you.
	<br><br>

	<<if $rng gte 81>>
		<<generate2>><<generate3>><<generate4>>

		"Damn," says an unfamiliar voice. "You got a cute one." A <<person2>><<personcomma>> <<person3>><<person>> and <<person4>><<person>> step from the darkness.
		<br><br>

		The <<person1>><<person>> grasps you from behind.
		<br><br>

		<<link [[Next|Bartending VIP Patron Gang Rape]]>><<set $molestationstart to 1>><</link>>
		<br>

	<<else>>

		The <<person>> grasps you from behind.
		<br><br>

		<<link [[Next|Bartending VIP Patron Rape]]>><<set $molestationstart to 1>><</link>>
		<br>

	<</if>>

<</if>>