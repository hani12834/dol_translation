<<set $outside to 1>><<set $location to "docks">><<effects>>

<<set $docksrobinintro to 1>><<set $robindebtknown to 1>>
<<npc Robin>><<person1>>You search the docks for Robin. You find <<him>> strapped naked to a table on board a yacht. <<His>> body is covered in welts and bruises. <<He>> looks right through you, <<his>> eyes glazed and empty.
<br><br>

<<endevent>>

<<generate1>><<generate2>><<generate3>>There are three <<people>> on board. Two of them are arguing. "You fucking broke the toy already," the <<person1>><<person>> says. "We haven't even cast off yet."
<br><br>

"I was only testing it," the <<person2>><<person>> responds. "How was I to know it would break so easily?"
<br><br>

"You went completely mental with that whip," the <<person1>><<person>> says. "Now look." <<He>> grabs Robins genitals and tugs. It looks very painful, but Robin doesn't make a sound. "It's not gonna be any fun fucking that."
<br><br>

"What do you want me to do? It's not like we can get a refund."
<br><br>

"Ladies," the <<person3>><<person>> says. "We have company." They look at you.
<br><br>

	<<if $submissive gte 1150>>
	"L-let my friend go," you say. "What you're doing is wrong."
	<br><br>
	<<elseif $submissive lte 850>>
	"Let my friend go," you say. "Or else."
	<br><br>
	<<else>>
	"Let my friend go," you say.
	<br><br>
	<</if>>
	<br><br>

They pause for a moment, then burst into laughter. "Sure thing," the <<person1>><<person>> says. "Just get your little butt on here, and we'll let your friend go. If not, piss off. We're about to set sail."
<br><br>

<<link [[Take Robin's place|Docks_Robin Swap]]>><</link>>
<br>
<<link [[Fight|Docks_Robin Fight]]>><<set $fightstart to 1>><</link>>
<br>
<<link [[Leave|Mer Street]]>><<endevent>><</link>>
<br>