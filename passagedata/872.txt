<<effects>>

<<if $submissive gte 1150>>
	"W-We could have been injured much worse," you say.
<<elseif $submissive lte 850>>
	"Do you have a deathwish?" you ask. "We were a bit outnumbered."
<<else>>
	"There were too many," you say. "You should have been smarter."
<</if>>
<br><br>

Alex nods. "I understand if you don't want to work here anymore." <<He>> looks out at the damaged farm. "I've got some cleaning up to do."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>