<<effects>>

You find Winter trying to hook a brass pot onto a wall. <<npc Winter>><<person1>>

<<if $submissive gte 1150>>
	"I-I'd like to help," you say. "With the ducking stool."
<<elseif $submissive lte 850>>
	"It'll fall if you put it there," you say. "I'm here about the ducking stool. I'm in."
<<else>>
	"I've thought about it," you say. "I'd like to help demonstrate the ducking stool."
<</if>>
<br><br>

Winter smiles. "Excellent," <<he>> steades the pot against the wall. "Come. Let me show you what's involved."
<br><br>

You follow Winter through the front door. "The museum will be fine without me for five minutes. This way." <<He>> leads you down a side road, and into the park.
<br><br>

<<link [[Next|Museum Duck Agree 2]]>><</link>>
<br>