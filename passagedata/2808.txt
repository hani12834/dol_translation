<<effects>>

<<if $submissive gte 1150>>
"I'm sorry," You say. "I'm not interested."
<<elseif $submissive lte 850>>
"Nice try," you say. "Piss off."
<<else>>
"You're not that good," you say.
<</if>>
<br><br>

<<if $rng gte 51>>
The <<person>> considers, then turns away from you. <<He>> mutters something about a "tease".
<br><br>

<<link [[Next|Hallways]]>><<endevent>><<set $eventskip to 1>><</link>>
<<else>>
"No you don't," <<he>> sneers. "I'm not someone you can toy around with." <<He>> gives your arm a rough tug.
<<gpain>><<pain 1>><<gstress>><<stress 6>>
<br><br>

<<link [[Refuse again|Hallways Cupboard Refuse 2]]>><<detention 1>><</link>><<gdelinquency>>
<br>
<<link [[Give in|Hallways Cupboard Sex]]>><<set $sexstart to 1>><<set $phase to 1>><</link>><<promiscuous1>>
<br>

<</if>>