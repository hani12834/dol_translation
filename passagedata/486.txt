<<effects>>
You drop to your knees and crawl through the grass, searching for seeding lilies in the shadow of larger plants.
<br><br>
<span class="gold">You can now grow lilies.</span>
<br><br>
<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>