<<effects>>
<<if $timer gte 30>>
	<<set $bus to "residentialdrain">>
<<elseif $timer gte 20>>
	<<set $bus to "commercialdrain">>
<<elseif $timer gte 10>>
	<<set $bus to "industrialdrain">>
<<else>>
	<<set $bus to "drainexit">>
<</if>>
<<if $timer lte 0>>
	The water carries you toward the end of the drain. You try to cling to the side of the tunnel, but to no avail. You are carried into the open air and plummet into the sea below. <<tearful>> you struggle to the surface.
	<br><br>
	<<if $upperoff isnot 0>>
		<<upperruined>>
	<</if>>
	<<if $loweroff isnot 0>>
		<<lowerruined>>
	<</if>>
	<<if $underloweroff isnot 0>>
		<<underlowerruined>>
	<</if>>
	<<if $underupperoff isnot 0>>
		<<underupperruined>>
	<</if>>
	<<clotheson>>
	<<endcombat>>
	<<seacliffseventend>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	Its seed deposited, the beast loses interest in you.
	<br><br>
	<<tearful>> you gather yourself.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Drain Water]]>><<set $eventskip to 1>><</link>>
<<else>>
	It whimpers and flees into the darkness.
	<br><br>
	<<tearful>> you gather yourself.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Drain Water]]>><<set $eventskip to 1>><</link>>
<</if>>