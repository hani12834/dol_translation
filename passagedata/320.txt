<<effects>>

You shake your head. The <<person>> looks relieved for a moment, but <<his>> ordeal isn't over. <<He>> leaves to find someone else to offer <<himself>> to.
<br><br>

<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>