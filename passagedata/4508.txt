<<effects>>

<<smugglerdifficultynpcs>>

You sneak toward the open bag. Now closer, you see the figure is a <<personstop>>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

<<if $smuggler_stolen_stat gte 4>>
	You wait nearby, hiding in the dark until each of <<his>> friends are looking away. The <<person>> <span class="green">doesn't look up</span> as you slip a hand inside the bag. You grasp a warm object. You pull it out, and creep away.
<<elseif $smuggler_stolen_stat gte 2>>
	You pause in the dark, waiting for <<his>> friend to look away. Your chance comes. The <<person>> <span class="green">doesn't look up</span> as you slip a hand inside the bag. You grasp a warm object. You pull it out, and creep away.
<<else>>
	<<He>> looks relaxed, <span class="green">and doesn't look up</span> as you slip a hand inside the bag. You grasp a warm object. You pull it out, and creep away.
<</if>>
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<smugglerobject>>
<<endevent>>
<<destinationsmuggler>>

<<else>>

	<<if $smuggler_stolen_stat gte 4>>
		You wait nearby, hiding in the dark until each of <<his>> friends are looking away. As you reach into the bag, <span class="red">the <<person>> jerks around and grasps your arm.</span> "We've been looking out for you," <<he>> says. "Our little thief." <<His>> friends close in.
	<<elseif $smuggler_stolen_stat gte 2>>
		You pause in the dark, waiting for <<his>> friend to look away. Your chance comes. As you reach into the bag however, <span class="red">the <<person>> jerks around and grasps your arm.</span> "Look what I caught," <<he>> says to the <<person2>><<personstop>> "Bet this is the blighter that robbed the others. Let's have some fun."<<person1>>
	<<else>>
		<<He>> looks relaxed, <span class="red">but jerks around</span> when you reach into the bag, grasping your arm.
	<</if>>
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>

<<link [[Next|Smuggler Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>