<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You tiptoe toward the door.
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<span class="green">They can't hear you over the sound of the water, and their own voice. You creak open the door, and step into a dark stone corridor.</span>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<pass 10>>
	You find yourself in a maze of halls and staircases. There are lots of wooden doors, but they're all locked. At last you find one that opens. You step through, and enter the main hall.
	<br><br>
	<<endevent>>
	<<link [[Next|Temple]]>><</link>>
	<br>
<<else>>
	Your foot clips a strange metal plate, <span class="red">sending it clattering across the floor.</span>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<endevent>>
	<<npc Jordan>><<person1>>The curtain is pulled aside. Jordan peeks around it. <<He>> screams.
	<<llgrace>><<grace -5>>
	<br><br>
	<<link [[Run|Temple Jordan Run]]>><</link>><<athleticsdifficulty 1 1400>>
	<br>
	<<link [[Apologise|Temple Jordan Apologise]]>><<grace 1>><</link>><<ggrace>>
	<br>
	<<if $promiscuity gte 15>>
		<<link [[Flirt|Temple Jordan Flirt]]>><</link>><<promiscuous2>>
		<br>
	<</if>>
<</if>>