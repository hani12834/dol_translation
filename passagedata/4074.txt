<<set $outside to 0>><<set $location to "home">><<effects>>

The doctor turns back to Robin, now stood naked. <<person3>><<He>> examines Robin from head to toe. "I don't think you need stitches," <<he>> says. "But this is an awful lot of bruising. Are you two siblings?"
<br><br>

"Yes," Robin says. "I mean, no. <<pShes>> like my big <<if $player.gender_appearance is "f">>sister<<else>>brother<</if>> though."
<br><br>

"Where do you live?" the doctor asks.
<br><br>

"At the orphanage," Robin replies.
<br><br>

The doctor frowns. "Some of this needs bandaging. You'll also need painkillers..."
<br><br>

<<person1>>The doctor sees to Robin wounds while you sit and hold <<his>> hand. Robin turns to you once dressed. "I'm a mummy!" <<he>> says, tapping the bandage wrapped around <<his>> head. <<He>> looks cheerful again for just a moment.
<br><br>

Once home, Robin gives you a surprise hug. "Thank you," <<he>> says. <<Hes>> tearful, but smiling. "I feel better already." <<He>> heads back to <<his>> room.
<br><br>
<<endevent>>
<<link [[Next|Orphanage]]>><</link>>
<br>