<<if $enemyhealth lte 0>>

They recoil in pain, a glint of fear in their eyes. "Fucking psycho!" They leave you in peace. <<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|School Library]]>><</link>>
<<elseif $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

<<person1>>The <<person>> looks at the <<person2>><<personstop>> "I came first, pay up!"
<br><br>

The <<person>> shakes <<his>> head. "No way, I started shuddering before you."
<br><br>

They leave you slumped over the desk. They're still arguing as their voices fade. <<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|School Library]]>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>
<<set $rescued += 1>>

<<endevent>>

The librarian, startled by your cries, arrives to investigate.
<br><br>

The pair cease their assault and flee between the bookshelves, as the librarian rushes over to you.

<<clotheson>>
<<endcombat>>
<<exposure>>
<<generate1>><<person1>>
<<if $exposed gte 1>>
<<He>> gives you some towels to cover up with.<<towelup>>
<</if>>
"Are you OK?" <<He>> asks.
	<<if $stress gte 10000>>
	Your consciousness is already slipping away, however.
	<br><br>
	<<endevent>>
	<<ambulance>>
	<<elseif $pain gte 100 and $willpowerpain is 0>>
You try to speak, but instead burst into tears. You feel the librarian's arms around you as you pass out.
	<br><br>
	<<endevent>>
	<<ambulance>>
	<<elseif $pain gte 20>>
<<tearful>> you nod and smile at the librarian through your tears. Once assured that you're unhurt, <<he>> returns to <<his>> duties.
<<endevent>>
<br><br>
<<link [[Next|School Library]]>><</link>>
	<<else>>
<<tearful>> you nod and smile at the blushing librarian. Once assured that you're unhurt, <<he>> returns to <<his>> duties.
<br><br>
<<endevent>>
<<link [[Next|School Library]]>><</link>>
	<</if>>
<<elseif $timer lte 0>>

Seeming to grow bored, the pair leave you alone. <<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|School Library]]>><</link>>
<</if>>