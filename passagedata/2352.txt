<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>"This style was popular among the middle-classes some hundred years ago," Winter says. <<He>> curls <<his>> nose. "I wouldn't drink from it now. I think it's had quite an adventure."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>