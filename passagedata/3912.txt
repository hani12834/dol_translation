<<set $outside to 0>><<set $location to "home">><<effects>>

You lean close, and whisper your invite into Kylar's ear. <<He>> shivers.<<promiscuity1>>
<br><br>

Kylar hugs your arm on the walk over, as if afraid you'll run.

<<if $rng gte 81>>
	No one bothers you until you arrive at the orphanage itself. "What's that thing doing here?" a <<generatey2>><<person>> asks as you enter the main hall. Anger flashes across Kylar's face, but <<person1>><<he>> doesn't say anything.
	<br><br>

	<<link [[Defend Kylar|Kylar Orphanage Defend]]>><<status -10>><<npcincr Kylar love 1>><</link>><<lcool>><<glove>>
	<br>
	<<link [[Berate Kylar|Kylar Orphanage Berate]]>><<npcincr Kylar love -3>><</link>><<lllove>>
	<br>
<<else>>
	No one bothers you. You arrive at the orphanage, and lead Kylar to your room.
	<br><br>

	<<link [[Next|Kylar Orphanage]]>><</link>>
	<br>
<</if>>