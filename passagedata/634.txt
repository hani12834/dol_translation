<<set $outside to 1>><<set $location to "wolf_cave">><<effects>>

<<if $phase is 1>>
<<set $phase to 0>>
	You search for plants long and sturdy enough to build an improvised garment. You find some suitable specimens and tie them together around your chest. It's fragile and revealing, but it's better than nothing.
	<<plantupper>>
	<br><br>
<<elseif $phase is 2>><<set $phase to 0>>
	You search for plants long and sturdy enough to build an improvised garment. You find some suitable specimens and tie them together around your waist. It's fragile and revealing, but it's better than nothing.
	<<plantlower>>
	<br><br>
<</if>>

<<link [[Next|Wolf Cave Clearing]]>><</link>>
<br>