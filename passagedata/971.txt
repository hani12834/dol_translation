<<effects>>

<<if $submissive gte 1150>>
	"This is wrong," you say. "Take me where I want to go. And give me back my clothes!"
<<elseif $submissive lte 850>>
	"Fuck you," you say. "We had an agreement. Take me where I want to go. And give me back my clothes."
<<else>>
	"I'm not playing your game," you say. "Take me where I want to go, and give me back my clothes."
<</if>>

<br><br>

"You'll do as I say," the <<person>> sneers, grasping your neck.

<<if $physique gte random(1, $physiquemax)>>
	<<He>> tries to push you from the car, <span class="green">but you resist.</span> You push <<his>> arms away and kick <<him>> in the face. You feel something come loose.
	<br><br>

	<<He>> clutches <<his>> face in one hand, while holding the other up in surrender. "F-fine," <<he>> says. "You violent psycho."
	<br><br>
	<<clotheson>>
	<<He>> wipes the blood away while you get dressed.
	<br><br>

	<<link [[Next|Farm Hitchhike Defiant]]>><</link>>
	<br>
<<else>>
	<span class="red"><<He>> shoves you from the car,</span> where you roll onto the dusty road. <<He>> slams the door shut, and drives away. <<stealclothes>>
	<br><br>

	<<endevent>>
	<<destinationfarmroad>>
<</if>>