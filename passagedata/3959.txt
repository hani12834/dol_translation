<<set $outside to 1>><<set $location to "town">><<effects>>

<<if $athletics gte random(1, 600)>>
You chase the fabric, and <span class="green">succeed in snatching it from the air.</span> A force still pulls on it. There's a hook embedded in the garment, with a thin wire attached. You free the hook, and the wire drags it into an open window on the second story of a building.
<br><br>

The fabric is ruined, but you wrap it around your loins like a towel. Better than nothing.
<<lowerwear 3>><<set $worn.lower.colour to $worn.lower.coloursaved>>
Your run attracted attention. You blush and keep walking.
<br><br>

<<destinationeventend>>

<<else>>
You chase the fabric, and try to snatch it from the air. <span class="red">It flies beyond your reach</span> and enters a window on the second story of a building. The window shuts after it.
<br><br>

Your run attracted attention. You run for the cover of a <<if $bus is "park">>bush<<else>>parked car<</if>> and crouch. You're stuck in the middle of town with your <<lewdness>> displayed.
<br><br>

<<destinationeventend>>

<</if>>