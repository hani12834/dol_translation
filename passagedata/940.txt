<<effects>>

<<if $museumAntiques.antiques.antiquestonetalisman isnot "found" and $museumAntiques.antiques.antiquestonetalisman isnot "talk" and $museumAntiques.antiques.antiquestonetalisman isnot "museum">>
	<<set $antiquemoney += 1200>><<museumAntiqueStatus "antiquestonetalisman" "found">>
	"Thank you!" the <<fox_text>> says. <<fox_He>> scurries back toward the tunnel, leaving your clothes and the strange stone behind.
<<else>>
	"Thank you!" the <<fox_text>> says. <<fox_He>> scurries back toward the tunnel, leaving your clothes behind.
<</if>>
<<clotheson>>

You turn away from the water, and walk back down the tunnel.
<br><br>

<<link [[Next|Meadow Cave Exit]]>><<unset $fox>><</link>>
<br>