<<widget "vore">><<nobr>>
<<set $rng to random(1, 100)>>
<<if $vorestage is 1>>
	Your thighs are gripped by the $vorecreature's mouth, your shins and feet at the mercy of its probing tongue.
	<<neutral 5>>
<<elseif $vorestage is 2>>
	Your waist is gripped by the $vorecreature's mouth, your delicate parts at the mercy of its probing tongue.
	<<neutral 5>>
<<elseif $vorestage is 3>>
	Your chest is gripped by the $vorecreature's mouth, your body at the mercy of its probing tongue.
	<<neutral 10>>
<<elseif $vorestage is 4>>
	Your shoulders are gripped by the $vorecreature's mouth, your body at the mercy of its probing tongue.
	<<neutral 10>>
<<elseif $vorestage is 5>>
	Your entire body is in the $vorecreature's mouth.
	<<neutral 15>>
<<elseif $vorestage is 6>>
	You are in the $vorecreature's gullet, pushed along by movements in the walls.
	<<neutral 15>>
<<elseif $vorestage is 7>>
	You are in the $vorecreature's stomach, it's a struggle to keep your head above the slimy liquid.
	<<neutral 20>>
<</if>>
<<if $vorestage is 1>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms,<span class="blue"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to the waist.
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm,<span class="blue"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to the waist.
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="blue">and it takes advantage of your vulnerability.</span> You slide deeper into its mouth, until it swallows you up to the waist.
		<</if>>
	<</if>>
<<elseif $vorestage is 2>>
	<<set $worn.lower.integrity -= 1>><<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 1>><</if>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms,<span class="purple"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to your chest.
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm,<span class="purple"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to your chest.
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="purple">and it takes advantage of your vulnerability.</span> You slide deeper into its mouth, until it swallows you up to your chest.
		<</if>>
	<</if>>
<<elseif $vorestage is 3>>
	<<set $worn.lower.integrity -= 1>><<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 1>><</if>><<set $worn.upper.integrity -= 1>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You push down on the $vorecreature's maw with both your arms,<span class="pink"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to your neck.
		<<elseif $vorestruggle is 1>>
			You push down on the $vorecreature's maw with one arm,<span class="pink"> but it isn't enough.</span> You slide deeper into its mouth, until it swallows you up to your neck.
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="pink">and it takes advantage of your vulnerability.</span> You slide deeper into its mouth, until it swallows you up to your neck.
		<</if>>
	<</if>>
<<elseif $vorestage is 4>>
	<<set $worn.lower.integrity -= 1>><<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 1>><</if>><<set $worn.upper.integrity -= 1>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both your arms,<span class="pink"> but it isn't enough.</span> The last of your body slides into its mouth, its lips closing behind you.
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm,<span class="pink"> but it isn't enough.</span> The last of your body slides into its mouth, its lips closing behind you.
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="pink">and it takes advantage of your vulnerability.</span> The last of your body slides into its mouth, its lips closing behind you.
		<</if>>
	<</if>>
<<elseif $vorestage is 5>>
	<<set $worn.lower.integrity -= 1>><<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 1>><</if>><<set $worn.upper.integrity -= 1>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both your arms,<span class="pink"> but it isn't enough.</span> The $vorecreature sucks you further down, sliding you into its gullet.
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm,<span class="pink"> but it isn't enough.</span> The $vorecreature sucks you further down, sliding you into its gullet.
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="pink">and it takes advantage of your vulnerability.</span> The $vorecreature sucks you further down, sliding you into its gullet.
		<</if>>
	<</if>>
<<elseif $vorestage is 6>>
	<<set $worn.lower.integrity -= 1>><<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 1>><</if>><<set $worn.upper.integrity -= 1>>
	<<if ($vorestruggle * $physique) gte $vorestrength>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both arms, <span class="green">preventing it from swallowing you further.</span>
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm, <span class="green">preventing it from swallowing you further.</span>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="green">but it doesn't take advantage of your vulnerability.</span>
		<</if>>
	<<else>>
		<<set $vorestomach to 0>>
		<<set $vorestage += 1>>
		<<if $vorestruggle is 2>>
			You grab the inside of the $vorecreature's maw with both your arms,<span class="pink"> but it isn't enough.</span> The $vorecreature sucks you further down, depositing you in a fleshy chamber.
			<<if $leftarm is "grappled">><<set $leftarm to 0>><</if>><<if $rightarm is "grappled">><<set $rightarm to 0>><<swallowed>><</if>>
		<<elseif $vorestruggle is 1>>
			You grab the inside of the $vorecreature's maw with one arm,<span class="pink"> but it isn't enough.</span> The $vorecreature sucks you further down, depositing you in a fleshy chamber.
			<<if $leftarm is "trapped">><<set $leftarm to 0>><</if>><<if $rightarm is "trapped">><<set $rightarm to 0>><<swallowed>><</if>>
		<<else>>
			There's nothing preventing the $vorecreature from swallowing you further, <span class="pink">and it takes advantage of your vulnerability.</span> The $vorecreature sucks you further down, depositing you in a fleshy chamber.
			<<if $leftarm is "trapped">><<set $leftarm to 0>><</if>><<if $rightarm is "trapped">><<set $rightarm to 0>><</if>><<swallowed>>
		<</if>>
	<</if>>
<<else>>
	<<set $worn.lower.integrity -= 5>>
	<<if $worn.lower.integrity lte 0>><<set $worn.under_lower.integrity -= 5>><</if>>
	<<set $worn.upper.integrity -= 5>>
<</if>>
<br><br>
<<set $vorestruggle to 0>>
<<set $rng to random(1, 100)>>
<<if $vorestage is 1>>
	<<if $rng gte 66>>
		You feel the $vorecreature licking your feet.
		<<neutral 5>>
	<<elseif $rng gte 33>>
		The $vorecreature's tongue protrudes from its maw, and licks your inner thighs.
		<<neutral 5>>
	<<else>>
		The $vorecreature's tongue protrudes from its maw, and licks your butt.
		<<neutral 5>>
	<</if>>
<<elseif $vorestage is 2>>
	<<if $rng gte 66>>
		You feel the $vorecreature licking your butt.
		<<neutral 5>>
	<<elseif $rng gte 33>>
		The $vorecreature gently licks your <<groinstop>>
		<<neutral 10>>
	<<else>>
		The $vorecreature's tongue wraps around your pelvis, and rubs up and down the length of your thighs.
		<<neutral 5>>
	<</if>>
<<elseif $vorestage is 3>>
	<<if $rng gte 66>>
		The $vorecreature's tongue presses your body against the roof of its maw.
		<<neutral 5>>
	<<elseif $rng gte 33>>
		The $vorecreature gently prods your <<groinstop>>
		<<neutral 10>>
	<<else>>
		The $vorecreature's tongue caresses your inner thighs.
		<<neutral 5>>
	<</if>>
<<elseif $vorestage is 4>>
	<<if $rng gte 66>>
		The $vorecreature runs the tip of its tongue down the length of your body.
		<<neutral 5>>
	<<elseif $rng gte 33>>
		The $vorecreature gently prods your <<groinstop>>
		<<neutral 10>>
	<<else>>
		The $vorecreature's tongue caresses your inner thighs.
		<<neutral 10>>
	<</if>>
<<elseif $vorestage is 5>>
	<<if $rng gte 66>>
		The $vorecreature runs the tip of its tongue down the length of your body.
		<<neutral 10>>
	<<elseif $rng gte 33>>
		The $vorecreature wraps its tongue around your body.
		<<neutral 10>>
	<<else>>
		The $vorecreature flicks your <<groin>> with the tip of its tongue.
		<<neutral 15>>
	<</if>>
<<elseif $vorestage is 6>>
	<<if $rng gte 81 and $leftarm is 0>>
		A groove in the side of the gullet constricts around your left arm, trapping it.
		<<set $leftarm to "trapped">><<neutral 10>>
	<<elseif $rng gte 61 and $rightarm is 0>>
		A groove in the side of the gullet constricts around your right arm, trapping it.
		<<set $rightarm to "trapped">><<neutral 10>>
	<<elseif $rng gte 41>>
		The gullet tightens around your entire body, holding you in place.
		<<neutral 10>>
	<<elseif $rng gte 21>>
		Valves open at the side of the tube and release a warm liquid, coating you in a slimy goo.
		<<neutral 15>>
	<<elseif $rng gte 1>>
		The sides of the gullet push against you, sliding you along the tube.
		<<neutral 15>>
	<</if>>
<<elseif $vorestage is 7>>
	<<if $rng gte 81 and $rightarm is 0>>
		A groove in the side of the chamber constricts around your right arm, trapping it.
		<<set $rightarm to "trapped">><<neutral 15>>
	<<elseif $rng gte 61>>
		More liquid squirts out the side of the chamber, covering you in a slimy goo.
		<<neutral 15>><<outergoo>>
	<<elseif $rng gte 41 and $leftarm is 0>>
		A groove in the side of the chamber constricts around your left arm, trapping it.
		<<set $leftarm to "trapped">><<neutral 15>>
	<<elseif $rng gte 21>>
		The chamber pulses to a gentle rhythm.
		<<neutral 15>>
	<<elseif $rng gte 1>>
		The entire chamber pulsates and rubs against you.
		<<neutral 20>>
	<</if>>
	<<if $vorestomach is 0>>
		<<set $vorestomach to 1>>
		<span class="blue">The walls close in around you.</span>
	<<elseif $vorestomach is 1>>
		<<set $vorestomach to 2>>
		<span class="purple">The walls close in around you.</span>
	<<elseif $vorestomach is 2>>
		<<set $vorestomach to 3>>
		<span class="pink">The walls close in around you, squeezing your body and stealing your breath.</span>
	<<elseif $vorestomach is 3>>
		<<set $vorestomach to 4>>
		<span class="red">The walls close in around you. You soon won't be able to move at all.</span>
	<<else>>
		<<set $vorestomach to 5>>
		<span class="red">The walls close in around you. The world starts to fade.</span>
	<</if>>
<</if>>
<br><br>
<<set $rng to random(1, 100)>>
<<if $vorestage lte 5>>
	<<if $rng gte 75>>
		<<set $vorestrength to random(0, 0)>>
		<span class="lblue">The $vorecreature seems content to savour your taste, for now.</span>
	<<elseif $rng gte 50>>
		<<set $vorestrength to random(-5000, 10000)>>
		<span class="blue">The $vorecreature salivates in anticipation.</span>
	<<elseif $rng gte 25>>
		<<set $vorestrength to random(-5000, 20000)>>
		<span class="purple">The $vorecreature prepares to suck you in.</span>
	<<elseif $rng gte 1>>
		<<set $vorestrength to random(1, 20000)>>
		<span class="pink">The $vorecreature prepares to gulp you down.</span>
	<</if>>
	<br><br>
<<else>>
	<<if $rng gte 75>>
		<<set $vorestrength to random(0, 0)>>
	<<elseif $rng gte 50>>
		<<set $vorestrength to random(-5000, 10000)>>
	<<elseif $rng gte 25>>
		<<set $vorestrength to random(-5000, 20000)>>
	<<elseif $rng gte 1>>
		<<set $vorestrength to random(1, 20000)>>
	<</if>>
<</if>>
<<if $voretrait gte 1>>
	<<set $vorestrength -= 2500>>
<</if>>
<<if $enemytype isnot "tentacles">>
	<<if $panicattacks gte 1 and $panicviolence is 0 and $panicparalysis is 0 and $controlled is 0>>
		<<set $rng to random(1, 100)>>
		<<if $rng is 100>>
			<<set $panicparalysis to 10>>
		<</if>>
	<</if>>
	<<if $panicattacks gte 2 and $panicviolence is 0 and $panicparalysis is 0 and $controlled is 0>>
		<<set $rng to random(1, 100)>>
		<<if $rng is 100>>
			<<set $panicviolence to 3>>
		<</if>>
	<</if>>
	<<if $arousal gte 10000>>
		<<orgasmpassage>>
	<</if>>
	<<set $seconds += 10>>
	<<if $seconds gte 60>>
		<<set $seconds to 0>>
		<<pass 1>>
	<</if>>
<</if>>
<</nobr>><</widget>>
<<widget "swallowedstat">><<nobr>>
<<set $swallowedstat += 1>>
<</nobr>><</widget>>
<<widget "swallowed">><<nobr>>
<<if $swallowed isnot 1>>
	<<set $swallowed to 1>>
	<<swallowedstat>>
<</if>>
<</nobr>><</widget>>