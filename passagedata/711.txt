<<effects>>

<<if $submissive gte 1150>>
	"Th-thank you," you say.
<<elseif $submissive lte 850>>
	"Thanks," you say.
<<else>>
	"Thank you," you say.
<</if>>
<br><br>

Alex smiles. "Don't mention it. I'll keep an ear out in case you need me again."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>