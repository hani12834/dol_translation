<<effects>>

<<set $rng to 2>>
<<if $rng is 1>>
	You hear a clink of metal on glass. The <<person2>><<person>> stands at the end of the room, beside a set of open doors. Everyone turns to listen.
	<br><br>

	"Thank you for joining me this evening," the <<person>> says, smiling over the audience. "If you'd like to follow me outside, I have something to show you."
	<br><br>

	The excited guests shuffle through the open doors, you and Avery with them. A tall pavilion has been set up beside a fountain, with one side open to the garden, facing a set of stands. You and Avery take a seat.
	<br><br>

<<elseif $rng is 2>>
	The band playing in the corner conclude their tune. They wait for the applause to die down before starting the next. This one is more upbeat. Avery holds out <<person1>><<his>> hand. "Can I have this dance?"
	<br><br>

	<<link [[Take Avery's hand|Avery Party Dance]]>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
	<br>
	<<link [[Refuse|Avery Party Dance Refuse]]>><<npcincr Avery love -1>><<set $endear -= 50>><</link>><<garage>><<llove>><<lendear>>
	<br>

<<else>>
	"Welcome everyone," <<he>> says. "I see some new faces among the old tonight."

<</if>>