<<effects>>

<<endevent>><<npc Alex>><<person1>>
"I'm sorry about this," Alex says once Remy is gone. "I was afraid we'd get a visit like that once we started turning a profit. Fucking rat."
<br><br>

<<Hes>> so tense <<he>> shatters one of the cups while trying to clean it, so you take over the washing up. <<He>> calms down some, and laughs. "I might count on you too much."<<gglove>><<npcincr Alex love 3>>
<br><br>

"I don't know what Remy will try," <<he>> continues as you finish. "But working for me could be dangerous. I understand if you don't want to continue, but if you do, I can pay <span class="gold">£45</span> an hour."
<br><br>

<<He>> smiles. "I shouldn't let that idiot get me down. Come on. Things are going great, and there's more to do."
<br><br>
<<set $farm.wage to 4500>>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>