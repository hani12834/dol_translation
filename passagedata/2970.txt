<<set $outside to 0>><<set $location to "town">><<effects>>

You call to River, who pierces Whitney with <<if $NPCName[$NPCNameList.indexOf("River")].pronoun is "m">>his<<else>>her<</if>> blue eyes.
<br><br>

"I'm only joking around," Whitney scoffs as <<he>> makes a paper aeroplane from your solution. <<He>> throws it at you. "You're gonna make it up to me later."
<br><br>

You continue to the front row and find an empty seat. You glance around, worried that Whitney followed you. You see <<him>> sitting with <<his>> friends on the other side of the hall, in the second row.
<br><br>
<<endevent>>
<<link [[Next|Maths Competition Intro]]>><</link>>
<br>