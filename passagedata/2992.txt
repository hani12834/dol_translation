<<set $outside to 0>><<set $location to "town">><<effects>>
<<person1>>
Leighton and Sirris examine your project.

<<if $phase is 0>>
	<<if $sciencelichenchance gte 100>>
		Even Leighton seems impressed. "I've never seen colours like this in lichen," <<he>> says. Sirris beams at you.
	<<elseif $sciencelichenchance gte 20>>
		Leighton yawns. "Lichen? How boring," <<he>> says. Sirris seems happier with it.
	<<else>>
		They don't seem impressed. "It's like you made it just now," Leighton says. "You didn't even collect any lichen. What a waste of time."
	<</if>>
<<elseif $phase is 1>>
	<<if $scienceshroomchance gte 100>>
		Even Leighton seems impressed. "You've been thorough," <<he>> says. "Must have been a lot of work, finding these mushroom samples." Sirris beams at you.
	<<elseif $scienceshroomchance gte 20>>
		Leighton yawns. "Mushrooms grow in the forest," <<he>> says. "Who knew." Sirris seems more impressed.
	<<else>>
		They don't seem impressed. "I think the mushrooms went to your head," Leighton says. "What a waste of time."
	<</if>>
<<elseif $phase is 2>>
	<<if $sciencephalluschance gte 100>>
		"This is... Different," Leighton says.
		<br><br>

		"It's for science," Sirris says. "<<pShe>> should be commended for originality."
		<br><br>

		Leighton nods, and reads one of the papers stuck to your board. "I can get behind this. Though I'm not sure people count as wildlife."
	<<elseif $sciencephalluschance gte 20>>
		"This is... different," Leighton says.

		"It's for science," Sirris says. "<<pShe>> should be commended for originality."

		Leighton nods, and reads one of the papers stuck to your board. "I can get behind this. Though I'm not sure people count as wildlife."
		<br><br>
	<<else>>
		They stand and stare for a few moments. "So," Leighton says. "You decided to submit crudely drawn cocks to the science fair?" You nod. Sirris looks embarrassed.
		<br><br>
	<</if>>
<</if>>

<br><br>

<<link [[Flirt|Science Fair Flirt]]>><</link>><span class="gold"> +10% win chance</span>
<br>
<<link [[Explain your project|Science Fair Explain]]>><</link>>
<br>