You're reading "Raul and Janet".
<br><br>
The book tells the story of two lovers from few centuries ago. Raul is the son of a poor tailor, but from the honest work he prefers night trips in search of adventures. Janet is a young polite girl from a wealthy family and daughter of local baron and ruler of the city. Despite the differences between them they fall in love with each other.
<br><br>
<<if $english gte 400>>
	You slowly delve into the text.
	<br><br>
	<<link [[Continue reading (0:20)|ScarletBook3]]>><<pass 20>><<englishskill>><</link>><<genglish>>
	<br>
<<else>>
	It seems to be some kind of romance for old lonely people. Nothing interesting.
	<br><br>
<</if>>
	<<link [[Put the book away|School Library]]>><</link>>

/*indev scarlet book event. English 400-499*/