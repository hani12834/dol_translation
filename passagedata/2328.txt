<<set $outside to 0>><<set $location to "museum">><<effects>>

<<if ($willpower / 10) gte (($pain - 100) + random(1, 100)) and $phase isnot 1>>
<span class="green">You grit your teeth and bear it.</span> "It hurts!" you shout through tears. "It hurts so much!"
<<gwillpower>><<willpower 3>>
<br><br>
<<masopain 5>>
Winter doesn't stop. Again and again <<he>> lashes at your flesh. Some of the audience look disturbed, others shocked, and some are clearly enjoying it.
<br><br>

You feel like you're about to pass out, but then Winter stops. <<He>> walks in front of you, panting. "That ends our demonstration," <<he>> says. "Thank you for attending. We hope you found it informative, and we invite you to appreciate the many other antiques we have on display. Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The audience applaud. Some are crying themselves.
<br><br>

"You did very well," Winter says once you're back in the small room. "Let me see your back." You turn and feel <<him>> trace a finger over you skin. "I'm blessed to have your assistance," <<he>> says as <<he>> applies something soothing. "Thank you. I'd like to hold another demonstration next weekend if it suits you." <<He>> peeks around the door. "I think we've earned some new interest." <<He>> leaves you to get dressed.
<br><br>

Despite the pain, you feel a strong catharsis.<<trauma -24>>
<br><br>
<<endevent>><<unbind>>
<<set $museuminterest += 50>>
<<earnFeat "Pain Rider">>
<<link [[Next|Museum]]>><<clotheson>><</link>>
<br>
<<else>>
The pain is too much for you. <span class="red">"$wintersafeword!"</span> you shout."
<<ggwillpower>><<willpower 10>>
<br><br>

Winter ceases the assault. It only takes <<him>> a moment to remove the straps tying the weights to your legs. <<He>> helps you off the horse and holds your hand for a moment. Once sure you're okay, <<he>> turns to the audience.
<br><br>

"That ends our demonstration. Thank you for attending. We hope you found it informative, and we invite you to appreciate the many other antiques we have on display. Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The audience applaud politely, though some look like they wish it had gone further.
<br><br>

"You did very well," Winter says once you're back in the small room. "I'm blessed to have your assistance. Thank you. I'd like to hold another demonstration next weekend if it suits you." <<He>> peeks around the door. "I think we've earned some new interest." <<He>> leaves you to get dressed.
<br><br>

Despite the pain, you feel a strong catharsis.<<trauma -18>>
<br><br>
<<endevent>><<unbind>>
<<set $museuminterest += 40>>
<<link [[Next|Museum]]>><<clotheson>><</link>>
<br>
<</if>>