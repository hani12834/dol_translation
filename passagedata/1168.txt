<<effects>>

You lean forward and take a bite. It's so sweet and delicious. The juice runs down your chin. Remy turns the fruit for you, and you lean forward again, and again, until just the core is left.
<br><br>

"There's more where that came from," Remy says as <<he>> ruffles your hair. "As long as you're good." <<He>> pulls you into the field. You lick fruit off your chin as the gate clanks shut behind you.
<br><br>

<<link [[Next|Livestock Field]]>><<endevent>><</link>>
<br>