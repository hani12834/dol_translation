<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 3>>
This is going too far. You dress yourself with shaking fingers, doing your best to ignore the disappointed heckling from the class. You walk back to your seat. Sirris doesn't say anything, but refusing a teacher won't reflect well on you.
<br><br>

<<link [[Next|Science Lesson]]>><<clotheson>><<endevent>><</link>>
<br>

<<elseif $phase is 4>>

This is going too far. You dress yourself with shaking fingers, doing your best to ignore the disappointed heckling from the class. You walk back to your seat. Sirris doesn't say anything, but refusing a teacher won't reflect well on you.
<br><br>

<<link [[Next|Science Lesson]]>><<clotheson>><<endevent>><</link>>
<br>

<<else>>

You walk back to your seat. Sirris doesn't say anything, but refusing a teacher won't reflect well on you.
<br><br>

<<link [[Next|Science Lesson]]>><<endevent>><</link>>
<br>

<</if>>