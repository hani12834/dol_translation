<<effects>>

<<generate2>><<generate3>>

<<covered>> Remy takes your leash, and leads you into a small adjoining room. A <<person2>><<person>> and <<person3>><<person>> are chatting inside. They fall silent as the doors open.
<br><br>
<<person1>>
Without waiting for a command, they step forward and grasp both your arms before bending you over a hay bale. A firm push shunts you on top. The clanking of metal follows as <span class="lewd">your legs are forced into immobile iron shackles,</span> forcing you to stay on your knees. You feel the farmer's eyes on your <<bottom>> and <<genitalscomma>> and resist the urge to squirm.
<<legbind>>
<br><br>

<<His>> hands appear on either side of your head, holding leather straps attached to something metallic. <<He>> pulls it against your mouth, and ties the straps around the back of your head. It's a muzzle. <span class="purple">You can't move your mouth.</span>
<<facewear 17>><<set $livestock_muzzle to 1>>
<br><br>

"If you weren't such a tempermental beast," Remy says behind you as <<he>> closes a jangling iron collar around your neck. "This wouldn't be necessary."
<<if $cow is 6>>
	<<set $worn.neck.type.push("broken")>>
	<<neckruined>>
	<<neckwear 8>>
<</if>>
<br><br>

<<link [[Next|Livestock Intro Heavy 2]]>><</link>>
<br>