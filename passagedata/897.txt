<<effects>>


Alex leads you around the coop, toward a long stone building. You hear oinking. "The pigs are harder to keep fed. They're hungry buggers. It's a job to carry it all." You enter the sty. 

<<if $monsterchance gte random(1, 100) and ($hallucinations gte 1 or $monsterhallucinations is "f")>>
	<<if $malechance gte 100>>
		Several pigboys sit in pens.
	<<elseif $malechance gte 1>>
		Several pigboys and girls sit in pens.
	<<else>>
		Several piggirls sit in pens.
	<</if>>
<<else>>
	Several pigs sit in pens.
<</if>>
Some look quizzical, others lie on their sides.
<br><br>

"There's an area for them to roam outside. They don't need much space."
<br><br>

<<link [[Next|Farm Intro 5]]>><</link>>
<br>