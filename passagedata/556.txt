<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<if $phase is 1>>
	<<set $phase to 0>>
	You sit on the rock and look out at the lake. <i>Meditation effectiveness depends on willpower.</i>
	<<if $daystate is "night">>
		<<if $weather is "rain">>
			The violent darkness roils before you.
		<<else>>
			The blackness stretches before you.
		<</if>>
	<<else>>
		<<if $weather is "rain">>
			The water dances with raindrops.
		<<else>>
			The water is almost perfect in its stillness.
		<</if>>
	<</if>>
	<br><br>
<<elseif $phase is 2>>
	<<set $phase to 0>>
	With some effort you quiet your thoughts until they still.
	<br><br>
	An hour passes, and you feel less burdened.
	<br><br>
<<elseif $phase is 3>>
	<<set $phase to 0>>
	You detach yourself from your thoughts. You witness them come and go, as if summoned by something else.
	<br><br>
	<<if $awareness gte 400>>
		The water parts, revealing a gaping abyss. Terror shivers up your spine, and breaks your concentration.
		<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
		<br><br>
	<</if>>
<<elseif $phase is 4>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_penis_tiny gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Tiny Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 5>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_penis_small gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Small Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 6>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_penis_big gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Big Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 7>>
	<<set $phase to 0>>
	Your thoughts turn to your flat chest, and the mocking way people regard it. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_breasts_tiny gte 1000>>
		Realisation dawns as you lean back. Your chest is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Tiny Breasts</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 8>>
	<<set $phase to 0>>
	Your thoughts turn to your <<breastscomma>> and the mocking way people regard them. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_breasts_small gte 1000>>
		Realisation dawns as you lean back. Your <<breasts>> are beautiful. Why should you care what other people say about them? You've gained the <span class="green">Acceptance: Small Breasts</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 9>>
	<<set $phase to 0>>
	Your thoughts turn to your <<breastscomma>> and the mocking way people regard them. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_breasts_big gte 1000>>
		Realisation dawns as you lean back. Your <<breasts>> are beautiful. Why should you care what other people say about them? You've gained the <span class="green">Acceptance: Big Breasts</span> trait.
		<br><br>
	<</if>>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte $stressmax>>
	<<passoutlake>>
<<else>>
	<<meditateoptions>>
<</if>>
<<set $eventskip to 0>>