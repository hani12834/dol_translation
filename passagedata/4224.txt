<<set $outside to 0>><<set $location to "home">><<effects>>

Whitney snarls, drops the bag, and stomps on it. The <<person5>><<person>> bursts into tears. Whitney doesn't care, and continues to stomp until the sweets are ground to dust.
<br><br>
The <<person5>><<person>> runs inside, chased by laughter. "See you around, slut." Whitney says, resting <<his>> bat on <<his>> shoulder and marching away. <<His>> friends follow.
<br><br>

<<link [[Next|Domus Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>