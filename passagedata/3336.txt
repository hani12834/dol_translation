<<set $bus to "elk">><<set $outside to 0>><<effects>>
<<set $tower_sleep -= 10>>
You are in a strange monster's lair within a building near Elk Street. A gaping hole dominates one wall.
<br><br>
<<if $stress gte 10000>>
	<<link [[Next|Passout Monster Tower]]>><</link>>
	<br>
<<elseif $eventskip isnot 1 and $rng gte $tower_sleep>>
	You beast stirs from its slumber. It steps toward you, growling.
	<br><br>
	<<link [[Next|Monster Tower Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	The beast slumbers on a ruined mattress.
	<<if $tower_sleep gte 90>>
		<span class="teal">It snores deeply.</span>
	<<elseif $tower_sleep gte 75>>
		<span class="lblue">It snores.</span>
	<<elseif $tower_sleep gte 60>>
		<span class="blue">It snores, occasionally swatting at the air.</span>
	<<elseif $tower_sleep gte 45>>
		<span class="purple">It stretches in its sleep.</span>
	<<elseif $tower_sleep gte 30>>
		<span class="pink">It growls in its sleep.</span>
	<<else>>
		<span class="red">Its eyes open and it starts to stand, before falling back to sleep.</span>
	<</if>>
	<<set $tower_sleep -= 15>>
	<br><br>
	<<if $tower_view isnot 1>>
		<<link [[Look out the hole|Monster Tower View]]>><<set $tower_view to 1>><</link>>
		<br>
	<<else>>
		<<if $tower_sheet gte 12>>
			<<link [[Escape|Monster Tower Escape]]>><<pass 60>><</link>>
			<br>
		<<else>>
			<<link [[Make sheet rope (1:00)|Monster Tower Sheet]]>><<pass 60>><</link>>
			<br>
		<</if>>
		<<if $exposed gte 1>>
			<<link [[Cover yourself (0:15)|Monster Tower Cover]]>><<pass 15>><</link>>
			<br>
		<</if>>
	<</if>>
	<<link [[Calm beast (1:00)|Monster Tower Calm]]>><<pass 60>><<set $tower_sleep to 115>><</link>>
	<br>
	<<if $deviancy gte 15>>
		<<link [[Calm beast with lewd|Monster Tower Sex]]>><<set $sexstart to 1>><</link>><<deviant2>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>