<<effects>>
<<set $money -= $rentmoney>>
Bailey snatches the money from your hand and starts counting. "Good," <<he>> says, satisfied. "You came through.
<<set $baileydefeatedchain to 0>>

<<if $rentstage is 1>>
<<set $rentmoney to 30000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>. You didn't think it would get any easier did you?"

<<elseif $rentstage is 2>>
<<set $rentmoney to 50000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>."

<<elseif $rentstage is 3>>
<<set $rentmoney to 70000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>."

<<elseif $rentstage is 4>>
<<set $rentmoney to 100000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>."

<<elseif $rentstage is 5>>
<<set $rentmoney to 150000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>."

<<else>>
<<set $rentmoney to 200000>><<rentmod>>

Next week I want <span class="pink">£<<print $rentmoney / 100>></span>."

<</if>>

<br><br>

<<set $rentstage += 1>>

<<if $bus is "hospital">>
	<<if $crime gte 1000>>
	<<link [[Next|Hospital Arrest]]>><<pass 10>><<endevent>><</link>>
	<br>
	<<else>>
	<<link [[Next->Hospital front]]>><<pass 10>><<endevent>><</link>>
	<br>
	<</if>>
<<else>>
<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>
<</if>>