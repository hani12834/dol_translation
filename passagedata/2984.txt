<<effects>>

<<if $phase is 0>>
	<<set $scienceshroomheartready += 1>>
	You measure the heartshrooms. You note down the weight, volume, where you found them and describe what they look like. You note that they smell refreshing. You feel more energetic by the end.
	<<ltiredness>><<tiredness -6>>
	<br><br>
<<else>>
	<<set $scienceshroomwolfready += 1>>
	You measure the wolfshroom. You note down the weight, volume, where you found them and describe what they look like. You note that they smell sweet. Your hand is shaking by the end.
	<<ggarousal>><<arousal 3000>>
	<br><br>
<</if>>

<<link [[Next|Science Project]]>><<set $phase to 0>><</link>>
<br>