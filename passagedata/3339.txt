<<set $bus to "elk">><<set $outside to 0>><<effects>>

You walk over to the hole in the wall. A blast of wind almost snatches you away, but you cling to a pipe for support. Other tall buildings tower around. Beneath you is a sheer drop into a bed of mist below. You can't see the ground.
<br><br>
You call out. Only the wind howls in response. You call again. You hear the beast emit a low growl behind you.
<br><br>
You look around the lair, at the rubbish littering the floor and piled against the walls. You lift a strip of ruined cloth, and tug. It's sturdy.
<br><br>

<<link [[Next|Monster Tower]]>><</link>>
<br>