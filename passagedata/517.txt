<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
You kiss them back. One after the other, while the other kisses your cheek. Sometimes they kiss each other.
<<promiscuity1>>
<br><br>
"I'm sorry," the <<person2>><<person>> pants. "I should have asked first." You're not sure who <<hes>> addressing.
<br><br>
<<person1>>"It's getting late," the <<person>> says. "The tent is waiting."
<br><br>
<<if $promiscuity gte 15>>
	<<link [[Join them|Lake Couple 5]]>><</link>><<promiscuous2>>
	<br>
<</if>>
<<if $exhibitionism gte 35>>
	<<link [[Strip and join them|Lake Couple 5]]>><<strip>><<set $phase to 1>><</link>><<exhibitionist3>>
	<br>
<</if>>
<<link [[Part ways|Lake Campsite]]>><<endevent>><</link>>
<br>