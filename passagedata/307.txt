<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<if $enemyhealth lte 0>>
	You push <<him>> away from you. <<He>> looks angry, but soon regains <<his>> composure. "You still have some issues with anger management," <<he>> says. "But that's not why you were here. I'd like to keep seeing you on Fridays, at the hospital. There's a vehicle waiting to take you back to town. Be well."
	<br><br>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	<<He>> lies on top of you for a moment, breathing deeply. <<He>> steps away and fixes <<his>> clothing. "T-there's a vehicle waiting to take you back to town. I'd like to keep seeing you on Fridays, at the hospital. Be well."
<<else>>
	<<He>> looks angry, but soon regains <<his>> composure. "If only I could keep you until I finished the treatment," <<he>> sighs. "There's a vehicle waiting to take you back to town. I'd like to keep seeing you on Fridays, at the hospital. Be well."
<</if>>
<br><br>
<<clothesontowel>>
<<endcombat>>
<<if $suspicion gte 80>>
	A group of orderlies lead you away from Harper's office, through the main area of the asylum.
<<elseif $suspicion gte 20>>
	A pair of orderlies lead you away from Harper's office, through the main area of the asylum.
<<else>>
	A nurse leads you away from Harper's office, through the main area of the asylum.
<</if>>
<<if $asylumstatus gte 80>>
	The other patients wave you goodbye.
<<elseif $asylumstatus gte 20>>
	The other patients ignore you.
<<else>>
	The other patients sneer at you.
<</if>>
You go through a staff door, then through a larger double door and into a waiting car.
<br><br>
The car starts, and drives through the iron gates into the forest. It's a dark, overgrown road. You don't move very fast. The driver says nothing.
<br><br>
<<pass 60>>
You emerge from the wood far from town, but the road curves north through moor and farmland. You soon arrive back at civilisation. The bustle of Harvest Street greets you. You continue until the orphanage comes into view.
<br><br>
<<asylumend>>
<<endevent>>
<<link [[Next|Asylum Return]]>><</link>>