<<effects>>

You hand over the money Alex gave you, and pick up the basket.
<br><br>
<<endevent>>
<<npc Alex>><<person1>>
<br>

You return to the farm. Alex meets you in the yard. "Nice," <<he>> says. "Leave it in the kitchen."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>