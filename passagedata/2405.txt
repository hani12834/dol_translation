<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>
<<if $submissive gte 1150>>
	"I-it's nothing," you say.
<<elseif $submissive lte 850>>
	"None of your business," you say.
<<else>>
	"School stuff," you say.
<</if>>
<br><br>
<<He>> nods. "I'll help you down when you're done. I'm only out for a stroll, there's no rush."
<br><br>
The lichen is attached firm, but you manage to pry it free with some effort. <<He>> offers an arm and helps you climb down. "I'm Avery," <<he>> says. "I bet you're thirsty after that. Would you like to get a drink? I know this cute little place."
<br><br>
<<link [[Accept|Park Lichen Accept]]>><</link>>
<br>
<<link [[Refuse|Park Lichen Refuse]]>><</link>>
<br>