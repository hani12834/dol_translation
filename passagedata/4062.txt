<<set $outside to 1>><<set $location to "beach">><<effects>>
You clench your fists in anger. "What would your superior think about you shaking down lemonade stands?"
<br><br>
The <<person1>><<person>> laughs. "Don't pretend anyone cares." <<He>> looks back at Robin. "Pay up!" <<He>> snaps, making Robin jump in fright.
<br><br>
"And I'm sure you've been giving them their cut, right?" The <<person2>><<person>> looks nervous.
<br><br>
"Shut up, slut," the <<person1>><<person>> says, but the <<person2>><<person>> turns to <<person1>><<himstop>> "This sounds like a headache. I think we leave this one."
<br><br>
"Fine," the <<person>> snarls. <<He>> looks at you. "I'll remember your face."
<br><br>
<<endevent>><<npc Robin>><<person1>>
Robin bursts into tears and buries <<his>> head in your shoulder. "I-I," <<he>> sobs. "I was so scared. Thank you." Once <<he>> calms down a bit <<he>> puts the money back beneath the stand.
<br><br>
<<endevent>>
<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
<br>
<<link [[Leave|Beach]]>><</link>>
<br>