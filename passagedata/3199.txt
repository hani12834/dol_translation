<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "residential">>

You feel pavement beneath your feet and hear <<if $hour gte 7 && $hour lte 20>>cars and pedestrians nearby.<<else>>the occasional car driving nearby.<</if>>
<br><br>

<<if $exposed gte 1>>
	<<covered>>
<</if>>
<br><br>

<<if $stress gte 10000>><<set $phase to 0>>
	<<passoutstreet>>
<<elseif !$worn.face.type.includes("blindfold")>>
	<span class="green">The tattered blindfold falls to pieces around your head, freeing you.</span> You squint against the light.
	<<ltrauma>><<lllstress>><<trauma -6>><<stress -24>>
	<br><br>
	<<destinationeventend>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip isnot 1 or $phase is 1>><<set $phase to 0>>
		<<eventsbondagewest>>
	<<else>>
		<<link [[Run towards the sound of machinery (0:05)|Bondage Run]]>><<set $bus to "connudatus">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run towards the sound of the sea (0:05)|Bondage Run]]>><<set $bus to "barb">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run towards the sound of traffic (0:05)|Bondage Run]]>><<set $bus to "domus">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run away from the sounds of the town (0:05)|Bondage Run]]>><<set $bus to "danube">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Hide (0:05)|Bondage Hide]]>><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
	<</if>>
<</if>>

<<set $eventskip to 0>>