<<effects>>
<<endmasturbation>>
<<endcombat>>
<<generatesf1>><<person1>>
<<if $rng gte 81>>
	You hear a thud from behind. A <<person>> stands gaping at you, <<his>> bag on the floor beside <<himstop>> <<He>> blinks free of <<his>> shock and grins. "Girls!" <<he>> shouts. "You have an audience." The changing room falls silent.
	<br><br>
	<<if $exhibitionism gte 75>>
		<<link [[Enter|School Girl's Exhibitionism]]>><<detention 3>><</link>><<gdelinquency>><<exhibitionist5>>
		<br>
	<</if>>
	<<link [[Run|School Girl's Run]]>><</link>>
	<br>
<<else>>
	You hear a thud from behind. A <<person>> stands gaping at you, <<his>> bag on the floor beside <<himstop>> Blushing, <<he>> grabs <<his>> bag and runs the way <<he>> came.
	<<gstress>><<stress 3>>
	<br><br>
	<<clotheson>>
	<<link [[Next|School Pool Entrance]]>><</link>>
	<br>
<</if>>