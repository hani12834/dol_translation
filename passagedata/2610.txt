<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $submissive gte 1150>>
	"I-it's nothing." You say. "It was just a s-silly prank."
	<br>
<<elseif $submissive lte 850>>
	"It's a costume," you say. "It's quirky. It's fun. Don't you have a sense of humour?"
	<br>
<<else>>
	"It's a costume," you say. "I forgot to take it off."
	<br>
<</if>>
Winter sighs. "Explain it to Leighton in detention."
<<detention 2>><<gdelinquency>>
<br><br>
<<endevent>>
<<link [[Next|History Lesson]]>><</link>>
<br>