<<effects>>
<<set $bartend_info to 1>>
<<npc Darryl>><<person1>>
You enter Darryl's office. <<Hes>> standing by <<his>> desk, hunched over an open book. <<Hes>> doesn't realise you're there until you're stood right beside <<himstop>>
<br><br>

"Oh, sorry," <<he>> says, turning to face you. <<if $skulduggery gte 200>><<He>> shuts the book and covers it with another.<</if>> "I was lost in thought. Is everything okay downstairs?"
<br><br>

You tell <<him>> what happened, and what you overheard. "I'm sorry you have to go through this," <<he>> says when you're finished. "I'll have someone wait for you outside. It's just a precaution, but they'll make sure you get home safely."
<br><br>

You return to the bar. The rest of the shift passes without event.
<<tipreceive>>
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>