<<effects>>

<<if $submissive gte 1150>>
	"L-leave <<person1>><<him>> alone," you say, surprised by your own bravery.
<<elseif $submissive lte 850>>
	"Put the bag down," you say. "And step away from <<person1>><<himstop>> Now."
<<else>>
	"Leave <<person1>><<him>> alone," you say. "Bullying is wrong."
<</if>>
<br><br>

All three bullies look at you. The <<person1>><<person>> continues to stare at <<his>> feet. The <<person2>><<person>> barks a laugh and swaggers closer. "Or what?" <<he>> asks. "You gonna start something?" The <<person3>><<person>> beside <<him>> drops the bag and cracks <<his>> knuckles.
<br><br>

<<link [[Walk away|Street Bullies Walk]]>><<sub 1>><</link>>
<br>
<<link [[Fight|Street Bullies Fight]]>><<stress 6>><<def 1>><<famescrap 4>><<famegood 1>><</link>><<gstress>>
<br>