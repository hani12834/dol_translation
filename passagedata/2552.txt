<<set $outside to 0>><<schooleffects>><<effects>>
<<if $submissive gte 1150>>
	"I-I didn't want to dress like a boy" you say. "I forgot my uniform."
<<elseif $submissive lte 850>>
	"Some idiot made me dress this way,"
<<else>>
	"I had no choice," you say. "I forgot my uniform."
<</if>>
<br><br>
The girls enjoy a laugh at your expense. "We're not letting you in here looking like a boy," <<he>> says. "It's distracting." <<He>> blushes and throws you your clothes.
<br><br>
<<clotheson>>
<<endevent>>
<<link [[Next|School Pool Entrance]]>><</link>>
<br>