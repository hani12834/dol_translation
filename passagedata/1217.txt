<<effects>>

<<if $breastfeedingdisable is "f">>
	<<if $penisexist is 1 and !$worn.genitals.type.includes("chastity")>>
		Remy retrieves a few glass pumps from just outside your cage. It's attached to a tube leading to a much larger machine. The same machine the other cattle are being connected to. <<He>> grasps your collar and pulls you towards the gate. "On your knees <<girl>>."
		<br><br>

		<<if $cow gte 6>>
			<<link [[Moo|Livestock Lactate Moo]]>><<npcincr Remy dom 1>><<npcincr Remy love 1>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
			<br>
		<</if>>
		<<link [[Obey|Livestock Lactate Obey]]>><<npcincr Remy dom 1>><<livestock_obey 1>><<sub 1>><<transform cow 1>><</link>><<gobey>>
		<br>
		<<link [[Refuse|Livestock Lactate Refuse]]>><<npcincr Remy dom -1>><<livestock_obey -1>><<def 1>><</link>><<lobey>>
		<br>
	<<else>>
		Remy retrieves a couple of glass pumps from just outside your cage. It's attached to a tube leading to a much larger machine. The same machine the other cattle are being connected to. <<He>> grasps your collar and pulls you towards the gate. "On your knees <<girl>>."
		<br><br>

		<<if $cow gte 6>>
			<<link [[Moo|Livestock Lactate Moo]]>><<npcincr Remy dom 1>><<npcincr Remy love 1>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
			<br>
		<</if>>
		<<link [[Obey|Livestock Lactate Obey]]>><<npcincr Remy dom 1>><<livestock_obey 1>><<sub 1>><<transform cow 1>><</link>><<gobey>>
		<br>
		<<link [[Refuse|Livestock Lactate Refuse]]>><<npcincr Remy dom -1>><<livestock_obey -1>><<def 1>><</link>><<lobey>>
		<br>
	<</if>>
<<else>>
	<<if $penisexist is 1 and !$worn.genitals.type.includes("chastity")>>
		Remy retrieves a glass pump from just outside your cage. It's attached to a tube leading to a much larger machine. The same machine the other cattle are being connected to. <<He>> grasps your collar and pulls you towards the gate. "On your knees <<girl>>."
		<br><br>

		<<if $cow gte 6>>
			<<link [[Moo|Livestock Lactate Moo]]>><<npcincr Remy dom 1>><<npcincr Remy love 1>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
			<br>
		<</if>>
		<<link [[Obey|Livestock Lactate Obey]]>><<npcincr Remy dom 1>><<livestock_obey 1>><<sub 1>><<transform cow 1>><</link>><<gobey>>
		<br>
		<<link [[Refuse|Livestock Lactate Refuse]]>><<npcincr Remy dom -1>><<livestock_obey -1>><<def 1>><</link>><<lobey>>
		<br>
	<<else>>
		"You're not much use for now," Remy says. "But that doesn't mean you shouldn't be treated." <<He>> retrieves a small pink pill-shaped object attached to a wire that leads to the milking machine. The same machine the other cattle are being connected to. <<He>> grasps your collar and pulls you towards the gate. "On your knees <<girl>>."
		<br><br>

		<<if $cow gte 6>>
			<<link [[Moo|Livestock Lactate Moo]]>><<npcincr Remy dom 1>><<npcincr Remy love 1>><<livestock_obey 5>><<sub 1>><<transform cow 1>><</link>><<ggobey>>
			<br>
		<</if>>
		<<link [[Obey|Livestock Lactate Obey]]>><<npcincr Remy dom 1>><<livestock_obey 1>><<sub 1>><<transform cow 1>><</link>><<gobey>>
		<br>
		<<link [[Refuse|Livestock Lactate Refuse]]>><<npcincr Remy dom -1>><<livestock_obey -1>><<def 1>><</link>><<lobey>>
		<br>

	<</if>>
<</if>>