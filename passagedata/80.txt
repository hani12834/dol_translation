<<widget "beastclothing">><<nobr>>

<<set $rng to random(1, 100)>>

<<if $NPCList[_n].mouth is "lowerclothes">>
	<<if $NPCList[_n].stance is "top">>
		<<set $NPCList[_n].mouth to 0>>
	<<else>>
		<<if $worn.lower.name is "naked">>
			<span class="purple"><<bHe>> spits out the ruined fabric.</span>
			<<set $NPCList[_n].mouth to 0>>
		<<elseif $lowerstruggle is 1>>
			<<set $lowerstruggle to 0>>
			<<bHe>> tugs at your $worn.lower.name, but you keep <<him>> from stripping you.
			<<set $lowerstruggle to 0>><<neutral 1>><<set $worn.lower.integrity -= 5>>
		<<elseif $rng gte 91>>
			<span class="blue"><<bHe>> releases your $worn.lower.name from <<bhis>> mouth.</span>
			<<set $NPCList[_n].mouth to 0>>
		<<elseif $rng lte 90>>
			<<if $worn.upper.set is $worn.lower.set>>
				<<if $worn.upper.open is 1>>
					<<if $worn.upper.state_top is "chest">>
						<<bHe>> tugs your $worn.lower.name, pulling down your $worn.upper.name and <span class="lewd">revealing your <<breastsstop>></span>
						<<set $worn.upper.state_top to "midriff">><<set $worn.upper.exposed to 2>><<neutral 3>><<set $speechbreasts to 1>>
						<<if $worn.upper.state is "chest">>
							<<set $worn.upper.state to "midriff">>
						<</if>>
						<<if $worn.lower.state is "chest">>
							<<set $worn.lower.state to "midriff">>
						<</if>>
					<<elseif $worn.upper.state_top is "midriff">>
						<<bHe>> tugs your $worn.lower.name, pulling down your $worn.upper.name passed your midriff.
						<<set $worn.upper.state_top to "waist">><<neutral 1>>
						<<if $worn.upper.state is "midriff">>
							<<set $worn.upper.state to "waist">>
						<</if>>
						<<if $worn.lower.state is "midriff">>
							<<set $worn.lower.state to "waist">>
						<</if>>
					<<elseif $worn.upper.state_top is "waist">>
						<<bHe>> pulls your $worn.upper.name down to your thighs, revealing your
						<<if $worn.under_lower.state is "waist">>
							$worn.under_lower.name
							<<neutral 2>>
						<<else>>
							<span class="lewd"><<genitalsstop>></span>
							<<neutral 5>><<set $speechgenitals to 1>>
						<</if>>
						<<set $worn.upper.state_top to "thighs">><<set $worn.upper.state to "thighs">><<set $worn.lower.vagina_exposed to 1>><<set $worn.lower.anus_exposed to 1>><<set $worn.lower.exposed to 2>>
						<<if $worn.lower.state is "waist">>
							<<set $worn.lower.state to "thighs">>
						<</if>>
					<<elseif $worn.upper.state_top is "thighs">>
						<<bHe>> pulls your $worn.upper.name down to your knees.
						<<set $worn.upper.state_top to "knees">><<set $worn.upper.state to "knees">><<neutral 1>>
						<<if $worn.lower.state is "thighs">>
							<<set $worn.lower.state to "knees">>
						<</if>>
					<<elseif $worn.upper.state_top is "knees">>
						<<bHe>> pulls your $worn.upper.name down to your ankles.
						<<set $worn.upper.state_top to "ankles">><<set $worn.upper.state to "ankles">><<neutral 1>>
						<<if $worn.lower.state is "knees">>
							<<set $worn.lower.state to "ankles">>
						<</if>>
					<<elseif $worn.upper.state_top is "ankles">>
						<span class="purple"><<bHe>> pulls your $worn.upper.name off the bottom of your legs.</span>
						<<neutral 5>><<set $worn.upper.state_top to 0>><<set $worn.upper.state to 0>><<upperstrip>>
						<<if $worn.lower.state is "ankles">>
							<<set $worn.lower.state to 0>><<lowerstrip>>
						<</if>>
					<</if>>
				<<else>>
					<<bHe>> tugs on your $worn.lower.name, tugging your $worn.upper.name against your shoulders. You hear a tearing sound.
					<<set $worn.lower.integrity -= 20>><<neutral 1>>
				<</if>>
			<<elseif $worn.upper.set isnot $worn.lower.set>>
				<<if $worn.lower.state is "waist">>
					<<bHe>> pulls down your $worn.lower.name, exposing your
					<<if $worn.under_lower.state is "waist">>
						$worn.under_lower.name.
						<<neutral 2>><<set $worn.lower.vagina_exposed to 1>><<set $worn.lower.anus_exposed to 1>>
					<<else>>
						<span class="lewd"><<genitalsstop>></span>
						<<neutral 5>><<set $speechgenitals to 1>>
					<</if>>
					<<set $worn.lower.state to "thighs">><<set $worn.lower.vagina_exposed to 1>><<set $worn.lower.anus_exposed to 1>><<set $worn.lower.exposed to 2>>
				<<elseif $worn.lower.state is "thighs">>
					<<bHe>> pulls your $worn.lower.name down to your knees.
					<<set $worn.lower.state to "knees">><<neutral 1>>
				<<elseif $worn.lower.state is "knees">>
					<<bHe>> pulls your $worn.lower.name down to your ankles.
					<<set $worn.lower.state to "ankles">><<neutral 1>>
				<<elseif $worn.lower.state is "ankles">>
					<span class="purple"><<bHe>> pulls your $worn.lower.name off your legs.</span>
					<<lowerstrip>>
					<<set $NPCList[_n].mouth to 0>><<neutral 3>><<clothesstripstat>>
				<</if>>
			<<else>>
				<<bHe>> tugs on your $worn.lower.name, you hear the fabric tear.
				<<neutral 1>><<set $worn.lower.integrity -= 20>>
			<</if>>
		<</if>>
	<</if>>
<</if>>

<<set $rng to random(1, 100)>>

<<if $NPCList[_n].mouth is "underclothes">>
	<<if $worn.under_lower.name is "naked">>
		<span class="purple"><<bHe>> spits out the ruined fabric.</span>
		<<set $NPCList[_n].mouth to 0>>
	<<elseif $understruggle is 1>>
		<<bHe>> tugs on your $worn.under_lower.name, but you keep <<him>> from stripping you.
		<<set $understruggle to 0>><<set $speechstripstruggle to 1>><<neutral 1>><<set $worn.under_lower.integrity -= 5>>
	<<elseif $rng gte 91>>
		<span class="blue"><<bHe>> releases your $worn.under_lower.name from <<bhis>> mouth.</span>
		<<set $NPCList[_n].mouth to 0>>
	<<elseif $rng lte 90>>
		<<if $worn.lower.skirt is 1>>
			<<if $worn.under_lower.state is "waist">>
				<<bHe>> pulls your $worn.under_lower.name down your thighs, <span class="lewd">revealing your <<genitalsstop>></span>
				<<set $worn.under_lower.state to "thighs">><<neutral 5>><<set $worn.under_lower.vagina_exposed to 1>><<set $worn.under_lower.anus_exposed to 1>><<set $worn.under_lower.exposed to 1>><<set $speechgenitals to 1>>
			<<elseif $worn.under_lower.state is "thighs">>
				<<bHe>> pulls your $worn.under_lower.name down to your knees.
				<<set $worn.under_lower.state to "knees">><<neutral 1>>
			<<elseif $worn.under_lower.state is "knees">>
				<<bHe>> pulls your $worn.under_lower.name down to your ankles.
				<<set $worn.under_lower.state to "ankles">><<neutral 1>>
			<<elseif $worn.under_lower.state is "ankles">>
				<span class="purple"><<bHe>> pulls your $worn.under_lower.name off your legs.</span>
				<<underlowerstrip>><<neutral 3>><<set $NPCList[_n].mouth to 0>><<clothesstripstat>>
			<</if>>
		<<elseif $worn.lower.state isnot "waist">>
			<<if $worn.under_lower.state is "waist">>
				<<bHe>> pulls your $worn.under_lower.name down your thighs, <span class="lewd">revealing your <<genitalsstop>></span>
				<<set $worn.under_lower.state to "thighs">><<neutral 5>><<set $worn.under_lower.vagina_exposed to 1>><<set $worn.under_lower.anus_exposed to 1>><<set $worn.under_lower.exposed to 1>><<set $speechgenitals to 1>>
			<<elseif $worn.under_lower.state is "thighs">>
				<<bHe>> pulls your $worn.under_lower.name down to your knees.
				<<set $worn.under_lower.state to "knees">><<neutral 1>>
			<<elseif $worn.under_lower.state is "knees">>
				<<bHe>> pulls your $worn.under_lower.name down to your ankles.
				<<set $worn.under_lower.state to "ankles">><<neutral 5>>
			<<elseif $worn.under_lower.state is "ankles">>
				<span class="purple"><<bHe>> pulls your $worn.under_lower.name off your legs.</span>
				<<underlowerstrip>><<neutral 3>><<set $NPCList[_n].mouth to 0>><<clothesstripstat>>
			<</if>>
		<</if>>
	<</if>>
<</if>>

<<if $oxygen lt $oxygenmax and $underwater isnot 1 and _strangle is 1>>
	<<unset _strangle>>
	<br>Air: <<oxygencaption>>
<</if>>

<</nobr>><</widget>>