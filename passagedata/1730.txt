<<set $outside to 1>><<set $location to "compound">><<effects>>
"We won't need to wait long," the <<person1>><<person>> says. As if in answer, the door to the central building opens. A <<generate2>><<person2>><<person>> in a lab coat exits.
<br><br>
"I'll take the <<girl>> from here," <<he>> says. "They want to speak to <<phim>> downstairs."
<br><br>
"If you say so."
<br><br>
As soon as the <<person>> touches your arm a jolt surges through you. You lose consciousness.
<br><br>
<<upperruined>><<lowerruined>><<underruined>>
<<link [[Next|Elk Compound Experiment]]>><<endcombat>><</link>>
<br>