<<if $molestationstart is 1>>
	<<generate1>><<person1>>A <<person>> pins you down.
	<br>
	<<set $molestationstart to 0>>
	<<controlloss>>
	<<violence 1>>
	<<neutral 1>>
	<<molested>>
	<<maninit>>
	<<set $rescue to 0>>/*no rescue possible*/
	<<set $dun_counter to 1>>
<</if>>
<<effects>>
<<effectsman>>
<<if $alarm is 1>>
	<<set $alarm to 0>>
	<<switch $dun_counter>>
	<<case 1>>
		"Soundproofed walls," <<he>> breathes. "So I can enjoy the screams."
	<<case 2>>
		"Keep screaming!" <<he>> whispers. "It makes it hotter."
		<<set $enemyarousal += 10>>
	<<case 3>>
		"Scream harder!" <<he>> growls. "You're turning me on."
		<<set $enemyarousal += 20>>
	<<case 4>>
		"Scream!" <<he>> growls. "Like I'm hurting you."
		<<set $enemyarousal += 20>>
	<<default>>
		"I own you bitch!" <<he>> grunts. "Scream!"
		<<set $enemyarousal += 30>>
	<</switch>>
	<<if $NPCList[0].lefthand is "mouth" or $NPCList[0].righthand is "mouth">>
		<br><span class="teal"><<He>> releases <<his>> hand from your mouth.</span>
		<<set $NPCList[0].righthand to 0>>
		<<set $NPCList[0].lefthand to 0>>
		<<set $mouthuse to 0>>
	<</if>>
	<<set $enemytrust -= 20>><<set $enemyanger -= 30>>/*reduce rate of anger/distrust*/
	<<set $dun_counter += 1>>
	<br><br>
<</if>>
<<man>>

<<stateman>>
<br><br>
<<actionsman>>

<<if $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Domus House Dungeon Rape Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Domus House Dungeon Rape Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Domus House Dungeon Rape]]>><</link>></span><<nexttext>>
<</if>>