<<effects>>

You wrap your arms around Robin. <<He>> rests <<his>> head on your shoulder, and sobs.
<br><br>

"I'm okay," <<he>> says after a few minutes, wiping <<his>> eyes. "Thank you. I'll be inside."
<br><br>

<<link [[Next|Garden Flowers]]>><<endevent>><</link>>
<br>