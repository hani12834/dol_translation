<<effects>>

You turn and run the way you came, hoping the barrier is tall enough to protect your <<lewdness>> from anyone looking below. If the pair ahead will see everything if they reach the top of the stairs before you reach the end of the bridge.
<br><br>

<<if $athletics gte random(1, 500)>>
	<span class="green">You make it to the stairs in time.</span> You dart down the steps and into the park, where you slump to the ground behind a tree. Your heart thunders in your chest.
	<br><br>

	<<link [[Next|Park]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<<else>>
	<<fameexhibitionism 2>>
	You're almost there, when you <span class="red">trip</span> and fall to your knees, sticking your <<bottom>> in the hair. Between your legs you see the <<person1>><<person>> and <<person2>><<personcomma>> stood still and staring right at you.
	<<gstress>><<garousal>><<stress 6>><<arousal 6>>
	<br><br>
	You scramble the rest of the way, and dash down the stairs.
	<br><br>
	You hide behind a tree and slump to the ground, your heart thundering in your chest.
	<br><br>

	<<link [[Next|Park]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<</if>>