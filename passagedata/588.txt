Deciding you've had enough, you attempt to unravel yourself and leave the room.
<br><br>

<<if $swimmingskill gte random(1, 1000)>>
	<span class="green">You free your ankles and make your escape.</span> The tentacles grasp at you with increasing desperation. You narrowly avoid one shooting for your leg, and swim through the doorway. The tentacles protrude from the room and grope around the walls, but they refuse to chase you any further. They slink away.
	<br><br>
	<<link [[Continue|Lake Ruin Deep]]>><<loxygen>><</link>>
	<br>
<<else>>
	You try to swim towards the doorway, <span class="red">but you're unable to free yourself.</span> The tentacles snaring your ankles drag you back into the centre of the room while the rest surround you from all sides.
	<br><br>
	<<link [[Next|Lake Ruin Deep NonConsentacles]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>