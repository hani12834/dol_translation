<<effects>>

You follow Alex through the field, toward a barn. A dog sits outside, its mouth open and tongue lolling.
<br><br>

<<if $monsterchance gte random(1, 100) and ($hallucinations gte 1 or $monsterhallucinations is "f")>>
	<<if $malechance gte 100>>
		Inside, a number of bullboys stand on all-fours, each with a tube attached to his penis. They moo in pleasure.
	<<elseif $malechance lte 0>>
		Inside, a number of cowgirls stand on all-fours, each with two tubes attached to her breasts. They moo in pleasure.
	<<else>>
		Inside, a number of cowgirls and bullboys stand on all-fours, each attached to a milking machine. They moo in pleasure.
	<</if>>

<<else>>
	<<if $malechance gte 100>>
		A number of bulls stand inside, each attached to a milking machine.
	<<elseif $malechance lte 0>>
		A number of cows stand inside, each attached to a milking machine.
	<<else>>
		A number of cattle stand inside, each attached to a milking machine.
	<</if>>
	
<</if>>

"They need milking three times a day," Alex says. "They prefer to be milked by hand, but it's too time-consuming."
<br><br>

"I'll be back in a jiffy," <<he>> says to the cattle. "Just showing our new friend around." You follow <<him>> outside.
<br><br>

<<link [[Next|Farm Intro 7]]>><</link>>
<br>