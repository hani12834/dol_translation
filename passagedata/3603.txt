<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
<<if $phase is 1>>
	<<set $phase to 0>>
	You sit on a pew and bow your head. <i>Prayer effectiveness depends on purity.</i>
	<br><br>
<<elseif $phase is 2>>
	<<set $phase to 0>>
	You pray for salvation. Your thoughts drift to your hopes for the future, and how things could be better.
	<br><br>
	An hour passes, and you feel less burdened.
	<br><br>
<<elseif $phase is 3>>
	<<set $phase to 0>>
	You open one eye and examine the room. There are others like you, praying in the pews. <<if $NPCName[$NPCNameList.indexOf("Jordan")].pronoun is "m">>Monks<<else>>Nuns<</if>> prowl the aisles, passing furtive messages to one another. There are many more than you'd expect. You count <<print random(22, 34)>> coming and going.
	<br><br>
	<<if $awareness gte 400>>
		You glance up at the shadowed ceiling, at the gaping abyss perched above. Terror shivers up your spine. Your gaze is forced away.
		<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
		<br><br>
	<</if>>
<<elseif $phase is 4>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You pray for a bigger penis.
	<br><br>

	<br><br>
	<<if $acceptance_penis_tiny gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Tiny Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 5>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You pray for a bigger penis.
	<br><br>
	An hour passes, and your penis is no bigger.
	<br><br>
	<<if $acceptance_penis_small gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Small Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 6>>
	<<set $phase to 0>>
	Your thoughts turn to your <<peniscomma>> and the mocking way people regard it. The torment returns. You pray for a smaller penis.
	<br><br>
	An hour passes, and your penis is no smaller.
	<br><br>
	<<if $acceptance_penis_big gte 1000>>
		Realisation dawns as you lean back. Your <<penis>> is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Big Penis</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 7>>
	<<set $phase to 0>>
	Your thoughts turn to your flat chest, and the mocking way people regard it. The torment returns. You pray for bigger breasts.
	<br><br>
	An hour passes, and your breasts are no bigger.
	<br><br>
	<<if $acceptance_breasts_tiny gte 1000>>
		Realisation dawns as you lean back. Your chest is beautiful. Why should you care what other people say about it? You've gained the <span class="green">Acceptance: Tiny Breasts</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 8>>
	<<set $phase to 0>>
	Your thoughts turn to your <<breastscomma>> and the mocking way people regard them. The torment returns. You try to remain detached.
	<br><br>
	An hour passes, and for a few moments the weight of scrutiny lifts.
	<br><br>
	<<if $acceptance_breasts_small gte 1000>>
		Realisation dawns as you lean back. Your <<breasts>> are beautiful. Why should you care what other people say about them? You've gained the <span class="green">Acceptance: Small Breasts</span> trait.
		<br><br>
	<</if>>
<<elseif $phase is 9>>
	<<set $phase to 0>>
	Your thoughts turn to your <<breastscomma>> and the mocking way people regard them. The torment returns. You pray for smaller breasts.
	<br><br>
	An hour passes, and your breasts are no smaller.
	<br><br>
	<<if $acceptance_breasts_big gte 1000>>
		Realisation dawns as you lean back. Your <<breasts>> are beautiful. Why should you care what other people say about them? You've gained the <span class="green">Acceptance: Big Breasts</span> trait.
		<br><br>
	<</if>>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte $stressmax>>
	<<passouttemple>>
<<elseif $danger gte (9900 - $allure) and $eventskip isnot 1>>
	<<if $rng gte 81>>
		<<generate1>><<person1>> A <<monk>> carrying a donation plate walks by. <<He>> smiles at you.
		<br><br>

		<<link [[Look away|Temple Plate Look]]>><</link>>
		<br>
		<<if $money gte 100>>
			<<link [[Make a small donation (£1)|Temple Plate Donation]]>><<set $phase to 0>><<set $money -= 100>><<trauma -6>><<famegood 1>><</link>><<ltrauma>>
			<br>
		<</if>>
		<<if $money gte 500>>
			<<link [[Make a reasonable donation (£5)|Temple Plate Donation]]>><<set $phase to 1>><<set $money -= 500>><<trauma -12>><<famegood 2>><</link>><<lltrauma>>
			<br>
		<</if>>
		<<if $money gte 2500>>
			<<link [[Make a large donation (£25)|Temple Plate Donation]]>><<set $phase to 2>><<set $money -= 2500>><<trauma -18>><<famegood 3>><</link>><<llltrauma>>
			<br>
		<</if>>
		<<set $skulduggerydifficulty to 100>>
		<<link [[Steal|Temple Plate Steal]]>><<crimeup 5>><</link>><<crime>><<skulduggerydifficulty>>
		<br>

	<<elseif $rng gte 71>>
		<<generate1>><<generatey2>>A door at the side of the temple creaks ajar. Through the gap you see a <<person2>><<persons>> head, lying on a table. <<Hes>> wearing a blindfold and gag.
		<br><br>

		You hear a solid whack, and the <<person>> jerks and winces. A moment later a stern-looking <<person1>><<monk>> appears at the gap, blocking your view. <<He>> closes the door.
		<br><br>
		<<endevent>>
		<<prayoptions>>

	<<elseif $rng gte 61>>
		<<generate1>><<generatey2>>You hear a <<person1>><<monk>> speak behind you. "... We said there'd be consequences for breaking your oaths," <<he>> says, addressing someone behind <<himstop>> "Now stand up straight." <<He>> holds a silk cord that trails after <<himstop>> It binds the wrists of a younger <<monkstop>>
		<br><br>

		The junior <<monk>> wears a blindfold. <<His>> mouth is gagged by another span of cloth. The elder monk directs <<his>> steps by tugging on the cord. "Once you've faced penance..." you make out before they pass through one of the side doors, disappearing from view and earshot.
		<br><br>
		<<endevent>>
		<<prayoptions>>

	<<elseif $rng gte 41>>
		<<generatey1>><<person1>>A young <<monk>> walks by, swinging a sweet-smelling thurible in <<his>> hands.
		<br><br>

		<<if $worn.upper.type.includes("naked") and $worn.under_upper.type.includes("naked")>>
			<<He>> glances at your bare <<breasts>>, then looks away and blushes.
		<<else>>
			<<He>> glances at you, then looks away and blushes.
		<</if>>
		<br><br>

		<<if $player.gender_appearance is "f" or $breastsize gte 3 and (!$worn.upper.type.includes("naked") or !$worn.under_upper.type.includes("naked"))>>
			<<if $exhibitionism gte 55>>
			<<link [[Flash chest|Temple Thurible Flash]]>><</link>><<exhibitionist4>>
			<br>
			<</if>>
		<</if>>

		<<link [[Chat|Temple Thurible Chat]]>><</link>>
		<br>
		<<link [[Ignore|Temple Thurible Ignore]]>><</link>>
		<br>

	<<elseif $rng gte 21>>

	<<prayoptions>>

	<<else>>
		<<generate1>><<person1>>A <<person>> dressed as a <<monk>> sits close beside you. Very close. <<He>> rests a hand on your thigh. "Don't cause a fuss," <<he>> whispers. "Or I'll say you attacked me. Who will they believe?"
		<br><br>
		<<if $angel gte 6>>
		<<link [[Guilt|Temple Guilt]]>><</link>> | <span class="gold">Angel</span>
		<br>
		<</if>>
		<<link [[Endure|Temple Rape]]>><<set $molestationstart to 1>><</link>>
		<br>
	<</if>>
<<else>>
	<<prayoptions>>
<</if>>
<<set $eventskip to 0>>