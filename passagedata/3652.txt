<<if $fightstart is 1>>
<<set $fightstart to 0>>

<<neutral 1>>

<<maninit>>
<<set $enemytrust -= 100>>
<<set $enemyanger += 200>>
<<npcidlegenitals>>
"You will be purified," the <<monk>> says. They form a circle around you, preventing escape.
<br><br>
<</if>>

<<effects>>
<<effectsman>><<man>>

<<stateman>>
<br><br>
<<actionsman>>

<<if $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Temple Arcade Fight Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Temple Arcade Fight Finish]]>><</link>></span><<nexttext>>
<<elseif $pain gte 100 and $willpowerpain is 0>>
	<span id="next"><<link [[Next|Temple Arcade Fight Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Temple Arcade Fight]]>><</link>></span><<nexttext>>
<</if>>