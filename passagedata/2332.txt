<<set $outside to 0>><<set $location to "museum">><<effects>>

<<if $museumintro isnot 1>>
	<<set $museumintro to 1>><<set $phase to 0>>

	<<npc Winter>><<person1>>You enter the dim museum. Small windows peek down from just beneath the high ceiling, doing little to break the gloom. The other rooms are no better.
	<br><br>

	Winter stands polishing a brass pot. <<He>> regards you with a level stare. "Don't touch anything," <<he>> says. "I doubt you've washed your hands."
	<br><br>

	<<link [[Respond indignantly|Museum Intro]]>><<set $submissive -= 1>><<set $phase to 0>><</link>>
	<br>
	<<link [[Respond obediently|Museum Intro]]>><<set $submissive += 1>><<set $phase to 1>><<npcincr Winter love 1>><</link>><<glove>>
	<br>
	<<link [[Respond flirtatiously|Museum Intro]]>><<set $phase to 2>><<npcincr Winter love -1>><</link>><<promiscuous1>><<llove>>
	<br>

<<elseif $museumhorseintro is undefined and $museumAntiques.museumCount gte 10>>
	<<set $museumhorseintro to 0>>
	<<npc Winter>><<person1>>You enter the dim museum. Winter stares at a brass pot sitting alone on a display. "I can't find a place for it," <<he>> sighs. <<He>> notices you. "I was hoping you'd visit. I have a favour to ask, seeing as you've been such a help already." <<He>> walks to the corner of the room, where something large hides beneath a dirty sheet. <<He>> tugs it off, revealing a wooden horse.
	<br><br>

	"It's genuine," <<he>> says with satisfaction. "And in good condition."
	<br><br>

	<<if $awareness gte 200>>
		"It's a torture device," you say.
		<br><br>

		"I'm impressed." <<he>> responds. "That's exactly what it is. They would sit the victim on top, and weigh them down to put pressure on their crotch. They'd often be whipped at the same time."
	<<else>>
		"What is it for?" you ask.
		<br><br>

		"Torture," <<he>> responds. "Modern examples exist for... other uses. They would sit the victim on top, and weigh them down to put pressure on their crotch. They'd often be whipped at the same time."
	<</if>>
	<br><br>
	<<He>> rests a hand on the horse. "I want to stage a demonstration, to get people interested in history." <<He>> regards you. "I don't want you to feel pushed into it, but I'd like your help. Give it some thought."
	<br><br>

	<<link [[Next|Museum]]>><<endevent>><</link>>
	<br>
<<elseif $museumduckintro is undefined and $museumAntiques.museumCount gte 20>>
	<<set $museumduckintro to 0>>
	<<npc Winter>><<generate2>><<person2>>You enter the dim museum. A <<person>> brushes past you.
	<br><br>
	<<person1>>
	"Just who I wanted to see," Winter says, looking up from a dark brown wooden chair. "I've been speaking with an interesting <<person2>><<if $pronoun is "m">>gentleman<<else>>lady<</if>>. You might have passed <<him>> on the way in."<<person1>>
	<br><br>
	
	<<person1>>Winter places a finger on the chair beside <<him>>. "I've received a donation. Something rather special."
	<br><br>
	
	<<if $awareness gte 300>>
		You look closer. The chair has a hook at the back, as if it were meant to be tied to something. "It's a ducking stool," you say. "They'd use it to suffocate people underwater."
		<br><br>
		
		"I'm very impressed," <<he>> responds. "That's it precisely. They'd tie the chair to a... crane of sorts. Then dunk the victim in a river. It could be fatal, depending."
	<<else>>
		"What is it for?" you ask.
		<br><br>
		
		"Punishment," <<he>> responds. "It's called a ducking stool. See this hook at the back? They'd tie it to a crane with a victim strapped to the seat. Then suffocate said victim in water. It could be fatal."
	<</if>>
	<br><br>
	"I want to demonstrate it in public," <<he>> continues. "I've found someone who can provide the rest of the apparatus, and there's a river nearby. I don't presume anything, but I would like your help. Give it some thought."
	<br><br>
	
	<<link [[Next|Museum]]>><<endevent>><</link>>
	<br>
<<else>>
	You are inside the museum. Items of historic value cover the walls and perch on pedestals.
	<<if $museuminterest gte 100>>
		Visitors crowd the building, examining the antiques with interest. Winter looks overworked, but content.
	<<elseif $museuminterest gte 60>>
		The building is full of visitors, here to examine the antiques.
	<<elseif $museuminterest gte 20>>
		Several other visitors examine the displays.
	<<else>>
		The building is empty of visitors, save yourself.
	<</if>>
	<br><br>

	<<if $museumhorseintro is 0>>
		<<link [[Agree to wood horse demonstration|Museum Horse Agree]]>><<set $museumhorseintro to 1>><</link>>
		<br>
	<<elseif $museumhorseintro is 1 and $museumhorse isnot 1>>
		<<link [[Demonstrate wooden horse (1:00)|Museum Horse]]>><<pass 60>><<set $museumhorse to 1>><</link>><<gpain>><<exhibitionist1>>
		<br>
	<</if>>
	<<if $museumduckintro is 0>>
		<<link [[Agree to ducking stool demonstration|Museum Duck Agree]]>><<set $museumduckintro to 1>><</link>>
		<br>
	<<elseif $museumduckintro is 1 and $museumduck isnot 1>>
		<<link [[Demonstrate ducking stool (1:00)|Museum Duck]]>><<pass 60>><<set $museumduck to 1>><</link>><<exhibitionist1>>
		<br>
	<</if>>
	<<museumtalk>>
	<<if $museumAntiques.antiques.antiqueforestgem is "museum" and $museumgreengemtouch isnot 1>>
		<<link [[Touch green gem|Museum Green Gem Touch]]>><<stress -3>><<set $museumgreengemtouch to 1>><</link>><<lstress>>
		<br>
	<</if>>
	<<if $antiquemoney gt 0>>
		<<link [[Sell antiques|Museum Sell]]>><<if $museumAntiques.antiques.antiquesilvermask is "found">><<set $phase to 1>><</if>><<museumdonate>><</link>>
		<br>
	<</if>>
	<<link [["Look around the Museum"|Museum Look around]]>><</link>>
	<br><br>

	<<link [[Leave|Oxford Street]]>><</link>>
	<br>

<</if>>