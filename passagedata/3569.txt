<<set $outside to 1>><<set $location to "temple">><<temple_effects>><<effects>>

You sip the drink. It's bitter, but fruity. "Good right?" the <<monk>> says. <<He>> continues without waiting for an answer. "We make harder stuff too, but we don't sell it. Used in rituals. They make you drink it when becoming a <<monkcomma>> but only the priests and bishop know what's in it."
<br><br>
You finish the drink as the <<monk>> speaks, until the mug is empty. "Thanks for the help," <<he>> says.
<br><br>

<<endevent>>

<<link [[Next|Temple Garden]]>><</link>>
<br>