<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<if $rng gte 71>>
	Harper leads you and the other patients indoors, to the showers. "The water should be on," <<he>> says. "Hygiene is important. I want to see everyone wash."
	<br><br>
	<<He>> rests a hand on your shoulder as the other patients strip. "As I said, hygiene is important. You wouldn't mind if I take care to personally ensure yours is up to standard?"
	<br><br>
	<<link [[Accept|Asylum Shower Accept]]>><<strip>><<wash>><<stress 6>><<arousal 1800>><<set $suspicion -= 1>><<npcincr Harper love 1>><</link>><<exhibitionist1>><<gstress>><<garousal>><<lsuspicion>>
	<br>
	<<link [[Refuse|Asylum Shower Refuse]]>><<set $suspicion += 1>><<npcincr Harper love -1>><</link>><<gsuspicion>>
	<br>
<<else>>
	Harper leads you and the other patients indoors, to the showers. "The water should be on," <<he>> says. "Hygiene is important. I want to see everyone wash."
	<br><br>
	Most of the patients are used to this, and strip without a problem. Some hesitate. Harper sends the orderlies to help them out of their clothes.
	<br><br>
	<<link [[Strip and find somewhere secluded|Asylum Shower Secluded]]>><<strip>><<wash>><</link>><<exhibitionist1>>
	<br>
	<<if $exhibitionism gte 15>>
		<<link [[Strip and wash around others|Asylum Shower Wash]]>><<strip>><<wash>><</link>><<exhibitionist2>>
		<br>
	<</if>>
	<<link [[Sneak away|Asylum]]>><<endevent>><<suspicion 5>><</link>><<ggsuspicion>>
	<br>
<</if>>