<<set $outside to 0>><<set $location to "town">><<dockeffects>><<effects>>

<<if $phase gte 1>>
You hand the <<person>> the ticket and £<<print $dockslavemoney>>.
<<else>>
You hand the <<person>> the ticket.
<</if>>

<<if $submissive gte 1150>>
"It was nice to meet you," you say. "I hope you have a nice journey."
<<elseif $submissive lte 850>>
"You should be okay from here," you say.
<<else>>
"This train will take you home," you say.
<</if>>

<<He>> seems reluctant to leave you, but <<he>> understands and walks alone toward the waiting train. Before reaching it however, <<he>> turns and runs back. <<He>> almost knocks you over with a hug.
<br><br>

<<He>> waves from the window as the train departs.
<br><br>

<<endevent>>
<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>
<br>