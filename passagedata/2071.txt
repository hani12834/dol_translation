<<set $outside to 0>><<set $location to "home">><<effects>>
<<fameexhibitionism 20>>
Wanting to avoid any jealousy, you throw open the door and stride into the hallway. The small crowd parts to let you through. You take your time, and pretend to be on a catwalk. Their eyes are locked onto your <<lewdnessstop>> The hallway isn't wide enough for three people, so you rub against everyone as you move.
<br><br>
"Do you like what you see?" you say as you twirl at the end of the hallway. You make your way back to your room, and blow a quick kiss over your shoulder before closing the door behind you. Your heart races as you listen to their excitement through the door.
<br><br>
<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>