<<effects>>

<<if $steed is "male" or $steed is "female">>
	<<if $submissive gte 1150>>
		"Pl-please don't hurt me," you say, trying to remain calm. "I didn't do anything to hurt you."
	<<elseif $submissive lte 850>>
		"Knock if off," you say, trying to remain calm. "Or I'll knock some sense into you."
	<<else>>
		"Please calm down," you say, trying to remain calm yourself. "I mean you no harm."
	<</if>>
<<else>>
	You try to remain calm, and make soothing noises as the beast approaches.
<</if>>

<<if $tending gte random(600, 1200)>>
	<span class="green">The <<steed_text>> stops.</span>
	<<if $steed is "male" or $steed is "female">>
		<<steed_He>> raises a palm to <<steed_his>> face and shakes <<steed_his>> head. "I'm sorry," <<steed_he>> says. "I don't know what came over me."
	<<else>>
		<<steed_He>> shakes <<steed_his>> head. The malice is gone.
	<</if>>
	<br><br>

	You ride back to the others. You hear voices calling as you draw closer, until you crest a ridge and see the rest of the procession. Remy rides over to make sure you and your steed are okay.
	<br><br>

	<<link [[Next|Riding School Lesson]]>><</link>>
	<br>
<<else>>
	<span class="red">The <<steed_text>> charges,</span> knocking you down and taking position above you.
	<br><br>

	<<link [[Next|Riding School Horse Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>