<<set $outside to 1>><<effects>>
You curl up and slam into the mixer. Fortunately, the cement cushions your blow. Unfortunately, it's cement. You manage to struggle free,
<<if $worn.under_lower.name is "naked" and $worn.lower.name is "naked" and $worn.upper.name is "naked">>
	stumbling away without attractive notice.
	<br><br>
<<else>>
	but lose your clothes in the process.
	<br><br>
	<<underruined>>
	<<lowerruined>>
	<<upperruined>>
<</if>>
<<parkquick>>