<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $uncomfortable.underwear is false>>
You proudly walk out from behind the screen, the clothes barely hiding your <<genitalsstop>> The class falls silent as they see how little you're wearing.
		<<if $fameexhibitionism gte 400>> You doubt this is the first time any of them have seen you so thinly attired.<</if>>
		<br><br>
		Winter smiles in satisfaction. You model the outfit as <<he>> describes the ceremonies it was used in, which all seem to have had a risqué element. You welcome your classmates' looks and continue posing in an inviting way. The segment is over sooner than you'd like.
<<else>>
You <<nervously>> walk out from behind the screen. The class falls silent as they see how little you're wearing. Winter smiles in satisfaction. You model the outfit as <<he>> describes the ceremonies it was used in, which all seem to have had a risqué element. You're too embarrassed by the way your classmates look at you to learn anything, but the segment is over soon enough.
<</if>>
<br><br>
<<link [[Next|History Lesson]]>><<endevent>><</link>>
<br>