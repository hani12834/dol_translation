<<effects>>

You reach out and pet the mastiff's head. His ears twitch a little, but he's no less accepting of you than he is the other orphans.
<br><br>

The <<person>> returns. "I spoke to his owner," <<he>> says. "They're on the way."
<br><br>

More orphans arrive to investigate, each wanting a turn at petting. The <<person2>><<person>> brings a bowl of water. Max is eager to drink. "I knew he'd be thirsty," <<he>> says.
<br><br>

This continues a while, until the <<person1>><<person>> looks out the window. "His owner's here," <<he>> says. You hear voices outside. Max recognises at least one of them, and rises to his feet, tail wagging.
<br><br>

Everyone crowds around the windows to watch as the <<person>> leads Max to his owner.<<endevent>><<generateyv1>><<person1>> A younger orphan tugs your sleeve. "Will Bailey let us have a dog?" <<he>> asks. The other orphans are listening.
<br><br>

<<link [[Express Doubt|Home Max Doubt]]>><<hope -1>><<reb 1>><</link>><<lhope>><<greb>>
<br>
<<link [[Express Hope|Home Max Hope]]>><<hope 1>><<reb -1>><</link>><<lreb>><<ghope>>
<br>