<<pass 30>><<set $outside to 1>><<set $location to "beach">><<effects>>

You run along the shore.
<<if $daystate is "night">>
	<<if $weather is "rain">>
		The sound of the waves crashing competes with the torrential rain.
	<<elseif $weather is "clear">>
		The cold night breeze invigorates you.
	<<elseif $weather is "overcast">>
		The cool night breeze feels pleasant against your skin.
	<</if>>
<<else>>
	<<if $weather is "rain">>
		Shards of rain assault you as you jog across the wet sand.
	<<elseif $weather is "clear">>
		The sun's intensity wears you down, tiring you out.
		<<tiredness 6>><<gtiredness>>
	<<elseif $weather is "overcast">>
		The cool weather makes for a pleasant jog.
	<</if>>
<</if>>
<<physique 3>><<tiredness 6>><<stress -6>>
<br><br>

<<link [[Next|Beach]]>><</link>>