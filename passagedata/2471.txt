<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">><<set $phase to 1>>
<<npc Landry>><<person1>>
<<if $police_hack is 1>>
	"Hacking a Police computer?!" Landry nods, reclining. "I like your style. But if you're looking for my help you're out of luck.
	Never been into all that cyber stuff."
	<br><br>
	Landry sits forward, rubs <<his>> chin thoughtfully. "Times change though. And they all keep telling me that's where the money is nowadays. Tell you what -
	they say there's this kid in town, <span class="teal">some orphan kid at the home on Domus.</span> Some kind of computers whiz. Makes stuff, breaks stuff.
	I think the kid's called Mickey or McKay or something like that."
	<br><br>
	You think of the other orphans. One face leaps to mind.
	<br>
	"Whatever," Landry sits back. "Best hope is to go to the orphanage on Domus and find this kid. Just watch out. Person who runs the place is called Bailey.
	Nasty piece of work. Makes Briar look like a bleeding-heart humanitarian. You know Briar? Well anyway, Bailey's protective. If you get caught, you're on your own.
	Bailey's connected. My name won't help. But if you can get this kid to come work with me there'll be money in it for you."
	<br><br>

	<<link [[Deal|Pub Hack Deal]]>><<set $police_hack to 2>><<set $pub_hack_job to 1>><<set $phase to 1>><</link>>
	<br>
	<<link [[No Deal|Pub Hack Deal]]>><<set $phase to 2>><</link>>
	<br>
<<elseif $police_hack is 4>>
	"Sure," Landry says. "Give me 10 minutes."
	<br><br>

	You watch the other bar customers for a while. Landry soon returns.
	<br>
	"Here you are," Landry says. "One new police master password. And our mutual friend says hi."
	<br><br>
	<<set $police_hack to 5>>
	<<link [[Leave|Pub]]>><<endevent>><</link>>
	<br>
<<else>>/*Was elseif $pub_hack_job is 4*/
	"Yeah, the kid turned up already," Landry rubs <<his>> head. "Strange one. Won't even tell me his name? Her name? I can't even tell. Smart as you like though.
	Said you'd have a bunch of Esty cards for me to take?"
	<br>
	You hand over the SD memory cards. In return Landry gives you an envelope. Inside is money and a card with a word scrawled on it.
	<br><br>
	"Thanks. And that there's the police master password along with a £500 thank you for bringing in some talent. I may have new kinds of work in the future.
	There's also this."
	<br>
	Landry hands you a USB stick.
	<br><br>

	"Some kind of virus. Spreads over an eye-pee network? Wipes every computer clean. To quote our mutual friend: 'it will most certainly ruin someone's day.'
	Should be able to cause some trouble with that one. Or make old trouble disappear. Right? Maybe next thing I'll be asking you to clear up my past misdeeds.
	Happy hunting."
	<br><br>
	<<set $money += 50000>><<set $police_hack to 5>>
	<<link [[Leave|Pub]]>><<endevent>><</link>>
	<br>
<</if>>