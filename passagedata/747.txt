<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

	<<beastejaculation>>

	<<if $farm_work.horse.monster is true>>
		"Don't forget," the centaur says as <<farm_he horse>> steps away. "You're just a toy." The pair run into the field.
	<<else>>
		The horses run into the field.
	<</if>>
	<<lrespect>><<farm_horses -1>><<gfarm>><<farm_yield 1>><<set $farm_work.horses_satisfied += 1>>
	<br><br>

	<<tearful>> you steady your feet. You close the gate.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>

	<<if $farm_work.horse.monster is true>>
		The centaur backs away. "What's wrong?" the other chuckles. "This little <<girl>> too much for you?"
		<br><br>
		
		"I don't see you having a go," the centaur responds. "I'm thirsty anyway." It runs for the field, and the other follows.
	<<else>>
		The horse backs away from you, and runs for the field. The other follows.
	<</if>>
	<<grespect>><<farm_horses 1>>
	<br><br>

	<<tearful>> you steady your feet. You close the gate.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<else>><<set $rescued += 1>>

	
	"I'm coming!" It's Alex.
	<<if $farm_work.horse.monster is true>>
		The centaur backs up at the sound. "Fuck," <<farm_he horse>> says, running for the field and pointing at the other. "It was <<farm_him horse>>!"
	<<else>>
		The horse backs up at the sound. Both run for the field.
	<</if>>
	<<lrespect>><<gdom>><<farm_horses -1>><<npcincr Alex dom 1>>
	<br><br>
	
	<<clotheson>>
	<<endcombat>>
	
	<<npc Alex>><<person1>>
	Alex arrives at the stable, and runs along the lane. "You okay?" <<he>> asks. "You don't want to lose control around the horses." 
	<<if $exposed gte 1 and $farm_naked isnot 1>>
		<<He>> helps you steady yourself, and offers some towels to cover with.
	<<else>>
		<<He>> helps you steady yourself.
	<</if>>
	<<if $exposed gte 1>>
		<<glust>><<npcincr Alex lust 1>>
	<</if>>
	<br><br>

	<<tearful>> you close the gate. "Leave the horses to me if you like," Alex adds. "For all their stubbornness, the pigs can be easier to manage."
	<br><br>

	<<He>> returns to <<his>> work.
	<br><br>

	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>

<</if>>