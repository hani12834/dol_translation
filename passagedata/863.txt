<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	They lie on the grass, their strength robbed for now. <<tearful>> you seize the opportunity. You grasp one of the axes, and pull it free.
	<br><br>
	Left drained by their orgasms, and their opponent now wielding an axe, the pair decide to turn and run.<<llaggro>><<farm_aggro -5>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>
	<<earnFeat "Farm Protector">>
	The <<person1>><<person>> falls into a ditch. The <<person2>><<person>> goes for <<his>> axe, but you kick <<his>> shin and shove <<him>> after <<his>> colleague.
	<br><br>
	<<tearful>> you plant your feet. They might come back for more. Instead, they scramble up the other side of the ditch, and run.<<llaggro>><<farm_aggro -5>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>
	
	<<endcombat>>
	<<npc Alex>><<person1>>
	"I'm coming!" It's Alex. The pair glance in the direction of <<his>> voice, then attack the fence with more energy, keen to inflict as much damage as possible.<<farm_aggro 5>><<set $farm_work.fence_damage += 1>><<gaggro>>
	<br><br>

	"You scum," Alex says, rushing over as the pair turn and run. "I'll feed you to my fucking pigs when I catch you!"
	<br><br>
	<<clotheson>>
	"It's a good thing you were nearby," <<he>> says, looking at the damage. "We should be able to patch this up." <<He>> removes <<his>> hat and wipes sweat from <<his>> brow.<<npc Alex dom 1>><<npc Alex love 1>> | <span class="purple">+ Alex's Dominance</span><<glove>>
	<br><br>
	
	<<if $exposed gte 1 and $farm_naked isnot 1>>
		<<covered>><<He>> notices your state of dress, and blushes. "L-Let me get you some towels," <<he>> says.
		<br><br>
		
		You follow <<him>> back to the farmhouse, where <<he>> hands you some towels.<<towelup>><<gdom>><<glust>><<npcincr Alex lust 1>><<npcincr Alex dom 1>>
		<br><br>
	<</if>>

	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>

<<else>>
	Too hurt to continue, you slump to the ground. The pair retrieve their axes and continue their work, until an area of fence wide enough to fit a cow is reduced to splinters.
	<br><br>
	Their job done, the pair swagger away from the farm.<<ggaggro>><<farm_aggro 10>><<set $farm_work.fence_damage += 3>>
	<br><br>
	<<tearful>> you struggle to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Farm Work]]>><</link>>

<</if>>