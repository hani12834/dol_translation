<<set $outside to 0>><<set $location to "town">><<dockeffects>><<effects>>

<<upperruined>><<lowerruined>><<underruined>>
<<set $leftarm to "bound">><<set $rightarm to "bound">>
You awaken naked on a chair. Lengths of rope criss-cross your body and tie you down. A round board sits on your knees and leans against your chest, giving you some cover from prying eyes. You can't move an inch. Walls of corrugated metal suggest you're inside a cargo container. You're not alone.
<br><br>

<<generate1>><<generate2>><<generate3>><<person2>>"It's not fair," a <<person>> says. <<He>> sits opposite you with <<his>> arms folded. "You set it up wrong."
<br><br>

A dart thuds into the board. The <<person1>><<person>> who threw it laughs. "Yeah yeah," <<he>> says. "We're all using the same board, if you haven't noticed." <<He>> throws another dart. "I win." <<He>> turns to a <<generate3>><<person3>><<personstop>> "Unless you score perfect."
<br><br>

The <<person>> stands and takes <<his>> position in front of the board as the <<person1>><<person>> approaches to retrieve <<his>> darts. "Hey," <<he>> says. "<<pShes>> awake."
<br><br>

<<link [[Insult|Passout Docks 2]]>><<set $phase to 0>><<def 1>><<trauma -6>><</link>><<ltrauma>>
<br>
<<link [[Plead|Passout Docks 2]]>><<set $phase to 1>><<sub 1>><<stress -6>><</link>><<lstress>>
<br>
<<link [[Remain silent|Passout Docks 2]]>><<set $phase to 2>><</link>>
<br>