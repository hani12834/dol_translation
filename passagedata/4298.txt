<<effects>>
"Some other time," You say.
<br><br>

"Your loss," Whitney says. "You're lucky I give you a choice." <<He>> fixes <<his>> clothes and walks from the alleyway.
<br><br>

<<clotheson>>
<<endevent>>
<<set $eventskip to 1>>
<<link [[Next|Connudatus Street]]>><</link>>
<br>