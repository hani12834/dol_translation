<<set $outside to 1>><<set $location to "town">><<effects>>
<<if $skulduggery lte 300>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">There's nothing more you can learn from such an easy target.</span>
<</if>>
You grab the <<wallet>> as you walk past.
<<if $rng gte 61>>
	The strap tenses and holds. The <<person>> turns and catches you red-handed. "You little brat," <<he>> says as <<he>> grabs your shoulder. "I'll teach you some respect."
	<br><br>
	<<link [[Next|Connudatus Snatch Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	It breaks free of the strap. You walk away, not changing your pace.
	<br><br>
	<<connudatuswallet>>
	<<endevent>>
<</if>>