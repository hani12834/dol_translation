<<set $outside to 1>><<set $location to "town">><<effects>>

<<npc Robin>><<person1>>You run up to Robin.

<<if $robinromance is 1>>
<<He>> hugs you. "Can we hold hands?" <<he>> says.
<br><br>

Together you walk back to the orphanage, chatting as you go.
<<ltrauma>><<lstress>><<trauma -2>><<stress -4>>
<br><br>

<<else>>

	<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 80>>
	<<He>> smiles when <<he>> sees you. "H-hey," <<he>> says.
	<br><br>

	Together you walk back to the orphanage. <<He>> limps, and you move slowly to let <<him>> keep up. <<He>> doesn't seem keen on talking, and stays very close to you whenever other people are around.
	<br><br>

	<<elseif $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 40>>
	<<He>> smiles when <<he>> sees you. "Hey," <<he>> says. "I'm glad you're here. I don't feel safe walking home alone."
	<br><br>

	Together you walk back to the orphanage. You chat a bit, but <<he>> tenses and falls silent whenever other people are around.
	<br><br>

	<<else>>
	<<He>> smiles when <<he>> sees you. "Hey," <<he>> says. "I'm glad you're here. I don't like walking home alone."
	<br><br>

	Together you walk back to the orphanage, chatting as you go.
	<<ltrauma>><<lstress>><<trauma -2>><<stress -4>>
	<br><br>

	<</if>>

<</if>>
<<npcincr Robin love 1>>
<<endevent>>
<<link [[Next|Orphanage]]>><</link>>
<br>