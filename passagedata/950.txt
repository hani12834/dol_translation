<<effects>>
<<if $daystate is "night" and random(1, 5) lte 4>>
	You stand at the side of the road and wait for a car to pass. No one drives by. Few travel the countryside at night.
	<br><br>

	<<destinationfarmroad>>
	<br>
<<else>>
	You stand at the side of the road and stick out your thumb.
	<br><br>
	<<hitchhike>>
<</if>>