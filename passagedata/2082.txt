<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $dev is 1>>

<<npc Bailey>><<person1>>You enter the main hall of the orphanage and line up with the other residents. The room falls silent as Bailey storms in, eyes livid. "Listen up," <<he>> shouts. "They're cutting our funding. And I'm not sacrificing anything for you shits." <<He>> runs a hand through <<his>> dark hair. "From now on, you're contributing. Financially."
<br><br>

<<He>> unfolds paper from <<his>> jacket pocket and examines it. "<span class="pink">£100</span> should be enough to start. For the first week. I don't care how you get it. Knock on doors and ask for work. Rent yourself as a footstool. Steal it even. Just have it a week from now. Or I'll find a way to extract value from you."
<br><br>

<<else>>

<<npc Bailey>><<person1>>

You enter Bailey's office. "I know why you're here," <<He>> says, running a hand through <<his>> dark hair. "You want me to release you from my protection, so you'll be an independent citizen. I could do that. But there's a problem. You've been living under my roof without giving anything in return. You owe me. Until you pay me back, I'm not letting you go."
<br><br>

<<He>> looks at the paper on <<his>> desk and smiles. "<span class="pink">£100</span> should do. To start with. I don't care how you get it. Knock on doors and ask for work. Rent yourself as a footstool. Steal it even. Just have it a week from now. Or I'll find a way to extract value from you."
<br><br>

<</if>>

You return to your tiny bedroom and wonder what to do.
<br><br>

<<set $rentmoney to 10000>>
<<set $renttime to 7>><<set $rentday to $weekday>>
<<set $rentstage to 1>>
<<endevent>>
<hr>
Attitudes
<br><br>
<<attitudes>>
<br>

<<link [[Next|Bedroom]]>><</link>>