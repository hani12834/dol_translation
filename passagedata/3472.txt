<<widget "eventsstreet">><<nobr>>

<<set $dangerevent to random(1, 100)>>
<<set $rng to random(1, 100)>>
<<if $kylarwatched is 1>>
<<kylarwatched>>
<<elseif $daystate is "night">>
	<<if $exposed gte 1>>
		<<if $bus is "residential">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<residentialex1>>
			<<elseif $dangerevent lte 20>>
				<<residentialex2>>
			<<else>>
				<<residentialex3>>
			<</if>>
		<<elseif $bus is "commercial">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<commercialex1>>
			<<elseif $dangerevent lte 20>>
				<<commercialex2>>
			<<else>>
				<<commercialex3>>
			<</if>>
		<<elseif $bus is "industrial">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<industrialex1>>
			<<elseif $dangerevent lte 20>>
				<<industrialex2>>
			<<else>>
				<<industrialex3>>
			<</if>>
		<<elseif $bus is "park">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<parkex1>>
			<<elseif $dangerevent lte 20>>
				<<parkex2>>
			<<else>>
				<<parkex3>>
			<</if>>
		<<else>>
			<<set $dangerevent to random(1, 60)>>
			<<if $dangerevent lte 20>>
				<<streetex1>>
			<<elseif $dangerevent lte 30>>
				<<streetex2>>
			<<elseif $dangerevent lte 40>>
				<<streetex3>>
			<<elseif $dangerevent lte 50>>
				<<streetex4>>
			<<elseif $dangerevent lte 60>>
				<<streetex5>>
			<</if>>
		<</if>>
	<<elseif $rng gte 51>>
		<<set $dangerevent to random(1, 150)>>
		<<if $dangerevent lte 10>>
			<<if $exhibitionismrun is undefined and $exhibitionismrunon isnot 1 and $days gte 8>>
				<<streetexoffer>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 20>>
			<<if $NPCName[$NPCNameList.indexOf("Whitney")].state is "dungeon" and $whitneyposter isnot 1>>
				<<whitneyposter>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 30>>
			<<if $hallucinations gte 2 and $tentacledisable is "f">>
				<<streettentacle>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 40>>
			<<if $famesex gte random(1, 1000)>>
				<<streetsexfame>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 50>>
			<<if $famerape gte random(1, 1000)>>
				<<streetrapefame>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 60>>
			<<if $famebestiality gte random(1, 1000)>>
				<<streetbestialityfame>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 70>>
			<<if $fameprostitution gte random(1, 1000)>>
				<<streetprostitutionfame>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 80>>
			<<if $fameexhibitionism gte random(1, 1000)>>
				<<streetexhibitionismfame>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 90>>
			<<if ($edenfreedom is 1 and $edendays gte 2) or ($edenfreedom gte 2 and $edendays gte 8)>>
				<<streeteden>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 100>>
			<<if $streetpolice isnot 1 and $crime gte 1000 and !$worn.lower.type.includes("naked")>>
				<<streetpolice>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 110>>
			<<if $worn.neck.collared is 1>>
				<<streetcollared>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 120>>
			<<if $leftarm is "bound" or $rightarm is "bound">>
				<<streetbound>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 130>>
			<<if $worn.lower.name is "towel" or $worn.lower.name is "large towel bottom">>
				<<streetlowertowel>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 140>>
			<<if $worn.upper.name is "towel" or $worn.upper.name is "large towel">>
				<<streetuppertowel>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<<elseif $dangerevent lte 150>>
			<<if $crime gte 5000>>
				<<streetwanted>>
			<<else>>
				<<eventsstreetnight>>
			<</if>>
		<</if>>
	<<else>>
		<<eventsstreetnight>>
	<</if>>
<<else>>
	<<if $exposed gte 1>>
		<<if $bus is "residential">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<residentialex1>>
			<<elseif $dangerevent lte 20>>
				<<residentialex2>>
			<<else>>
				<<residentialex3>>
			<</if>>
		<<elseif $bus is "commercial">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<commercialex1>>
			<<elseif $dangerevent lte 20>>
				<<commercialex2>>
			<<else>>
				<<commercialex3>>
			<</if>>
		<<elseif $bus is "industrial">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<industrialex1>>
			<<elseif $dangerevent lte 20>>
				<<industrialex2>>
			<<else>>
				<<industrialex3>>
			<</if>>
		<<elseif $bus is "park">>
			<<set $dangerevent to random(1, 30)>>
			<<if $dangerevent lte 10>>
				<<parkex1>>
			<<elseif $dangerevent lte 20>>
				<<parkex2>>
			<<else>>
				<<parkex3>>
			<</if>>
		<<else>>
			<<set $dangerevent to random(1, 20)>>
			<<if $dangerevent lte 10>>
				<<streetexday1>>
			<<elseif $dangerevent lte 20>>
				<<streetexday2>>
			<</if>>
		<</if>>

	<<elseif $averyseen isnot 1 and $weekday is 7 and $scienceproject is "won" and $daystate is "day" or $averyseen isnot 1 and $weekday is 7 and $scienceproject is "done" and $daystate is "day">><<set $averyseen to 1>>
	<<streetavery>>
	<<elseif $rng gte 52>>
		<<set $dangerevent to random(1, 170)>>
		<<if $dangerevent lte 10>>
			<<if $exhibitionismrun is undefined and $exhibitionismrunon isnot 1 and $days gte 8>>
				<<streetexoffer>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 20>>
			<<if $NPCName[$NPCNameList.indexOf("Whitney")].state is "dungeon" and $whitneyposter isnot 1>>
				<<whitneyposter>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 30>>
			<<if $hallucinations gte 2 and $tentacledisable is "f">>
				<<streettentacle>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 40>>
			<<if $famesex gte random(1, 1000)>>
				<<streetsexfame>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 50>>
			<<if $famerape gte random(1, 1000)>>
				<<streetrapefame>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 60>>
			<<if $famebestiality gte random(1, 1000)>>
				<<streetbestialityfame>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 70>>
			<<if $fameprostitution gte random(1, 1000)>>
				<<streetprostitutionfame>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 80>>
			<<if $fameexhibitionism gte random(1, 1000)>>
				<<streetexhibitionismfame>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 90>>
			<<if ($edenfreedom is 1 and $edendays gte 2) or ($edenfreedom gte 2 and $edendays gte 8)>>
				<<streeteden>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 100>>
			<<if $streetpolice isnot 1 and $crime gte 1000 and !$worn.lower.type.includes("naked")>>
				<<streetpolice>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 110>>
			<<if $worn.neck.collared is 1>>
				<<streetcollared>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 120>>
			<<if $leftarm is "bound" or $rightarm is "bound">>
				<<streetbound>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 130>>
			<<if $worn.lower.name is "towel" or $worn.lower.name is "large towel bottom">>
				<<streetlowertowel>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 140>>
			<<if $worn.upper.name is "towel" or $worn.upper.name is "large towel">>
				<<streetuppertowel>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 150>>
			<<bodywriting_exposure_check>>
			<<if _bodywriting_exposed is 1>>
				<<streetbodywriting>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 160>>
			<<bodywriting_exposure_check>>
			<<if _bodywriting_exposed is 1>>
				<<streetwhorebodywriting>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<<elseif $dangerevent lte 170>>
			<<if $crime gte 5000>>
				<<streetwanted>>
			<<else>>
				<<eventsstreetday>>
			<</if>>
		<</if>>
	<<else>>
		<<eventsstreetday>>
	<</if>>
<</if>>

<</nobr>><</widget>>

<<widget "eventsstreetday">><<nobr>>
<<set $dangerevent to random(1, 152)>>
<<set $rng to random(1, 100)>>
<<if $rng lte 10 and $worn.feet.type.includes("heels") and $feetskill lt $worn.feet.reveal>>
	<<streetheeltrip>>
<<elseif $rng gte 91>>
	<<street1>>
<<elseif $dangerevent lte 10>>
	<<streetfamerape>>
<<elseif $dangerevent lte 20>>
	<<streetbox>>
<<elseif $dangerevent lte 30>>
	<<street8>>
<<elseif $dangerevent lte 40>>
	<<street9>>
<<elseif $dangerevent lte 50>>
	<<street7>>
<<elseif $dangerevent lte 55>>
	<<street10>>
<<elseif $dangerevent lte 60>>
	<<streetrocks>>
<<elseif $dangerevent lte 65>>
	<<street6>>
<<elseif $dangerevent lte 75>>
	<<street5>>
<<elseif $dangerevent lte 85>>
	<<street4>>
<<elseif $dangerevent lte 95>>
	<<street2>>
<<elseif $dangerevent lte 96>>
	<<street3>>
<<elseif $dangerevent lte 106>>
	<<streetbullies>>
<<elseif $dangerevent lte 116>>
	<<streetfootbridge>>
<<elseif $dangerevent lte 126>>
	<<streetbottomgrope>>
<<elseif $dangerevent lte 132>>
	<<shadyFan>>
<<elseif $dangerevent lte 142>>
	<<streetvan>>
<<elseif $dangerevent lte 152>>
	<<streetfriendly1>>
<</if>>
<</nobr>><</widget>>

<<widget "eventsstreetnight">><<nobr>>
<<set $dangerevent to random(1, 131)>>
<<set $rng to random(1, 100)>>
<<if $rng lte 10 and $worn.feet.type.includes("heels") and $feetskill lt $worn.feet.reveal>>
	<<streetheeltrip>>
<<elseif $rng gte 91>>
	<<street1>>
<<elseif $dangerevent lte 10>>
	<<streetbox>>
<<elseif $dangerevent lte 20>>
	<<street8>>
<<elseif $dangerevent lte 30>>
	<<street9>>
<<elseif $dangerevent lte 40>>
	<<street7>>
<<elseif $dangerevent lte 45>>
	<<streetrocks>>
<<elseif $dangerevent lte 50>>
	<<street6>>
<<elseif $dangerevent lte 55>>
	<<street10>>
<<elseif $dangerevent lte 65>>
	<<street5>>
<<elseif $dangerevent lte 75>>
	<<street4>>
<<elseif $dangerevent lte 85>>
	<<streetfamerape>>
<<elseif $dangerevent lte 95>>
	<<street2>>
<<elseif $dangerevent lte 105>>
	<<street3>>
<<elseif $dangerevent lte 107>>
	<<streetnight1>>
<<elseif $dangerevent lte 109>>
	<<streetnight2>>
<<elseif $dangerevent lte 119>>
	<<streetbottomgrope>>
<<elseif $dangerevent lte 121>>
	<<streetlurker>>
<<elseif $dangerevent lte 131>>
	<<streetvan>>
<</if>>
<</nobr>><</widget>>

<<widget "streeteffects">><<nobr>>
	<<if $kylarstreettimer gte ($rng + 65) and $NPCName[$NPCNameList.indexOf("Kylar")].state is "active" and $kylarwatched isnot 1 and $exposed lte 0>>
		<<set $kylarstreettimer to 0>><<set $kylarwatchedtimer to 2>>
		You shiver, a warning from some primal instinct. <span class="blue">Something is watching you.</span>
		<<set $kylarwatched to 1>><<set $eventskip to 1>>
	<<elseif $kylarwatched is 1>>
		<<if $exposed gte 1 or $NPCName[$NPCNameList.indexOf("Kylar")].state isnot "active" or $kylarwatchedtimer lte 0>>
			<<set $kylarwatched to 0>>
		<<else>>
			<span class="blue">Something is watching you.</span>
		<</if>>
	<</if>>
<</nobr>><</widget>>

<<widget "loiter">><<nobr>>
	<<add_link "<<loitericon>><<link [[Loiter (0:15)|$passage]]>><<pass 15>><</link>><br>">>
	<<hideDisplay>>
<</nobr>><</widget>>