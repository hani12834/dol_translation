<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewerswaterfall">>
<<set $sewersevent -= 1>>
You are in the old sewers. Torrents of water crash down from multiple breaches in the ceiling.
<br><br>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
		<<link [[Hide behind the waterfalls|Sewers Waterfall Hide]]>><</link>>
		<br>
	<</if>>
	<<link [[Wet tunnel (0:05)|Sewers Lake]]>><<pass 5>><</link>>
	<br>
	<<link [[Swim with the current (0:01)|Sewers Industrial]]>><<pass 1>><<water>><</link>>
	<br><br>
<</if>>
<<set $eventskip to 0>>