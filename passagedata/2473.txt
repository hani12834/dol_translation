<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">>

You sit near the <<person>> and smile at <<himstop>>
<<if $rng gte 81>><<set $pubnpc to 1>>
<<He>> smiles back. You chat with <<him>> for a few minutes, laughing more loudly than you need to and making sure to mention you're single.
<<promiscuity1>>
<br><br>

"I'm a married <<if $pronoun is "m">>man<<else>>woman<</if>> myself," <<he>> says, but sounds sad about the fact.
<br><br>

	<<if $promiscuity gte 15>>
	<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
	<br>
		<<if $money gte 500>>
	<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<set $money -= 500>><<pass 20>><<set $pubdrink to 2000>><</link>>
	<br>
		<</if>>
	<</if>>
	<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 61>>
	<<set $pubnpc to 2>>
	<<He>> smiles back. You chat with <<him>> for a few minutes, laughing more loudly than you need to and making sure to mention you're single.
	<br><br>
	<<promiscuity1>>

	"Me too," <<he>> says. "I'm waiting for someone special."
	<br><br>

	<<if $promiscuity gte 15>>
		<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
		<br>
		<<if $money gte 500>>
			<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<pass 20>><<set $money -= 500>><<set $pubdrink to 2000>><</link>>
			<br>
		<</if>>
	<</if>>
	<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 41>>
	<<set $pubnpc to 3>>
	<<He>> glances at you once, then stares into <<his>> cup. You try to make small talk despite <<his>> lack of engagement. You make a point to mention that <<he>> looks good, and you're single.
	<<promiscuity1>>
	<br><br>

	<<He>> doesn't say anything, or even look up, but you can tell <<hes>> blushing.
	<br><br>

	<<if $promiscuity gte 15>>
		<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
		<br>
		<<if $money gte 500>>
			<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<pass 20>><<set $money -= 500>><<set $pubdrink to 2000>><</link>>
			<br>
		<</if>>
	<</if>>
	<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 21>>
	<<set $pubnpc to 4>>
	<<He>> looks bemused. You chat with <<him>> for a few minutes, making sure to mention that you're single. <<He>> doesn't say much, and seems happy to let you lead the conversation.
	<br><br>
	<<promiscuity1>>

	"Can I buy you a drink?" <<he>> says.
	<br><br>

	<<if $promiscuity gte 15>>
		<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
		<br>
	<</if>>
	<<link [[Accept (0:20)|Pub Drink]]>><<set $drunk += 200>><<pass 20>><<set $pubdrink to 2000>><</link>>
	<br>
	<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
	<br>

<<else>>
	<<set $pubnpc to 5>>
	<<He>> glances at you once, then stares into <<his>> cup. You try to make small talk despite <<his>> lack of engagement. You make a point to mention that <<he>> looks good, and you're single.
	<<promiscuity1>>
	<br><br>

	<<He>> rubs <<his>> temple. "Why can't I get any peace? Piss off."
	<br><br>

	<<if $promiscuity gte 15>>
		<<link [[Seduce (0:01)|Pub Seduce]]>><<pass 1>><</link>><<promiscuous2>>
		<br>
		<<if $money gte 500>>
			<<link [[Offer a drink (0:20) (£5)|Pub Drink]]>><<pass 20>><<set $money -= 500>><<set $pubdrink to 2000>><</link>>
			<br>
		<</if>>
	<</if>>
	<<link [[Say goodbye|Pub]]>><<endevent>><</link>>
	<br>

<</if>>