<<set $outside to 1>><<set $location to "forest">><<effects>>
<<set $wolfpackrob to 1>>

<<if $submissive gte 1150>>
"Give me your money and valuables," you say. "Then leave us to eat in peace."
<br><br>
<<elseif $submissive lte 850>>
"Your money and valuables," you say. "Before I feed you to my family." The wolves growl.
<br><br>
<<else>>
"Your money and valuables," you say. "Hurry, I can't hold them back forever." The wolves growl.
<br><br>
<</if>>

They pull money out of pockets and take off their jewellery. They drop it all in a pile at their feet before being marched from the camp by the wolves nipping at their heels. The wolves rush to the deer carcass once they are out of earshot.
<br><br>

	<<if $rng gte 96>>
	You find £200 in cash, as well as assorted baubles you think are very valuable.
	<<set $money += 20000>><<set $blackmoney += 800>><<crimeup 1000>>
	<<elseif $rng gte 51>>
	You find £50 in cash, as well as assorted baubles you think are quite valuable.
	<<set $money += 5000>><<set $blackmoney += 50>><<crimeup 100>>
	<<else>>
	You find £20 in cash, as well as assorted baubles you think are quite valuable.
	<<set $money += 2000>><<set $blackmoney += 30>><<crimeup 50>>
	<</if>>

<<endevent>>

	The black wolf keeps the choice parts of the deer for itself. You rummage in a bag left behind by the hunters and find some canned food that suits you better.
	<<stress -12>><<lstress>>
	<br><br>

	<<if $wolfpackspray isnot 1>><<set $wolfpackspray to 1>>
	You find a strange cylinder among their things. It looks like a charge for your pepper spray, but with an exposed computer chip at the base. You put it in.
	<<gspraymax>><<set $spraymax += 1>><<spray 5>>
	<br><br>
	<</if>>

	The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
	<br><br>

<<wolfpackhuntoptions>>