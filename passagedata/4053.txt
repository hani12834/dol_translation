<<set $outside to 1>><<set $location to "beach">><<effects>>
You walk to the High Street and buy a large bottle of water. You return to the beach. Robin waves at you. "Thank you. I'd need to close the stand if not for you." <<He>> hugs you.
<br><br>
<<endevent>>
<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
<br>
<<link [[Leave|Beach]]>><</link>>
<br>