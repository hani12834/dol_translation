<<set $outside to 1>>
<<if $phase is 0>>
	<<effects>>
	You run with all the speed you can muster.
	<<gathletics>><<athletics 1>><<physique>>
	<<if $bestialitydisable is "t">>
		You manage to escape, but not unscathed.
		<<set $trauma += 40>><<set $pain += 40>><<set $stress += 400>><<set $worn.lower.integrity -= 20>><<set $worn.upper.integrity -= 20>><<set $worn.under_lower.integrity -= 20>><<bruise leftarm>><<bruise full>>
		<br><br>
		<<destinationeventend>>
	<<else>>
		<<if $athletics gte random(1, 600)>>
			You slowly gain distance on the dog and it eventually gives up the chase. You run a bit further, then stop to catch your breath.
			<br><br>
			<<destinationeventend>>
		<<else>>
			The beast is faster than you however. It quickly catches up to you and, grabbing your arm in its teeth, drags you to the ground.
			<br><br>
			<<set $pain += 30>><<set $phase2 to 1>>
			<<link [[Next|StreetEx3]]>><<set $phase to 1>><<set $molestationstart to 1>><</link>>
			<br><br>
		<</if>>
	<</if>>
<<elseif $phase is 1>>
	<<if $molestationstart is 1>>
		<<set $molestationstart to 0>>
		<<controlloss>>
		<<violence 1>>
		<<neutral 1>>
		<<molested>>
		<<beastNEWinit 1 dog>>
		<<if $phase2 is 1>>
			<<set $enemyanger += 120>>
		<</if>>
	<</if>>
	<<effects>>
	<<effectsman>>
	<br>
	<<beast $enemyno>>
	<br><br>
	<<stateman>>
	<br><br>
	<<actionsman>>
	<<if $alarm is 1>>

		No one comes to your aid.
		<<set $alarm to 0>>
		<<if $drugged gte 1>>Intoxicated as you are, you couldn't cry very convincingly.<</if>>
		<br><br>
		<<if $enemyarousal gte $enemyarousalmax>>
			<span id="next"><<link [[Next->StreetEx3 Ejaculation]]>><</link>></span><<nexttext>>
		<<elseif $enemyhealth lte 0>>
			<span id="next"><<link [[Next->StreetEx3 Escape]]>><</link>></span><<nexttext>>
		<<else>>
			<span id="next"><<link [[Next->StreetEx3]]>><</link>></span><<nexttext>>
		<</if>>

	<<elseif $enemyarousal gte $enemyarousalmax>>
		<span id="next"><<link [[Next->StreetEx3 Ejaculation]]>><</link>></span><<nexttext>>
	<<elseif $enemyhealth lte 0>>
		<span id="next"><<link [[Next->StreetEx3 Escape]]>><</link>></span><<nexttext>>
	<<else>>
		<span id="next"><<link [[Next->StreetEx3]]>><</link>></span><<nexttext>>
	<</if>>
<</if>>