<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>
You look around for someone who might help. You see a <<npc Avery>><<person1>><<if $pronoun is "m">>man<<else>>woman<</if>> already watching you. <<He>> laughs and approaches you. <<He>> says nothing, but grabs you by the hips and helps lift you with surprising strength. It's enough for you to clamber up.
<<if $worn.lower.skirt is 1>>
	You feel your face flush as you realise <<he>> could have seen up your $worn.lower.name. If <<he>> did <<he>> gives no sign.
<</if>>
<<He>> smiles at you. "Why'd you want to go up there anyway?"
<br><br>
<<link [[Honest|Park Lichen Honest]]>><</link>>
<br>
<<link [[Evasive|Park Lichen Evasive]]>><</link>>
<br>