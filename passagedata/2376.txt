<<widget "eventstoilets">><<nobr>>

<<generate1>><<person1>>You enter the toilets, and hear a voice shout behind you. "I saw you go in there, pervert!" A <<person>> storms in, eyes livid. "I'll give exactly what you deserve!"
<br><br>

<<link [[Next|Toilets Molestation]]>><<set $molestationstart to 1>><</link>>

<<if $player.gender_appearance isnot $player.gender>><<set $toiletsmistake to 1>><</if>>

<</nobr>><</widget>>

<<widget "eventstoiletssafe">><<nobr>>

<<if $weather is "rain">>
Rain hammers against the small windows.
<br><br>
<<else>>
Wind rattles the small windows.
<br><br>
<</if>>

<<if $bus is "parkmens">>
<<link [[Next|Men's Toilets]]>><<set $eventskip to 1>><</link>>
<br>
<<else>>
<<link [[Next|Women's Toilets]]>><<set $eventskip to 1>><</link>>
<br>
<</if>>

<</nobr>><</widget>>