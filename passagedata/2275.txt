<<effects>>

<<flaunting>> you chase the <<person1>><<person>> through the market with your <<genitals>> and <<bottom>> on display. You feel more than a few eyes on you.
<<exhibitionism5>>

<<if $athletics gte random(1, 1200)>>

	<span class="green">You gain on your quarry,</span> and chase <<him>> into an alley. It's a dead end. <<He>> turns around, and the fear in <<his>> eyes is replaced by confusion at the sight of your nudity. Confusion and lust. <<covered>>
	<br><br>

	You hear voices behind you. A crowd's gathering, drawn by your exhibitionist spectacle.
	<br><br>

	Coming to <<his>> senses and seeing no other way out, the <<person>> throws the <<print $stall_plant.replace(/_/g," ")>> in the air. <<He>> struggles up and over the wall as you catch it.
	<br><br>

	You walk past the leering crowd and return to your stall as if nothing were amiss.
	<br><br>

	<<stall_actions>>
<<else>>

<span class="red"><<Hes>> too fast for you,</span> and you lose <<him>> in the maze of stalls.
<<set $plants[$stall_plant].amount -= 1>>
<br><br>

Panting, you look around. People from all over stare in your direction. Those are aren't are blushing. <<covered>>
<br><br>

	<<if $rng gte 61>>
		You return to your stall as if nothing were amiss.
		<br><br>
		<<stall_actions>>
	<<else>>
		<<endevent>>
		<<generate1>><<generate2>><<generate3>>
		You turn back in the direction of your stall, and almost walk straight into a <<person1>><<personstop>> There are two others with <<himstop>> "Where you going, lewd <<girl>>?" <<he>> says, stepping forward and into you, pushing you back. "Walking around like that's just asking for trouble. But don't worry. Me and my friends are here to look after you."
		<br><br>

		<<He>> wraps a hand around your waist and tries to pull you closer.
		<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
		<br><br>

		<<if $promiscuity gte 4>>
			<<link [[Allow|Stall Gang Sex]]>><<set $sexstart to 1>><</link>><<promiscuous4>>
			<br>
		<</if>>
		<<link [[Shove|Stall Gang Shove]]>><<trauma -6>><</link>><<ltrauma>>
		<br>
	<</if>>

<</if>>