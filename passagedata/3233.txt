<<effects>>

You throw the <<wallet>> up the staircase.

<<if $rng gte 51>>
	It's met by silence at first, until a voice breaks it. "Who's down there?"
	<br><br>
<<else>>
	It's met by a cheer. "Thanks, whoever you are. I didn't know we had company. Why don't you come on up?"
<</if>>
<br><br>

<<link [[Make excuses|Street Ex Deceive]]>><</link>>
<br>
<<link [[Tell the truth|Street Ex Truth]]>><</link>>
<br>
<<link [[Remain Silent|Street Ex Silent]]>><</link>>
<br>