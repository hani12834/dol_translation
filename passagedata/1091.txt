<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>>
<<set $livestock_horse to 2>>

His knees buckle, and he slumps to the ground. He looks asleep.
<br><br>

One of the other centaur trots up to him, then looks at you. "Me next,"
<br>
"Oi," says another, coming up behind him. "Remy's not given me any attention for ages."
<br><br>

<<tearful>> you climb off the straw as the centaur bicker.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Livestock Field]]>><</link>>
<br>

<<elseif $enemyhealth lte 0>>

The centaur rears up, and lands away from you. "Too much for me," he says, shaking his head.
<br><br>

<<tearful>> you climb off the straw.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Livestock Field]]>><</link>>
<br>

<<else>>

You're not sure the centaur will stop after all, until an older stallion trots up and gives him a stern look.
<br><br>

<<tearful>> you climb off the straw.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Livestock Field]]>><</link>>
<br>

<</if>>