<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
The next time <<he>> passes by you grasp the offending hand in your own, then slap <<himstop>>
<br><br>
<<He>> backs away from you, clutching <<his>> shocked face. <<He>> sneers, and lifts <<his>> arm to retaliate, until <<he>> notices the eyes watching from across the room.
<br><br>
<<He>> gives you one last evil look before leaving the room.
<br><br>
<<endevent>>
<<link [[Next|Temple Quarters]]>><</link>>
<br>