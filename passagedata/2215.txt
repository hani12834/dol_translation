<<set $location to "landfill">><<set $outside to 1>><<effects>>
<<pain 1>>
You awaken to the sound of rumbling. You see a metal ceiling above you. It's moving. You try to stand, but your arms and legs are tied. You're on a conveyor belt.
<br><br>

A loud thud up ahead steals your attention. A compactor crushes rubbish into convenient pancakes. You're headed straight for it.
<<gstress>><<gtrauma>><<stress 6>><<trauma 6>>
<br><br>

You struggle against the bonds, but they hold firm. You inch closer, and closer, until you can feel the vibrations through the belt.
<<gggstress>><<gggtrauma>><<stress 18>><<trauma 18>>
<br><br>

<<link [[Next|Trash Pass Out 2]]>><</link>>
<br>