<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $submissive gte 1150>>
"It's rude to barge," you say. "Please leave us be."
<<elseif $submissive lte 850>>
"No one invited you," you say. "Leave."
<<else>>
"Please leave," you say. "There's not enough room for you."
<</if>>
<br><br>

Kylar laughs. "I-I forgot," <<he>> says. "I have somewhere I need to be." <<He>> trips over the bench as <<he>> stands, eliciting laughter from several other students.
<br><br>

"I feel bad sending <<him>> away," Robin says when <<hes>> gone. "Everyone picks on <<himstop>>"
<br><br>
<<endevent>><<npc Robin>><<person1>>
You finish the food. "I'm going to wait in the classroom," Robin says when you're done. "I don't like being late." <<He>> hugs you.
<br><br>

	<<if $NPCName[$NPCNameList.indexOf("Kylar")].state is "active">>
	Kylar watches from across the canteen.
	<<gksuspicion>><<npcincr Kylar rage 1>>
	<br><br>
	<</if>>
<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>