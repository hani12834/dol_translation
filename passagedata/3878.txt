<<set $outside to 0>><<set $location to "town">><<effects>>
You talk with Doren over a variety of topics. <<He>> soon takes over the conversation, regaling you with personal stories. <<He>> seems to live an adventurous life.
<br><br>
<<He>> smiles at you. "I need to get back to school or I'll be in trouble." You look at the clock on the wall and realise a whole hour has passed. "I'm not home often, but if you visit between four and five in the afternoon I should be here. Visit as often as you like. Every day if you want. I mean it."
<br><br>
<<He>> drives you back to school. You feel guilty for having spent so much of <<his>> time, but <<he>> doesn't seem to mind.
<br><br>
<<endevent>>
<<link [[Return to school (0:05)|School Front Playground]]>><<pass 5>><</link>>
<br>