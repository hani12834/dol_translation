<<set $outside to 0>><<set $location to "hospital">><<effects>>

<<if $phase is 0>>

You shake yourself free of the effect. Harper quickly hides the camera behind <<his>> back. "That's enough for today. Remember, don't be afraid to relax." <<He>> holds open the door for you. "Same time next week. Be well."
<br><br>

<<link [[Next|Hospital Foyer]]>><<endevent>><</link>>

<<elseif $phase is 1>>

You remove your clothing, the flicker of resistance unable to break through the haze. "Very good." Harper says. The softness in <<his>> voice is still present, but there is now also a distinct quavering. "Just keep focusing on your breath." <<He>> starts circling you with the camera, which produces a clicking sound whenever <<he>> momentarily stops.
<<fameexhibitionism 10>>
<br><br>

This continues for several minutes. <<He>> places the camera back in the drawer. "Put your clothes back on," <<he>> says, as <<he>> sits back down and sighs. You dress yourself with robotic motions while Harper watches. After, <<he>> drops <<his>> pen on the ground and brings you back to reality.
<br><br>

<<He>> smiles at you, <<his>> face blushing. "Today was a success. Well done." <<He>> holds open the door for you. "Same time next week. Be well."
<br><br>

<<link [[Next|Hospital Foyer]]>><<clotheson>><<endevent>><</link>>

<</if>>