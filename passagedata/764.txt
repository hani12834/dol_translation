<<effects>>
<<npc Alex>><<person1>>
"I'm coming!" It's Alex. It's faint, but the <<farm_text horse>> rushes inside at the sound of <<his>> voice. You close the stall door, and shout back to let Alex know the problem is solved. You hear <<him>> laugh.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>