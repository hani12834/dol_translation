<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<pass 20>>
Leighton retrieves <<his>> camera from the drawer and leers at you.

<<if $rng gte 61>>

"Let's see some skin," <<he>> says. "Strip."
<br><br>

	<<if $exhibitionism lte 54>>
	You hesitate, and <<he>> smiles. "Better me than the whole school. I'll ask one more time. Strip."
	<<else>>
	<</if>>
<br><br>

<<strip>>
You <<nervously>> remove your clothing. You feel <<his>> eyes taking in your exposed body. "Good," <<he>> says. "I don't mind you covering, it's cute." <<He>> brings the camera up to <<his>> eyes and takes a picture. Then another. <<He>> circles around you, snapping from different angles.
<br><br>
<<fameexhibitionism 10>>
<<else>>

"I never get bored of that outfit," <<he>> says. <<He>> brings the camera up to <<his>> eyes and takes a picture. <<He>> circles around you, snapping from different angles. <<He>> occasionally stops to adjust your clothing, opening a button on your shirt or tugging down your $worn.lower.name to show more of your hips.
<br><br>

<</if>>

<<if $pain gte 60>>
<<He>> makes no mention of your tears.
<br><br>
<</if>>

<<set $rng to random(1, 100)>>
<<if $rng gte 61>>
<<He>> asks you to bend over, kneel, and other suggestive poses.
<<fameexhibitionism 10>>
<<else>>
<<He>> asks you to adopt various poses, but nothing too lewd.

<</if>>

After a while <<he>> puts down <<his>> camera. "Don't go anywhere just yet," <<he>> says. <<He>> steps beside you, and strokes your hair with <<his>> left hand.

<<set $rng to random(1, 100)>>
<<if $rng gte 81>>
Without warning, <<he>> seizes your hair and throws you over <<his>> desk, sending papers and knick-knacks crashing to the floor.
<br><br>

<<link [[Next|Head's Office Blackmail Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>
<<He>> turns away. "That'll be all. Don't forget. I want to see you every school day."
<br><br>

<<clotheson>>

<<tearful>> you leave the office.
<br><br>

<<link [[Next|Hallways]]>><<endevent>><</link>>
<br>

<</if>>