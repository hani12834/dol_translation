<<effects>>

<<if $submissive gte 1150>>
	"Please don't be mad at me," you say. "I would have made it if I could."
<<elseif $submissive lte 850>>
	"I'm busy sometimes," you say. "But I'm sorry if it inconvenienced you."
<<else>>
	"Sorry," you say. "I didn't mean to."
<</if>>
<br><br>

"If you're sorry," Avery says through gritted teeth. "Then you'll get in the damn car."
<br><br>

<<link [[Get in|Street Avery Rape Drive]]>><<npcincr Avery rage -5>><</link>><<larage>>
<br>
<<link [[Refuse|Street Avery Refuse]]>><</link>><<gggarage>><<lllove>>
<br>