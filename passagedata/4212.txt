<<set $outside to 0>><<set $location to "forest_shop">><<effects>>
"You should try this on," you say, holding the witch outfit against <<himstop>>
<br><br>
<<if $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "m">>
	"But it's for girls," Robin says.
	<br><br>
	"So?" you respond. "You're only dressing up for fun."
	<br><br>
	<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 60>>
		Robin hesitates, then takes the outfit. You leave the screen to give <<him>> some privacy.
		<br><br>
		"Promise you won't laugh," Robin says after a minute. <<He>> doesn't wait for your response before stepping out, still adjusting <<his>> hat. The costume is oddly tight around <<his>> waist, giving the impression of feminine contours. It fits well.
		<br><br>
		<<He>> examines <<himself>> in a nearby mirror.
		<br><br>
		<<link [[Tease|Robin Forest Witch Tease]]>><</link>>
		<br>
		<<link [[Compliment|Robin Forest Witch Compliment]]>><</link>>
		<br>
	<<else>>
		Robin hesitates, then shakes <<his>> head. "It's a really cool outfit," <<he>> says. "But I'd be too embarrassed."
		<br><br>
		<<link [[Ask Robin to try on the boy's vampire costume|Robin Forest Vampire]]>><</link>>
		<br>
		<<link [[Walk home|Robin Forest Shop Home]]>><</link>>
		<br>
	<</if>>
<<else>>
	"Okay." Robin says, taking the outfit. You leave the screen to give <<him>> some privacy.
	<br><br>
	"I'm ready," Robin says after a minute. <<He>> doesn't wait for your response before stepping out, still adjusting <<his>> hat. The costume is tight around <<his>> waist, and shows off <<his>> feminine contours. It fits well.
	<br><br>
	<<He>> examines <<himself>> in a nearby mirror.
	<br><br>
	<<link [[Tease|Robin Forest Witch Tease]]>><</link>>
	<br>
	<<link [[Compliment|Robin Forest Witch Compliment]]>><</link>>
	<br>
<</if>>