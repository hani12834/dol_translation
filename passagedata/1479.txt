<<effects>>

Briar's goon keeps a close eye on you as you peek out the front exit. The street is busy with cars and other vehicles, and more than a few pedestrians walk the pavements. <<flaunting>> you step out onto the street.

<<if $ex_brothel isnot 1>>
	<<set $ex_brothel to 1>>
	<<exhibitionism3>>
<<else>>
<br><br>
<</if>>

<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>
<br>