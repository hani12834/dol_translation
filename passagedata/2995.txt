<<set $outside to 0>><<set $location to "town">><<effects>>
<<person1>>
Leighton and Sirris examine the remaining projects. One is just an essay taped to a board. Another looks more like an art project.
<br><br>

The public roam between the displays, asking questions and flirting inappropriately with students.

<<if $phase is 2>>
	Yours attracts a lot of attention for its subject. A lot of inappropriate flirting too.
<</if>>

<br><br>

The teachers confer on the stage, then Leighton steps up to the microphone. "We've made a decision," <<he>> says.

<<if $phase is 0 and $sciencelichenchance + $sciencefaircheat gte $rng>>
	"The trophy goes to the <span class="green">student with the lichen project.</span>" Sirris waves at you. "Come on up."
	<br><br>

	You climb the stage and shake Leighton's hand as <<he>> smiles at a waiting photographer. <<He>> hands you the trophy. There's <span class="gold">£500</span> inside.
	<<set $money += 50000>>
	The audience cheer and applaud. The <<person2>><<person>> looks upset, but claps along.
	<<npcincr Sirris love 5>><<llltrauma>><<trauma -120>><<famebusiness 100>>
	<br><br>

	<<endevent>><<npc Sirris>><<person1>>Sirris follows you off the stage. "I'm so happy you took to the project with such enthusiasm," <<he>> says. <<He>> and Leighton herd you and the other students from the building.
	<<lldelinquency>><<detention -54>>
	<br><br>

	<<set $scienceproject to "won">><<set $scienceprojectwon to 1>><<earnFeat "Science Fair Winner">>
	<<scienceprojectfinish>>

	<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<<elseif $phase is 1 and $scienceshroomchance + $sciencefaircheat gte $rng>>
	"The trophy goes to the <span class="green">student with the mushroom project.</span>" Sirris waves at you. "Come on up."
	<br><br>

	You climb the stage and shake Leighton's hand as <<he>> smiles at a waiting photographer. <<He>> hands you the trophy. There's <span class="gold">£500</span> inside.
	<<set $money += 50000>>
	The audience cheer and applaud. The <<person2>><<person>> looks upset, but claps along.
	<<npcincr Sirris love 5>><<llltrauma>><<trauma -120>><<famebusiness 100>>
	<br><br>

	A group of students surround you as you climb down from the stage. They want to hear the story of how you collected the mushrooms that only grow deep in the woods, deeper than the bravest of them have ventured. Leighton herds you and the other students from the hall.
	<<ldelinquency>><<detention -18>><<ggcool>><<status 18>>
	<br><br>

	<<set $scienceproject to "won">><<set $scienceprojectwon to 1>><<earnFeat "Science Fair Winner">>
	<<scienceprojectfinish>>

	<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<<elseif $phase is 2 and $sciencephalluschance + $sciencefaircheat gte $rng>>
	"The trophy goes to the <span class="green">student with the phallus project.</span>" Murmuring fills the hall. Sirris waves at you. "Come on up."
	<br><br>

	You climb the stage and shake Leighton's hand as <<he>> smiles at a waiting photographer. <<He>> hands you the trophy. There's <span class="gold">£500</span> inside. The audience cheer and applaud.

	<<if $sciencephalluschance is 0>>
		The <<person2>><<person>> looks upset. <<His>> fists clench. "No," <<he>> shouts. "I won't accept this. All <<pshe>> did was draw some privates." <<He>> marches up the stage. "Give me that trophy!" <<He>> tries to wrestle it from your grip.
		<br><br>

		<<link [[Let them take it|Science Fair Take]]>><</link>>
		<br>
		<<link [[Wrestle|Science Fair Wrestle]]>><</link>><<physiquedifficulty 4000 14000>>
		<br>

	<<else>>
		The <<person2>><<person>> looks upset, but claps along.
		<<npcincr Sirris love 5>><<llltrauma>><<trauma -120>><<famebusiness 100>><<famesex 100>><<set $money += 50000>>
		<br><br>

		The photographer accosts you as you leave the stage, wanting to know more about how you acquired your data. Leighton herds you and the other students from the hall.
		<<ldelinquency>><<detention -18>>
		<br><br>

		<<set $scienceproject to "won">><<set $scienceprojectwon to 1>><<earnFeat "Science Fair Winner">>
		<<scienceprojectfinish>>

		<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
		<br>
	<</if>>

<<else>>
	<span class="red"><<He>> announces that the <<person2>><<person>> with a flower display is the winner.</span> <<He>> rushes up to accept <<his>> trophy.
	<br><br>

	Leighton herds you and the other students from the hall. "Thank you for taking part," Sirris says to you on the way out. "It's good practise."
	<<ldelinquency>><<detention -18>>
	<br><br>

	<<set $scienceproject to "done">>
	<<scienceprojectfinish>>

	<<link [[Next|Cliff Street]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>

<</if>>