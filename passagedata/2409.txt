<<set $outside to 0>><<set $location to "park">><<effects>>
<<He>> plants a kiss on your cheek and smiles. "I'll be in touch."
<br><br>
You leave soon after <<himcomma>> but <<hes>> already gone.
<br><br>
<<if $scienceproject is "ongoing">>
	<span class="gold">You can add the lichen you found to your project in your room or the school library.</span>
	<br><br>
	<<link [[Next|Park]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	<<endevent>>
	<<destinationeventend>>
<</if>>