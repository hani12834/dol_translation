<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $kylarenglishstate is "active">>
	You focus on listening to the teacher and studying the material. You think you're making progress, despite Kylar's distractions.
	<br><br>
<<else>>
	You focus on listening to the teacher and studying the material. It's hard going, but you think you're making progress.
	<br><br>
<</if>>

<<towelup>>

<<pass 5>><<stress 1>>
<<schooleffects>>
<<set _english_time to 1>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress 1>><<set _english_time += 1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress 1>><<set _english_time += 1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress 1>><<set _english_time += 1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<englishskill _english_time>>

<<if $phase is 1>>
	<<set $phase to 0>>
	The bell rings, signifying the end of the lesson. You
	<<storeon "englishClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "englishClassroom">>
		grab your coat and
	<</if>>
	leave the classroom.
	<br><br>
	<<if $time is ($hour * 60 + 57)>>
		<<pass 3>>
	<<elseif $time is ($hour * 60 + 58)>>
		<<pass 2>>
	<<elseif $time is ($hour * 60 + 59)>>
		<<pass 1>>
	<</if>>

	<<link [[Next|Hallways]]>><</link>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (8900 - $allure)>>
		<<eventsenglish>>
	<<else>>
		<<eventsenglishsafe>>
	<</if>>
<</if>>