<<effects>>
<<if $phase is 0>>
	You step on the stool. Closer, you now see the cord they mean. It's taped to the ceiling. You reach up and tug.
<<else>>
	You shake your head. The <<person3>><<person>> pouts and climbs the stool. You spot what <<hes>> reaching for. A cord is taped to the ceiling.
	<br><br>
	With a jump, <<he>> manages to snatch the cord. <<He>> tugs.
<</if>>
A crack forms in the ceiling, running in two directions. A large hatch creaks open and a ladder thuds to the floor, followed by a shower of dust and broken cobwebs. The <<person2>><<person>> screams as they get stuck in <<his>> hair.
<br><br>
You climb the ladder and shift aside a thin cover, letting you peek through the hatch. Once your eyes adjust to the gloom, you see a large space. It's filled with old boxes, with corridors lining each side. A little light pierces grimy windows. There's a lot of space up there.
<br><br>
<<link [[Next|Orphanage Loft Intro 2]]>><</link>>
<br>