<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You lean seductively against the table and address the <<personstop>> "You look tense, anything I can do to help?"
<<promiscuity1>>
<br><br>

<<if $cool gte 160>>
<<He>> checks you out, and blushes. "Would you... like to go somewhere private?" <<He>> clearly has lewd intentions.
<br><br>

	<<if $promiscuity gte 15>>
<<link [[Accept|Canteen Student Encounter]]>><<set $sexstart to 1>><</link>><<promiscuous2>>
<br>
	<</if>>
<<link [[Refuse|Canteen]]>><<endevent>><</link>>

<<elseif $cool lt 40>>
<<set $canteenapproach to 1>>
The group burst into laughter, except the <<personcomma>> who looks mortified. "Fuck off. And don't speak to me again."
<br><br>

<<link [[Next|Canteen]]>><<endevent>><</link>>
<br><br>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<He>> checks you out, and blushes. "Would you... like to go somewhere private?" <<He>> clearly has lewd intentions.
	<br><br>

		<<if $promiscuity gte 15>>
<<link [[Accept|Canteen Student Encounter]]>><<set $sexstart to 1>><</link>><<promiscuous2>>
<br>
		<</if>>
<<link [[Refuse|Canteen]]>><<endevent>><</link>>
	<<else>>
	<<set $canteenapproach to 1>>
	<<He>> blushes and looks away from you. <<His>> friends start laughing at <<his>> sudden reticence.
	<br><br>

	<<link [[Next|Canteen]]>><<endevent>><</link>>
	<</if>>

<</if>>