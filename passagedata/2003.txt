<<effects>>

You rise to your feet and shove the <<person2>><<personstop>><<He>> shoves you back. You move in for another, but the <<person1>><<person>> dashes in the way. "St-Stop," <<he>> says. "Please don't fight. Not over me. Oh." <<He>> throws <<his>> hands to <<his>> face and flees the room.
<br><br>

"Wait!" the <<person2>><<person>> cries. <<He>> chases after <<person1>><<himstop>>
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>