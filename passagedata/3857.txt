<<effects>>
<<pass 20>>

You play with your cutlery, your napkin, and whatever else your hands find on the table. "Don't fidget," Avery says. "You'll make us both look bad."
<br><br>

The food soon arrives.
<br><br>

<<link [[Next|Avery Date 3]]>><</link>>
<br>