<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $submissive gte 1150>>
	"N-no," you say. "I'm sorry! I'm too shy."
<<elseif $submissive lte 850>>
	"In a filthy shed in front of your scummy friends?," you say. "Fuck off."
<<else>>
	"No," you say. "Especially not with people watching."
<</if>>
<br><br>
Whitney's friends laugh at your brazen refusal. Whitney laughs too. "Suit yourself," <<he>> says. "My <<girl>> knows what <<pshe>> wants." <<He>> doesn't push you, instead simply taking another swig of <<his>> drink.
<br><br>

<<link [[Next|Whitney Trick 7]]>><</link>>
<br>