<<effects>>


"The demonstration will be more authentic if I submerge you completely," Winter says, producing a small device dominated by a green button. "I'll give you this waterproof buzzer, which will notify me the instant you want the demonstration to stop." <<He>> presses the button, and <<his>> pocket vibrates. "You'll be underwater, and this will be your only way to communicate with me."
<br><br>

"Again, I must stress that you should feel no pressure to accept," <<he>> adds. "It would help sell the scene and drum up interest, but I know I'm asking a lot."
<br><br>

<<link [[Accept|Museum Duck Extreme]]>><<bind>><<npcincr Winter love 3>><</link>><<swimmingdifficulty 1 400>><<gglove>><<ltrauma>>
<br>
<<link [[Refuse|Museum Duck Light]]>><<bind>><<npcincr Winter love 1>><</link>><<glove>>
<br>