<<effects>>

<<if $submissive gte 1150>>
	You throw back your head, and scream. The three back away from you.
<<elseif $submissive lte 850>>
	"Oi, teachers!" you shout. "A little help."
<<else>>
	"I'm being bullied!" you shout. "Somebody help."
<</if>>
<br><br>
The <<person1>><<person>> looks around, and smirks when no one comes. "So you're a coward-"
<br><br>
"Stop this at once," interrupts a firm voice. Sirris leans from a staff room window.
<br>
"<<pShe>> started-"
<br>
"I won't hear it," Sirris continues. "Go see Leighton. Right now."
<br>
The bullies share a worried glance, but do as they're told.
<br><br>
<<endevent>>

<<npc Sirris>><<person1>>
<<if $exposed gte 1>>
	Sirris holds out a towel and looks away. You cover yourself as fast as you can.
	<br><br>
	<<towelup>>
<</if>>

<<if $schoolrep.crossdress gte 5>>
	"Are you okay?" Sirris asks once you're dressed. "Don't let them get to you," <<he>> smiles. "You're not doing anything wrong."<<ltrauma>><<trauma -6>><<lstress>><<stress -6>>
<<else>>
	"Are you okay?" Sirris asks once you're dressed. "Don't let them get to you," <<he>> smiles. "There's nothing wrong with you."<<ltrauma>><<trauma -6>><<lstress>><<stress -6>>
<</if>>
<br><br>
<<endevent>>
<<playground>>