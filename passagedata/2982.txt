<<effects>>

<<if $sciencelichenknown is 1>>
	You have an ongoing project on lichen. It has a <<print ($sciencelichenparkready * 25 + $sciencelichenlakeready * 25 + $sciencelichendrainready * 25 + $sciencelichentempleready * 25)>>% chance of winning the fair.
	<br>
<</if>>
<<if $sciencelichentemple is 1 and $sciencelichentempleready lt 1>>
	<<link [[Add pink lichen (3:00)|Science Project Lichen]]>><<set $phase to 0>><<pass 2 hours>><<pass 1 hour>><<scienceskill 18>><</link>><<gggscience>>
	<br>
<</if>>
<<if $sciencelichenpark is 1 and $sciencelichenparkready lt 1>>
	<<link [[Add white lichen (3:00)|Science Project Lichen]]>><<set $phase to 1>><<pass 2 hours>><<pass 1 hour>><<scienceskill 18>><</link>><<gggscience>>
	<br>
<</if>>
<<if $sciencelichendrain is 1 and $sciencelichendrainready lt 1>>
	<<link [[Add violet lichen (3:00)|Science Project Lichen]]>><<set $phase to 2>><<pass 2 hours>><<pass 1 hour>><<scienceskill 18>><</link>><<gggscience>>
	<br>
<</if>>
<<if $sciencelichenlake is 1 and $sciencelichenlakeready lt 1>>
	<<link [[Add purple lichen (3:00)|Science Project Lichen]]>><<set $phase to 3>><<pass 2 hours>><<pass 1 hour>><<scienceskill 18>><</link>><<gggscience>>
	<br>
<</if>>
<br>
<<if $scienceshroomknown is 1>>
	You have an ongoing project on mushrooms. It has a <<print ($scienceshroomwolfready * 10 + $scienceshroomheartready * 10)>>% chance of winning the fair.
	<br>
	You've measured $scienceshroomheartready/5 heartshrooms.
	<br>
	You've measured $scienceshroomwolfready/5 wolfshrooms.
	<br>
<</if>>
<<if $scienceshroomheart gt $scienceshroomheartready>>
	<<link [[Add heart shroom (1:00)|Science Project Shroom]]>><<set $phase to 0>><<pass 1 hour>><<scienceskill 6>><</link>><<ggscience>>
	<br>
<</if>>
<<if $scienceshroomwolf gt $scienceshroomwolfready>>
	<<link [[Add wolf shroom (1:00)|Science Project Shroom]]>><<set $phase to 1>><<pass 1 hour>><<scienceskill 6>><</link>><<ggscience>>
	<br>
<</if>>
<br>
<<if $sciencephallusknown is 1>>
	You have an ongoing project on phallus size. It has a <<print ($sciencephallusready * 10)>>% chance of winning the fair.
	<br>
	<<if $sciencephallus gt $sciencephallusready>>
		<<link [[Add phallus (1:00)|Science Project Phallus]]>><<set $phase to 0>><<pass 1 hour>><<scienceskill 6>><</link>><<ggscience>>
		<br>
	<</if>>
<</if>>

You can only submit one project to the fair.
<br><br>

<<if $location is "home">>
	<<link [[Stop|Bedroom]]>><</link>>
	<br>
<<else>>
	<<link [[Stop|School Library]]>><</link>>
	<br>
<</if>>