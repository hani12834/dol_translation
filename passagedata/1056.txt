<<effects>>
<<set $livestock_escape to "hole">>
You pull yourself from the hole, and run. You climb a small hill, and down the other side. You should no longer be visible from the field.
<br><br>

<<pass 1>>A minute later you hear a shrill whistle behind you, followed by shouting and the barking of dogs. Remy's voice rises above the others. <span class="red">You are being hunted.</span><<lllobey>><<livestock_obey -20>>
<br><br>
<<link [[Next|Livestock Escape]]>><</link>>
<br>