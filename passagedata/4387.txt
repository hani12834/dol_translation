<<effects>><<set $location to "sewers">><<set $outside to 0>>
You do your best to keep up with Morgan in conversation. <<He>> speaks of commoners with disdain.
<br><br>
<<if $rng gte 75>>
	<<His>> take on things seems strange.
	<<lenglish>><<set $english -= 1>><<set $school -= 1>>
	<br><br>
<<elseif $rng gte 50>>
	<<His>> take on things seems strange.
	<<ggenglish>><<englishskill>><<englishskill>>
	<br><br>
<<else>>
	<<He>> rambles a lot, but has moments of greater lucidity.
	<<genglish>><<englishskill>>
	<br><br>
<</if>>
<<link [[Next|Sewers Morgan]]>><</link>>
<br>