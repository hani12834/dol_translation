<<effects>>

<<if $submissive gte 1150>>
	"I-I don't have any clothes," you say. "I'm hiding down here."
<<elseif $submissive lte 850>>
	"I'm not dressed properly," you say. "And I'm not showing myself to strangers."
<<else>>
	"I'm indecently dressed," you say. "Sorry, I can't"
<</if>>
<br><br>

"So you're some sort of pervert?" laughs the voice.
<<if $rng gte 51>>
	A <<person1>><<person>> appears at the top of the stairwell. "Well would you look at that. <<pShes>> telling the truth." The others stick their heads over the railing, looking down at you. <<covered>><<fameexhibitionism 4>>
	<br><br>

	You walk up the stairs. At least you'll feel less trapped up there. The <<person>> grasps your arm and pulls you in front of <<himcomma>> exposing you to <<his>> friends.
	<br><br>

	"So you're some kind of pervert, exposing yourself for the thrill?" <<he>> whispers. "Don't worry. We'll help get you off."
	<br><br>

	<<link [[Next|Street Ex Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>
	"I should be so lucky. Good night, helpful stranger." <<He>> doesn't believe you. You hear the group walk away, and breathe a sigh of relief.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<</if>>