<<effects>>

<<if $physique gte random(1, 24000)>>
	You grasp the shoots, <span class="green">and tear them away from your limbs.</span> They retract, disappearing into the undergrowth.
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You grasp the shoots and pull, <span class="red">but they're too firm.</span> You're dragged into the undergrowth.
	<br><br>
	
	<<link [[Next|Farm Tending Tentacles]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>