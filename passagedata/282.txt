<<effects>>
You snuggle under the covers.
<br><br>
<<if $sleeptrouble is 1 and $controlled is 0>>
	<<link [[Sleep for 10 hours|Asylum Sleep]]>><<set $sleephour to 10>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 9 hours|Asylum Sleep]]>><<set $sleephour to 9>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 8 hours|Asylum Sleep]]>><<set $sleephour to 8>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 7 hours|Asylum Sleep]]>><<set $sleephour to 7>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 6 hours|Asylum Sleep]]>><<set $sleephour to 6>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 5 hours|Asylum Sleep]]>><<set $sleephour to 5>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 4 hours|Asylum Sleep]]>><<set $sleephour to 4>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 3 hours|Asylum Sleep]]>><<set $sleephour to 3>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 2 hours|Asylum Sleep]]>><<set $sleephour to 2>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 1 hours|Asylum Sleep]]>><<set $sleephour to 1>><</link>><<ltiredness>>
	<br><br>
<<else>>
	<<link [[Sleep for 8 hours|Asylum Sleep]]>><<set $sleephour to 8>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 7 hours|Asylum Sleep]]>><<set $sleephour to 7>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 6 hours|Asylum Sleep]]>><<set $sleephour to 6>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 5 hours|Asylum Sleep]]>><<set $sleephour to 5>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 4 hours|Asylum Sleep]]>><<set $sleephour to 4>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 3 hours|Asylum Sleep]]>><<set $sleephour to 3>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 2 hours|Asylum Sleep]]>><<set $sleephour to 2>><</link>><<ltiredness>>
	<br>
	<<link [[Sleep for 1 hours|Asylum Sleep]]>><<set $sleephour to 1>><</link>><<ltiredness>>
	<br><br>
<</if>>
<<link [[Climb out of bed|Asylum Cell]]>><</link>>
<br><br>