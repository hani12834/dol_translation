<<set $outside to 0>><<set $location to "temple">><<effects>>
<<if $soup_kitchen_init isnot 1>>
	You search for the grey building you were told about. You find it hidden among the trees in the shadow of the temple. You try the door, but it's locked. You were told it opens between <span class="gold"><<if $timestyle is "military">>18:00 and 21:00<<else>>6:00 pm and 9:00 pm<</if>></span>.
	<br><br>
<<else>>
	You approach the soup kitchen, but the door is locked. You were told it opens between <span class="gold"><<if $timestyle is "military">>18:00 and 21:00<<else>>6:00 pm and 9:00 pm<</if>></span>.
	<br><br>
<</if>>
<<link [[Next|Wolf Street]]>><</link>>
<br>