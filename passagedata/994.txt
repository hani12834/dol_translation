<<effects>>


A towel drops down the stairwell, and you dry yourself in the kitchen. You hear Alex come down the stairs. <<He>> asks if you're decent before entering.

<<if $weather is "rain" and !$worn.head.type.includes("rainproof")>>
	"Get yourself a Sou'wester's if the rain's bothering you," <<he>> says. "They work a treat. I'd best get back to work."
<<else>>
	"Did you fall in the river?" <<he>> laughs. "You go careful. I'd best get back to work."
<</if>>
<br><br>

<<dry>>
<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>