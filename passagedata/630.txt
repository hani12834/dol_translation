<<set $location to "wolf_cave">><<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>><<wolfpacktrust>>
<br>

The black wolf calms down. The other wolves look grateful and relieved.
<<gharmony>>
<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<destinationwolfcave>>

<<elseif $enemyhealth lte 0>>

	<<if $wolfpackleader is 1>>
	The black wolf limps away from you. The other wolves look relieved.
	<<gharmony>>
	<br><br>

	<<else>>
The whole pack watches as the black wolf rolls onto its back in submission.
<br><br>

<<tearful>> you howl. The other wolves howl with you. The black wolf hobbles away to nurse its injuries. <span class="gold">You can now lead the pack on hunts.</span>
<<set $wolfpackleader to 1>><<earnFeat "Head of the Pack">>
<br><br>
	<</if>>

<<clotheson>>
<<endcombat>>

<<destinationwolfcave>>

<<else>>

The black wolf stops bothering you, but continues to bite and growl at the others.
<<lharmony>>
<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<destinationwolfcave>>
<</if>>