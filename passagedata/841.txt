<<effects>>

<<if $submissive gte 1150>>
	"W-wait," you say.
<<elseif $submissive lte 850>>
	"Hold up," you say.
<<else>>
	"Wait," you say.
<</if>>
<br><br>
Alex turns around. You hand <<him>> the money. <<He>> stares at it for a moment, before realising what it is.
<br><br>

<<if $submissive gte 1150>>
	"I-It's okay," you say. "I got a good deal."
<<elseif $submissive lte 850>>
	"Don't worry," you say. "I know how to get a deal."
<<else>>
	"It's okay," you say. "I got a good deal."
<</if>>
<br><br>

Alex shrugs. "That's some deal, but I'm not gonna complain. Cheers." <<He>> enters the farmhouse.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>