<<set $outside to 1>><<set $location to "park">><<effects>>

<<npc Kylar>><<person1>>
You approach Kylar's bench.

<<if $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
<<Hes>> sketching a picture of you.
<<else>>
<<He>> hides whatever <<he>> was sketching when <<he>> sees you.
<</if>>
<br><br>
<<kylaroptions>>