<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	The wolf climbs off of you, and disappears into the forest.
	<br><br>
	Your knees buckle as the slime's influence fades. <<tearful>> you steady yourself.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<<else>>
	The wolf runs into the forest, whimpering.
	<br><br>
	<<tearful>> you try to stand, but a jolt of pain sends you sprawling. The slime punishes you for defiance.
	<<corruption -1>><<pain 8>><<stress 6>><<trauma 6>><<set $submissive -= 1>><<lcorruption>><<ggpain>><<ggtrauma>><<ggstress>>
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<</if>>