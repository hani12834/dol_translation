<<physiquediffulty>>/*On previous passage after link. Displays the difficulty. First argument is the physique required to have any chance of success. Second argument is the physique required to have a 100% chance of success. Argument 1 defaults to 1 if left blank. Argument 2 defaults to $physiquemax if left blank, which is 20000 as of 0.2.9.0.*/

<<if $physique gte random(1, $physiquemax)>>/*Replace 1 and $physiquemax with whatever numbers/variables used as arguments in <<physiquedifficulty>>*/

Text about passing the physique check.

<<else>>

Text about failing the physique check.

<</if>>