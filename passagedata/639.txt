<<effects>>

<<if $enemyhealth lte 0>>
	<<beastwound>>
		<<if $combatTrain.length gt 0>>
		The <<beasttype>> recoils in pain and fear, but the other is eager for a go.
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Wolf Cave Fight Duo]]
	<<else>>
		The <<beasttype>> recoils in pain and fear.
		<br><br>
		<<link [[Next|Wolf Cave Fight Duo Finish]]>><<set $finish to 1>><</link>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	<<if $combatTrain.length gt 0>>
		Satisfied, the <<beasttype>> moves and another takes its turn.
		<<wolfpacktrust>>
		<<beastNEWinit $combatTrain.numberPerTrain[0] $combatTrain.beastTypes[0]>>
		<<combatTrainAdvance>>
		<br><br>
		[[Next|Wolf Cave Fight Duo]]
	<<else>>
		Satisfied, the <<beasttype>> moves away from you.
		<<wolfpacktrust>>
		<br><br>
		<<link [[Next|Wolf Cave Fight Duo Finish]]>><<set $finish to 1>><</link>>
	<</if>>
<<else>>
	<<if $consensual is 1>>
	The wolves groom each other.
	<br><br>
	<<else>>
		<<if $enemywounded is 1 and $enemyejaculated is 0>>
		The <<beasttype>> whimpers and flees into the darkness between the trees.
		<<elseif $enemywounded is 0 and $enemyejaculated is 1>>
		The <<beasttype>> leaves you lying on the grass.
		<<elseif $enemywounded gte 2 and $enemyejaculated is 0>>
		Feeling that you're more trouble than you're worth, the wolves flee between the trees.
		<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
		The wolves leave you spent and shivering on the grass.
		<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
		The wolves leave you spent and shivering on the grass.
		<</if>>
		<br><br>
	<</if>>

	<<tearful>> you gather yourself.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<destinationwolfcave>>
<</if>>