<<effects>>
<<if $submissive gte 1500>>
	"Thank you <<if $pronoun is "m">>sir,"<<else>>ma'am,"<</if>> you mutter.
	<br>
	You hasten away with your head down. It's not <<his>> fault. It's how people treat <<if $player.gender is "m">>boys<<elseif $player.gender is "f">>girls<<else>>freaks<</if>> like you.
<<else>>
	Head down, you hasten your pace to escape.
<</if>>
<<gstress>>
<br><br>
<<stress 3>>
<<endevent>>
<<destinationeventend>>