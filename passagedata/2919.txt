<<effects>>
You sit down and get to work.
<<if $mathschance lte 9>>
	You hardly know where to begin - the mathematical symbols are a whole new language. <span class="gold">There must be a way to make things easier.</span>
<<elseif $mathschance lte 24>>
	You have a basic understanding of what you need to do, but the amount of work remaining is daunting.
<<elseif $mathschance lte 44>>
	You've probably done enough to be in with a chance, but the project isn't complete by any means.
<<elseif $mathschance lte 69>>
	The project is definitely passable at this point, and you have a hard time imagining that any other student has worked this hard on it.
<<else>>
	It's painstaking work, but you go over the project in detail, catching anything that could be considered a mistake.
<</if>>
<<if $location is "home">>
	<<if $hour lte 3>>
		The orphanage is dead silent as you work.
		<br><br>
	<<elseif $hour lte 5>>
		As you work, the sky slowly brightens and the sun rises above the horizon.
		<br><br>
	<<elseif $hour lte 7 and $schoolday is 1>>
		You do your best to ignore Bailey's knocking and instead keep working while the other residents head off to school.
		<br><br>
	<<elseif $hour lte 7>>
		You do your best to ignore the sound of the other orphanage residents heading out for the day.
		<br><br>
	<<elseif $hour lte 12 and $schoolday is 1>>
		You may be skipping school, but you aren't having much fun as you work.
		<br><br>
	<<elseif $hour lte 12>>
		You listen to the sounds of the bustling town outside as you work.
		<br><br>
	<<elseif $hour lte 14 and $schoolday is 1>>
		You work, though your concentration is often broken by the sound of the other residents of the orphanage arriving home from school.
		<br><br>
	<<elseif $hour lte 14>>
		The orphanage grows noisy as its residents arrive home.
		<br><br>
	<<elseif $hour lte 16>>
		The orphanage is alive with shouting and footsteps as the afternoon fades into the evening.
		<br><br>
	<<elseif $hour lte 18>>
		The orphanage grows quiet as the residents wind down for the night.
		<br><br>
	<<elseif $hour lte 20>>
		The sky grows dark as you work. The sun dips below the horizon.
		<br><br>
	<<elseif $hour lte 21>>
		The orphanage is quiet at this hour, but you're still startled out of your work by the occasional sound of footsteps past your door.
		<br><br>
	<<elseif $hour lte 23>>
		The orphanage is dead silent as you work.
		<br><br>
	<<else>>
		You watch the clock as you work, continuing through midnight and into the next day.
		<br><br>
	<</if>>
<<else>>
	<<if $schoolday is 1>>
		<<if $hour lte 6>>
			You work through the night, occasionally ducking below the desk to avoid security.
		<<elseif $hour lte 7>>
			Morning light bleeds through the library windows as you work.
		<<elseif $hour lte 8>>
			Other students arrive as you work. Some work on their own projects.
		<<elseif $hour lte 11>>
			You may be skipping your lessons, but you aren't having much fun as you work.
		<<elseif $hour lte 12>>
			You work as the library fills with other students arriving to pursue their own solutions.
		<<elseif $hour lte 14>>
			You may be skipping your lessons, but you aren't having much fun as you work.
		<<elseif $hour lte 16>>
			You work as the library fills with other students staying late to pursue their own solutions.
		<<elseif $hour lte 18>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 21>>
			The remaining daylight dims as you work.
		<<else>>
			You work through the night, occasionally ducking below the desk to avoid security.
		<</if>>
	<<else>>
		<<if $hour lte 6>>
			You work through the night, occasionally ducking below the desk to avoid security.
		<<elseif $hour lte 7>>
			Morning light bleeds through the library windows as you work.
		<<elseif $hour lte 8>>
			Morning light bleeds through the library windows as you work.
		<<elseif $hour lte 11>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 12>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 14>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 16>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 18>>
			You listen to the sounds of the bustling town outside as you work.
		<<elseif $hour lte 21>>
			The remaining daylight dims as you work.
		<<else>>
			You work through the night, occasionally ducking below the desk to avoid security.
		<</if>>
	<</if>>
<</if>>
<br><br>
<<link [[Next|Maths Project]]>><</link>>
<br>