<<set $outside to 0>><<effects>>
Keeping low, you shuffle into the aisle and poke a <<generatey1>><<person1>><<person>> in the arm and return to your seat. <<He>> looks over <<his>> shoulder.
<<if $submissive gte 1150>>
	"I-I need some towels. Please help," you say, unable to make eye contact.
<<elseif $submissive lte 850>>
	"Gimme some towels. Quickly," you demand.
<<else>>
	"Do you have any towels I can cover up with?" you ask.
<</if>>
<br><br>
<<if $rng gte 81>>
	"Why would you-" <<he>> begins before realising your predicament and smiling. "I'll help, but I want a look at you first."
	<br><br>
	<<if $submissive gte 1150>>
		"Please don't make me,"
	<<elseif $submissive lte 850>>
		"Fuck off,"
	<<else>>
		"Don't be a pervert,"
	<</if>>
	you say, but <<he>> moves to the seat opposite you. <<covered>>
	<br><br>
	<<if $leftarm is "bound" and $rightarm is "bound">>
		<<He>> laughs. "I was right. Now turn around so I can see you properly.
	<<else>>
		<<He>> laughs. "I was right. Move your hands so I can see you properly.
	<</if>>
	If you don't, I'll let the rest of the bus know and we can all get a look."
	<br><br>
	<<link [[Comply|Bus School Strip Comply]]>><<sub 1>><<trauma 6>><<stress 6>><<fameexhibitionism 1>><</link>>
	<br>
	<<link [[Refuse|Bus School Strip Refuse]]>><<detention 1>><<def 1>><<trauma 6>><<stress 6>><</link>><<gdelinquency>>
	<br>
<<else>>
	"Why would you-" <<he>> says, before blushing in realisation. <<He>> rummages in <<his>> back and throws some towels at you.
	<br><br>
	<<towelup>>
	<<endevent>>
	The bus soon arrives at your destination.
	<br><br>
	<<destination5>>
<</if>>