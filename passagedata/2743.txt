<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>

You <<nervously>> undress and tuck your clothes away inside the cupboard. You open the door slightly and peek through the gap.
Thirty or so students, plus the teacher. You'll get in trouble if you don't join them, but can you really expose yourself in front of so many people?
<br><br>

<<link [[This was a mistake!|School Pool Refuse]]>><</link>>
<br>
<<link [[I have no choice|School Pool Nude Entry]]>><</link>>