<<effects>>
<<set $rng to random(0, 200)>>
<<if $physique gte random(1, $physiquemax)>>
	With burst of strength, you throw off your assailants and prepare for a fight.
	<br><br>
	<span id="next"><<link [[Fight|Whitney Bully Parasite Event Combat]]>><<set $molestationstart to 1>><</link>></span><<nexttext>>
<<else>>
	<<if $physique gte random(1, 10000)>>
		Horrified and enraged, you struggle for your freedom. You throw one of Whitney's cronies off you, shocking the others with your brawn. Another replaces them though, and then another, until they have enough for Whitney to plant a firm punch into your abdomen, robbing you of your strength.
		<<pain +10>><<gpain>>
	<<else>>
		Horrified, you struggle against your assailants, hoping you can break their grip just enough to slip out and away to safety. Unfortunately, you aren't enough to escape the grip of this many strong hands. Your pathetic attempt at struggling earns you a punch to the gut from Whitney, and a slap across the face for even daring to resist.
		<<pain +15>><<ggpain>>
	<</if>>
	<br><br>
	<<eventwhitneyparasitecontinue>>
<</if>>