<<effects>>

You grasp Alex by the shoulders and pull <<him>> into the farmhouse. "Get off me!" <<he>> shouts, but trips and stumbles backwards as you lock front door.
<br><br>

You hear laughter outside. <<He>> climbs to <<his>> feet and tries to escape, but you stand in <<his>> way.

<<if $submissive gte 1150>>
	"Y-you're gonna get hurt," you say. "I-I might be hurt too."
<<elseif $submissive lte 850>>
	"It's four on two you idiot," you say. "And did you see the way they looked at you? They've come for you. Let's not give them what they want."
<<else>>
	"Did you see how they looked at you?" you ask. "They've come for you. Let's not give them what they want."
<</if>>
<br><br>

Alex glares at you, but doesn't try to pass. Instead, <<he>> sits in the living room, and rests <<his>> forehead in <<his>> palms.
<br><br>

<<link [[Next|Farm Lorry Safety 2]]>><</link>>
<br>