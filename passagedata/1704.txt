<<set $outside to 0>><<set $location to "cafe">><<effects>>

You lift the plate and lap the cream off it directly.

	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>

	You catch a <<generate1>><<person1>><<person>> staring at you, <<his>> spoon suspended halfway to <<his>> mouth.
	<br><br>

	<<link [[Show off|Cafe Cream Show]]>><</link>><<exhibitionist1>>
	<br>
	<<link [[Ignore|Cafe Cream Ignore]]>><</link>>
	<br>

	<<else>>
	No one pays much attention. You lean back, satisfied.
	<br><br>

	<<link [[Next|Ocean Breeze]]>><</link>>
	<br>
	<</if>>