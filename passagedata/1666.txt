<<set $outside to 0>><<set $location to "cafe">><<effects>>

You remove your $worn.upper.name to make it easier to dry. The <<if $pronoun is "m">>waiter<<else>>waitress<</if>> averts <<his>> eyes and blushes at the sight of your <<breastsstop>> A customer whistles. Another cheers. You manage to dry off without hassle.
<<exhibitionism4>>

<<upperstrip>>

<<set $tipmod to 0.2>><<tipset "serving">>

<<set $tip += 500>>
<<set $tip += 500>>

The rest of the shift passes uneventfully. Someone left you a tip.
<<tipreceive>><<pass 1 hour>>
<br><br>

<<link [[Next|Ocean Breeze]]>><<clotheson>><<endevent>><</link>>