<<widget "drainexit">><<nobr>>
	<<link [[Drain ocean exit (0:05)|Drain Exit]]>><<pass 5>><</link>>
<</nobr>><</widget>>

<<widget "drainexitquick">><<nobr>>
	<<link [[Drain ocean exit|Drain Exit]]>><</link>>
<</nobr>><</widget>>

<<widget "drainexiteventend">><<nobr>>
	<<link [[Next|Drain Exit]]>><<set $eventskip to 1>><</link>>
<</nobr>><</widget>>