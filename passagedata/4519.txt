<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "connudatus">>
<<fameexhibitionism 10>><<arousal 600>><<stress 6>><<trauma 6>>
<<if $phase is 1>>
	"Fine," the <<person>> grabs you by the neck and drags you into the daylight. "Look everyone." <<He>> shouts. "Look what I found!" Both hawkers and customers turn to investigate, and see you struggling to stand up, exposed and vulnerable. With nowhere to hide, there's not much you can do but flee.
	<br><br>
<<else>>
	You grab the underside of the stall and are hoisted into the air with it. The ride is too bumpy and your grip slips, causing you to drop painfully to the ground. The stall moves on without you, leaving you uncovered in the middle of the crowded street. With nowhere to hide, there's not much you can do but flee.
	<<fameexhibitionism 50>><<set $pain += 10>><<garousal>><<gstress>><<gtrauma>>
	<br><br>
<</if>>

You move as quickly as your legs will carry you, trying to escape the laughter, whistling and jeers. You run into an alleyway and round a corner, out of sight of the road, before feeling safe enough to process what just happened. So many people saw you. Your feelings of disgrace are made all the worse by the fluid you feel leaking from your <<genitalsstop>>
<br><br>
<<endevent>>
<<residentialquick>>