<<effects>>

<<if $submissive gte 1150>>
	"I-I need time to think about it," you say.
<<elseif $submissive lte 850>>
	"I'll think about it," you say. "Was hoping it'd pay more."
<<else>>
	"I'll think about it," you say.
<</if>>
<br><br>
Alex nods. "I understand. Come back when you're ready."
<br><br>

<<link [[Next|Farmland]]>><<endevent>><</link>>
<br>