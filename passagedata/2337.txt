<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>Winter turns the gold necklace in <<his>> palm. "This is magnificent," <<he>> says. "Worn by someone of note I imagine. Where did you find it?"
<br><br>

<<link [[Tell|Museum Tell]]>><<set $winterlakeknown to 1>><</link>>
<br>
<<link [[Keep secret|Museum Secret]]>><</link>>
<br>