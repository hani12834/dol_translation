<<effects>>

<<npc Remy>><<generate2>><<generate3>>

You awaken in a dingy windowless room, the stone walls lit only by a flaming torch held by a <<person2>><<personstop>> <<He>> and a <<person3>><<person>> stand, looking down at you. You're lying on a bed of straw.
<br><br>

<<person1>>"Awake I see," says a voice. It's Remy. <<He>> walks down a set of stone steps leading into the room, and kneels beside you. "We found you passed out. You caused quite the worry, but my examination found no problems. Just a stress response." <<He>> strokes your hair. "You need to stop worrying. You're safe here."
<br><br>

<<link [[Listen|Livestock Passout 2]]>><<set $phase to 0>><<livestock_obey 1>><<transform cow 1>><</link>><<gobey>>
<br>
<<link [[Push away|Livestock Passout 2]]>><<set $phase to 1>><<livestock_obey -1>><<trauma -6>><</link>><<lobey>><<ltrauma>>
<br>