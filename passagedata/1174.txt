<<effects>>

<<if $worn.face.type.includes("gag")>>
	You remain still, refusing to move.
<<elseif $submissive gte 1150>>
	"I-I can't pull this," you say. "I'm too weak."
<<elseif $submissive lte 850>>
	"Untie me at once," you demand. "Or you'll be sorry."
<<else>>
	"I can't pull this," you say. "I'm not strong enough."
<</if>>
<br><br>

Remy's cracks the whip once to your side, then follows it up with a lash to your back. You cry out in pain. "If that's how you want it," <<he>> says, a hint of delight in <<his>> voice. "So be it."
<br><br>

You brace for another lash, but the whip cracks again at your side.
<br><br>

<<link [[Pull|Livestock Plough]]>><<sub 1>><<pass 60>><<transform cow 1>><<npcincr Remy dom 1>><<tiredness 18>><</link>><<ggtiredness>>
<br>
<<link [[Refuse|Livestock Plough Refuse 2]]>><<def 1>><<npcincr Remy dom -1>><<pain 12>><</link>><<willpowerdifficulty 1 400>><<ggpain>>
<br>