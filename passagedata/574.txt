<<effects>>

<<if $submissive gte 1150>>
	"I don't want to be a bother," you say. "I'll be okay."
<<elseif $submissive lte 850>>
	"Do I look like I'm afraid of the dark?" you ask. "I'll be fine."
<<else>>
	"It's okay," you say. "I know my way around the forest."
<</if>>
<br><br>

Mason seems unsure, but nods. "If you're sure. Be careful." <<He>> dries <<himself>> with a towel as <<he>> walks into the forest.
<br><br>

<<link [[Next|Lake Waterfall]]>><<endevent>><</link>>
<br>