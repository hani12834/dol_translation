<<effects>>

<<generatey1>><<person1>>You approach the student, a <<personcomma>> working on their solution. <<Hes>> too focused to notice you. You might learn something if you could get close enough to read.
<br><br>

<<link [[Distract with your body|Maths Library Student Seduce]]>><<set $mathslibrarystudent to 1>><</link>><<exhibitionist1>>
<br>
<<link [[Leave|School Library]]>><<endevent>><</link>>
<br>