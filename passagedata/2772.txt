<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $submissive gte 1150>>
"I-I need money," you say. "You can take pictures if you give me some and promise not to share any."
<<elseif $submissive lte 850>>
"You're not gonna be sharing those pictures of me," you say. "And you're paying me for the photoshoots."
<<else>>
"I know you still want pictures of me," you say. "Keep them to yourself and I'll do it for cash."
<</if>>
<<exhibitionism4>>
<br><br>

<<Hes>> taken aback. <<He>> runs <<his>> fingers along <<his>> desk. "I'll give you £50 per shoot. Once every school day. I'm not made of money," <<he>> says. "Clear off for now, I'm busy." You leave <<his>> office.
<br><br>

<<endevent>>
<<set $headblackmailed to 1>>
<<set $headphotoshoot to 1>>
<<set $schoolfameboard to 1>>
<<set $schoolfameconsensual to 1>>
<<set $schoolfameblackmail to 0>>

<<link [[Next|Hallways]]>><</link>>
<br>