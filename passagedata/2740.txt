<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>
"I don't have a swimming costume, <<sirstop>>"
<br><br>

<<He>> sighs and climbs out the pool. "Come on, we might have some spares."
<br><br>

<<He>> takes you into a small room and rummages inside a cupboard.

<<if $rng gte 81 and $delinquency gte 200>>
	"I'm sorry but we don't have anything to spare. You'll have to swim naked."
	<br><br>
	<<He>> doesn't wait for you to give voice to your anxiety. "The rest of the class will be distracted, just quietly slip into the water.
	No one will be able to see anything once you're in."
	<br><br>

	<<link [[Accept|School Pool Nude]]>><<undress "school pool">><<endevent>><<exposure>><</link>>
	<br>
	<<link [[Refuse (0:05)|School Pool Refuse]]>><<pass 5>><<detention 6>><<endevent>><</link>><<gdelinquency>>
<<elseif $rng gte 41>>
	<<if $player.gender is "f" or ($player.gender is "h" and $breastsize gt 3)>>
		<<He>> produces a bundle of fabric and passes it to you. "Don't be long!" <<He>> says happily, before leaving you alone in the room.
		<br><br>

		You unfurl the fabric to reveal a rather tattered swimsuit. You slip it on and store your clothes in the cupboard, before heading out for the lesson.
		<br><br>

		<<link [[Get changed|Swimming Lesson]]>><<endevent>><</link>>
		<<undress "school pool">><<spareschoolswimsuit>><<exposure>>
	<<else>>
		<<He>> produces a bundle of fabric and passes it to you. "Don't be long!" <<He>> says happily, before leaving you alone in the room.
		<br><br>

		You unfurl the fabric to reveal a rather tattered pair of swim shorts. You slip them on and store your clothes in the cupboard, before heading out for the lesson.
		<br><br>

		<<link [[Get changed|Swimming Lesson]]>><<endevent>><</link>>
		<<undress "school pool">><<spareschoolswimshorts>><<exposure>>
	<</if>>
<<elseif $rng gte 1>>
	<<if $player.gender is "f" or ($player.gender is "h" and $breastsize gt 3)>>
		<<He>> produces a bundle of fabric and passes it to you. "Don't be long!" <<He>> says happily, before leaving you alone in the room.
		<br><br>

		You unfurl the fabric to reveal a rather tattered pair of boy's swim shorts. You rummage in the cupboard yourself, but find not a scrap of clothing.
		It's the shorts or nothing.
		<br><br>

		<<link [[Wear the shorts|School Pool Crossdress]]>><<undress "school pool">><<spareschoolswimshorts>><<endevent>><<exposure>><</link>>
		<br>
		<<link [[Skip the lesson (0:05)|School Pool Refuse]]>><<pass 5>><<detention 6>><<endevent>><</link>><<gdelinquency>>
	<<else>>

		<<He>> produces a bundle of fabric and passes it to you. "Don't be long!" <<He>> says happily, before leaving you alone in the room.
		<br><br>

		You unfurl the fabric to reveal a rather tattered girl's swimsuit. You rummage in the cupboard yourself, but find not a scrap of clothing.
		It's the swimsuit or nothing.
		<br><br>

		<<link [[Wear the swimsuit|School Pool Crossdress]]>><<undress "school pool">><<spareschoolswimsuit>><<endevent>><<exposure>><</link>>
		<br>
		<<link [[Skip the lesson (0:05)|School Pool Refuse]]>><<pass 5>><<detention 6>><<endevent>><</link>><<gdelinquency>>
	<</if>>
<</if>>