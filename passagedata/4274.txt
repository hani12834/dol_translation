<<set $outside to 0>><<set $location to "town">><<effects>>
<<pass 20>>

Whitney drags you by your hair, forcing you to bend forwards. <<He>> leads you through the school gates and down a nearby alley. <<He>> releases you in front of a <<endevent>><<generate1>><<person1>><<personstop>>

<<if !$worn.upper.type.includes("naked")>>
Without warning, Whitney <<pullsup>> your $worn.upper.name.
<</if>>

"There. I've shown you a school <<girl>> shirtless. Now pay up."
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Whitney")].dom gte 20>>

The <<person>> reaches forward to grope your <<breastsstop>> "No touching!" Whitney says. "That's extra."
<br><br>

"How much extra?"
<br><br>

Whitney pauses, refusing to let you go. "£50."
<br><br>

"Done," the <<person>> says. <<He>> pulls some notes from <<his>> pocket. Whitney shoves you over while taking the money.
<br><br>

<<link [[Next|Bully Whore Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>

The <<person>> reaches forward to grope your <<breastsstop>> "No touching!" Whitney says. "You haven't paid for that." You struggle free.
<br><br>

<<endevent>><<npc Whitney>><<person1>>

You leave the alley as Whitney takes <<his>> money. "Don't be upset," <<he>> shouts at you. "You've been productive for once."
<<npcincr Whitney lust 5>><<glust>><<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<link [[Next|Oxford Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<</if>>