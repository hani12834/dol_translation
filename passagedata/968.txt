<<effects>>

You shuffle out of your $worn.lower.name, and hand it to the <<person>>.
<br><br>
<<lowerstrip>>
<<He>> holds it to <<his>> face and sniffs. "Alright," <<he>> says. "Now get out. Just for a moment. I won't drive away."
<br><br>

<<link [[Get out|Farm Hitchhike Exposed Step]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Refuse|Farm Hitchhike Exposed Refuse 2]]>><</link>><<physiquedifficulty 1 $physiquemax>>
<br>