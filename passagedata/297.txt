<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You open the door and peek through.
<<if $asylumstate is "sleep">>
	The hallway beyond is dark and empty. You tiptoe through.
	<br><br>
	You explore the staff area of the asylum, dodging the occasional patrol. You come to a room filled with screens. Cameras showing the inside of each cell. A couple of patients are masturbating.
	<br><br>
	One screen reads "<span class="red">Aphrodisiac ministration active: 3%</span>" There are many unmarked buttons and dials beneath it.
	<br><br>
	<<link [[Try to turn it up (1:00)|Asylum Cameras]]>><<pass 60>><</link>> <i>A higher science skill will make this more likely to succeed. Will trigger an escape sequence if successful.</i>
	<br>
	<<link [[Leave|Asylum]]>><</link>>
	<br>
<<else>>
	The hallway beyond is busy with staff performing their duties. You'll be seen if you stay here. Perhaps it'll be deserted at night.
	<br><br>
	<<link [[Next|Asylum]]>><</link>>
<</if>>