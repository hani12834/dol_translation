<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You pull the <<person1>><<persons>> arm away from the <<person2>><<personstop>> They both look shocked. "Don't touch me," the <<person1>><<person>> says. "Just who do you think you are?" The <<person2>><<person>> gives you a grateful smile.
<br><br>
<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>