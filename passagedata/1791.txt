<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">><<person1>>

You work through the room, selecting the most valuable things you can easily conceal and carry.
<<if $phase lte 1>>
	As you work, the <<person>> twists and strains against <<his>> bonds in impotent rage - <<his>> <<if $pronoun is "m">>penis swings around<<else>>breasts jiggle around<</if>> and some interesting 'toys' bounce around the bed as <<he>> struggles, giving you some very bad thoughts.
	<<garousal>><<arousal 100>><<set $blackmoney += 120>>
	<br><br>
	Finally, having made your selection of loot you leave, leaving the <<person>> red-faced, sweating and thoroughly enraged on the bed.
	<br><br>
<<else>>
	As you try to work, the <<person>> screams and hurls obscenities, while wishing various forms of violent sexual assault on you. <<He>> twists and strains against <<his>> bonds relentlessly. It makes it difficult for you to focus.
	<<set $blackmoney += 80>><<gstress>>
	<br><br>
	Finally, as you prepare to leave with your loot, the <<personcomma>> sweaty and red-faced, looks at you. <<He>> swallows and starts to speak.

	"Listen. Be a good <<if $player.gender_appearance is "m">>boy.<<else>>girl.<</if>> Before you go, would you- Would you put that gag back in. The gag. Th- The
	<<if $rng % 2>>
		Master will hurt me otherwise."
		<br><br>
		You smile at <<himstop>> "I tried. You didn't let me."
		<br><br>
		"I-I thought I didn't... No. No, please, don't go yet! Please! Put it back, you..."
		<br><br>
		You wave sweetly from the door on the way out. "Bye!"
		<br><br>
		"You don't understand, he-he.. He'll..."
	<<else>>
		Mistress will hurt me otherwise."
		<br><br>
		You smile at <<himstop>> "I tried. You didn't let me."
		<br><br>
		"I-I thought I didn't... No. No, please, don't go yet! Please! Put it back, you..."
		<br><br>
		You wave sweetly from the door on the way out. "Bye!"
		<br><br>
		"You don't understand, she-she.. She'll..."
	<</if>>
	<br><br>
	You can still hear <<his>> pleas as the front door clicks shut.
	<br><br>
	You find yourself smiling.
	<<lstress>><<stress -1>>
	<br><br>
<</if>>
<<link [[Leave|Danube Street]]>><<endevent>><</link>>
<br>