<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>

You run left, and collide with someone. You stumble, but manage to recover. Whoever you collided with isn't so lucky. "Fuck," you hear the <<person2>><<if $pronoun is "m">>man<<else>>woman<</if>> say beneath you. You keep going.
<br><br>

Your footsteps start to echo. You hear other footsteps behind you. You keep running until you feel stale air blast you from the front.
<br><br>

"There's a drop in front of you <<girlstop>>" the <<person1>><<monk>> shouts, fast approaching. "A dead end. Give up."
<br><br>

<<link [[Run Forward|Temple Garden Pit]]>><</link>>
<br>
<<link [[Run Left|Temple Garden Wall]]>><</link>>
<br>
<<link [[Run Right|Temple Garden Escape]]>><</link>>
<br>
<<link [[Give up|Temple Garden Give up]]>><<set $submissive += 1>><</link>>
<br>