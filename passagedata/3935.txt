<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You walk towards Kylar's hiding place. <<He>> peeks out, to find you stood right there. <<He>> freezes on the spot, stares over your shoulder and pretends <<he>> was looking at something else.
<br><br>

<<if $submissive gte 1150>>
"Hey," you say. "Are you okay?"
<<elseif $submissive lte 850>>
"I saw you watching me," you say. "What do you want?"
<<else>>
"I saw you watching," you say. "Don't be frightened."
<</if>>

<<He>> turns and runs.
<br><br>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><<endevent>><</link>>
<br>