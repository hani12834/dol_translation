<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewersworkshop">>
<<set $sewersevent -= 1>>
You are in the old sewers. You are surrounded by metal scrap, twisted into a parody of famous sculptures. There are several tables covered in tools. A chute is built into one of the walls, leading down.
<br><br>
<<if $sewersantiquewatch isnot 1>>
	You hear a faint ticking.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
	<</if>>
	<<if $sewersantiquewatch isnot 1>>
		<<link [[Investigate the ticking|Sewers Ticking]]>><</link>>
		<br><br>
	<</if>>
	<<link [[Tunnel filled with scrap (0:05)|Sewers Scrap]]>><<pass 5>><</link>>
	<br>
	<<link [[Drop down the chute|Sewers Chute]]>><</link>>
	<br><br>
<</if>>