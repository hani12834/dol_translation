<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<set $chef_state to 1>><<set $chef_sus to 0>><<set $bun_value to 5000>><<set $bun_cut to 0.2>>

You nod. Sam claps in excitement. "Fantastic! You can work whenever we're open. I know you'll need to fit it around other commitments. I'll get whatever ingredients you need. This could be the start of something big."
<br><br>
<<He>> turns to walk away, then remembers <<himselfstop>> "Almost forgot. It's the end of your shift. Here's your pay." <<He>> hands you <span class="gold">£10.</span>
<<set $money += 1000>>
<br><br>

<<endevent>>
<<link [[Next|Ocean Breeze]]>><</link>>
<br>