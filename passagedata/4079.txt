<<set $outside to 1>><<set $location to "town">><<effects>>

You refuse the towel, and tell Robin you'll be fine. <<He>> doesn't look convinced.
<<exhibitionism2>>

<<He>> glances up and down the street, nervous. "Let's get home before anyone notices."
<br><br>
The two of you head back to the orphanage. Robin looks around for any sign that people are aware. This draws a lot of attention, but Robin's insistence on hurrying means you make good time.
<br><br>

<<npcincr Robin love 1>><<npcincr Robin lust 1>>
<<endevent>>
<<link [[Next|Orphanage]]>><</link>>