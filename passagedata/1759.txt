<<set $outside to 0>><<set $location to "dance_studio">><<effects>>
<<endevent>>
<<npc Charlie>><<person1>>
<<if $phase is 0>>
	You practise as Charlie demonstrated. It's not long before <<he>> returns.
	<<danceskilluse>>
<<elseif $phase is 1>>
	A <<generate2>><<person2>><<person>> near you loses their balance would have fallen to the floor had you not intervened. You spend some time helping them with their balance. You don't learn much yourself, but <<he>> is clearly grateful, <span class="green">and the class seems pleased with your generosity.</span> Charlie soon returns.
	<<set $dancestudioanger -= 1>>
<<elseif $phase is 2>>
	You flaunt your skill in front of the other students. It feels good to act superior, <span class="pink">but the looks you receive suggest that the other students dislike being shown up like this.</span> Charlie soon returns.
	<<stress -12>><<set $dancestudioanger += 1>>
<</if>>
<br><br>
<<link [[Next|Dancing Lesson]]>><<endevent>><</link>>