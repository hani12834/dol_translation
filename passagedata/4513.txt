<<widget "danceactions">><<nobr>>
	<<exposure>>
	<<if $danceevent is 0>>
		<<if $exposed gte 2 and $exhibitionism lte 74 and $forceddance isnot 1 and ($corruptionDancing is undefined or $dancelocation isnot "brothel")>>
		<<elseif $exposed gte 1 and $exhibitionism lte 34 and $forceddance isnot 1 and ($corruptionDancing is undefined or $dancelocation isnot "brothel")>>
		<<else>>
			<<if $danceactiondefault is "cool">>
				<label>Cool Dance (0:01) <<radiobutton "$danceaction" "cool" checked>></label> |
			<<elseif $danceactiondefault is 0>>
				<label>Cool Dance (0:01) <<radiobutton "$danceaction" "cool" checked>></label> |
			<<else>>
				<label>Cool Dance (0:01) <<radiobutton "$danceaction" "cool">></label> |
			<</if>>
			<br>
			<<if $danceactiondefault is "sophisticated">>
				<label>Sophisticated Dance (0:01) <<radiobutton "$danceaction" "sophisticated" checked>></label> |
			<<else>>
				<label>Sophisticated Dance (0:01) <<radiobutton "$danceaction" "sophisticated">></label> |
			<</if>>
			<br>
			<<if $danceactiondefault is "seductive">>
				<label><span class="meek">Seductive Dance</span> (0:01) <<combatexhibitionist1>> <<radiobutton "$danceaction" "seductive" checked>></label> |
			<<else>>
				<label><span class="meek">Seductive Dance</span> (0:01) <<combatexhibitionist1>> <<radiobutton "$danceaction" "seductive">></label> |
			<</if>>
			<br>
			<<if $exhibitionism gte 15>>
				<<if $danceactiondefault is "sexual">>
					<label><span class="sub">Sexual Dance</span> (0:01) <<combatexhibitionist2>> <<radiobutton "$danceaction" "sexual" checked>></label> |
				<<else>>
					<label><span class="sub">Sexual Dance</span> (0:01) <<combatexhibitionist2>> <<radiobutton "$danceaction" "sexual">></label> |
				<</if>>
				<br>
			<</if>>
			<<goooutsidecount>>
			<<if $cat gte 6 and ($goooutsidecount + $semenoutsidecount) gte 1 and $leftarm isnot "bound" and $rightarm isnot "bound">>
				<<if $danceactiondefault is "groom">>
					<label><span class="sub">Groom</span> (0:01) <span class="blue">Cat</span> <<radiobutton "$danceaction" "groom" checked>></label> |
				<<else>>
					<label><span class="sub">Groom</span> (0:01) <span class="blue">Cat</span> <<radiobutton "$danceaction" "groom">></label> |
				<</if>>
			<</if>>
			<<dancestripactions>>
			<br><br>
			<<link "Continue">>
				<<script>>state.display(state.active.title, null)<</script>>
			<</link>>
			<br>
		<</if>>
	<<elseif $danceevent is "vip">>
		<<link "Allow">><<set $danceaction to "vipallow">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Push away">><<set $danceaction to "vippush">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "strippertrouble">>
		<<link "Help them">><<set $danceaction to "stripperhelp">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Ignore">><<set $danceaction to "stripperignore">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "note">>
		<<link "Approach">><<set $danceaction to "noteapproach">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Ignore">><<set $danceaction to "noteignore">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "rape">>
	<<elseif $danceevent is "molest">>
		<<link "Let them cop a feel">><<script>>state.display(state.active.title, null)<</script>><</link>><<set $danceaction to "cop">>
		<br>
		<<if $dancelocation is "brothel" and $corruptionDancing isnot undefined>>
			The slime in your ear prevents you from ending the show.
		<<elseif $dancelocation isnot "underground">>
			<<link "It's too dangerous, time to end the show">><<set $danceaction to "finish">><<script>>state.display(state.active.title, null)<</script>><</link>>
			<br>
		<</if>>
	<<elseif $danceevent is "skirtstrip">>
		<<link "Struggle for your clothes">><<set $danceaction to "skirtstruggle">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Surrender your clothes">><<set $danceaction to "skirtsurrender">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "lowerstrip">>
		<<link "Struggle for your clothes">><<set $danceaction to "lowerstruggle">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Surrender your clothes">><<set $danceaction to "lowersurrender">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "upperstrip">>
		<<link "Struggle for your clothes">><<set $danceaction to "upperstruggle">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Surrender your clothes">><<set $danceaction to "uppersurrender">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "understrip">>
		<<link "Struggle for your clothes">><<set $danceaction to "understruggle">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
		<<link "Surrender your clothes">><<set $danceaction to "undersurrender">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "private" and $dancelocation isnot "underground">>
		<<link "Ignore">><<set $danceaction to "privaterefuse">><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "leighton">>
		<<link "Ignore">><<set $leightondancerefused to 1>><<script>>state.display(state.active.title, null)<</script>><</link>>
		<br>
	<<elseif $danceevent is "corruptionDance">>
		<<if $promiscuity gte 75 and $submissive gte 1900>>
			<<link "Submit willingly">>
				<<corruption 3>><<pain -8>><<stress -12>><<trauma -24>><<set $submissive += 2>>
				<<set $corruptionResult to "submit">><<set $danceaction to "corruptionResult">>
				<<script>>state.display(state.active.title, null)<</script>>
			<</link>><<promiscuous5>><<ggcorruption>><<llpain>><<llltrauma>><<llstress>>
			<br>
		<</if>>
		<<link "Obey">><<corruption 1>>
			<<pain -4>><<stress -6>><<trauma -12>><<set $submissive += 1>>
			<<set $corruptionResult to "obey">><<set $danceaction to "corruptionResult">>
			<<script>>state.display(state.active.title, null)<</script>>
		<</link>><<gcorruption>><<lpain>><<lltrauma>><<lstress>>
		<br>
		<<link "Defy">><<corruption -1>>
			<<pain 8>><<stress 6>><<trauma 6>><<set $submissive -= 1>>
			<<set $corruptionResult to "defy">><<set $danceaction to "corruptionResult">>
			<<script>>state.display(state.active.title, null)<</script>>
		<</link>><<lcorruption>><<ggpain>><<ggtrauma>><<ggstress>>
		<br>
	<</if>>
<</nobr>><</widget>>