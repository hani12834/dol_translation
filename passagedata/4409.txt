<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<if $sewerschasedstep lte 0>>
	<<npc Morgan>><<person1>>"Oh, you poor little thing. This simply won't do," Morgan says, stepping out of the shadows. <<He>> twists a pin at the base of the shackle, freeing you. "Now <<charlescomma>> don't make things harder on yourself than they need to be." <<He>> grasps your arm and pulls.
	<br><br>
	<<set $sewerschased to 0>>
	<<link [[Go quietly|Sewers Return]]>><</link>>
	<br>
	<<link [[Fight|Sewers Fight]]>><<set $fightstart to 1>><</link>>
	<br>
<<elseif $physique gte random(10000, 20000) and $phase is 0>>
	<<set $phase += 1>>
	You rip the shackle open and run.
	<br><br>
	You hear Morgan screech, and the clang of metal soon after.
	<br><br>
	<<endevent>>
	<<destinationsewers>>
	<<set $sewerschased to 0>>
<<elseif $physique gte random(6000, 18000) and $phase is 1>>
	<<set $phase += 1>>
	You rip the shackle open and run.
	<br><br>
	You hear Morgan screech, and the clang of metal soon after.
	<br><br>
	<<endevent>>
	<<destinationsewers>>
	<<set $sewerschased to 0>>
<<elseif $physique gte random(1, 10000) and $phase gte 2>>
	<<set $phase += 1>>
	You rip the shackle open and run.
	<br><br>
	You hear Morgan screech, and the clang of metal soon after.
	<br><br>
	<<set $sewerschased to 0>>
	<<endevent>>
	<<destinationsewers>>
<<else>>
	You try to rip the shackle off. It gives a little, but remains stuck. Further attempts should be more likely to succeed. <<morganhunt>>
	<br><br>
	<<set $phase += 1>>
	<<link [[Struggle|Sewers Shackle Struggle]]>><</link>>
	<<if $phase is 1>>
		<<physiquedifficulty 6000 16000>>
	<<elseif $phase gte 2>>
		<<physiquedifficulty 1 10000>>
	<<else>>
		<<physiquedifficulty 10000 20000>>
	<</if>>
	<br>
	<<set $skulduggerydifficulty to 600>>
	<<link [[Pick the lock|Sewers Shackle Pick]]>><</link>><<skulduggerydifficulty>>
	<br>
<</if>>