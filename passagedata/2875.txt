<<set $outside to 0>><<effects>><<set $lock to 200>>

The school gate is shut and sealed by a sturdy padlock.
<br><br>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Pick it (0:10)|Oxford Street]]>><<pass 10>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
	<</if>>

<<link [[Leave|School Front Playground]]>><</link>>
<br>