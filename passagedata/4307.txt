<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

Whitney falls to the floor, gasping. <<tearful>> you push past <<his>> bemused friends and away from the toilets.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|School Toilets]]>><</link>>

<<elseif $enemyhealth lte 0>>

You shove Whitney into a cubicle and flee. <<His>> bemused friends let you past.
<br><br>

<<tearful>> you stagger away from the toilets.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|School Toilets]]>><</link>>

<<else>>

You fall to the ground, too hurt to resist.
<br><br>

Whitney's friends surround you and deliver blows with their feet. You curl up to protect yourself, until the bullies grow bored.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<tearful>> you stagger to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

	<<if $bus is "boys">>
		<<link [[Next|School Boy's Toilets]]>><<set $eventskip to 1>><</link>>
	<<else>>
		<<link [[Next|School Girl's Toilets]]>><<set $eventskip to 1>><</link>>
	<</if>>

<</if>>