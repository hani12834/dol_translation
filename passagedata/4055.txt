<<set $outside to 1>><<set $location to "beach">><<effects>>
<<endevent>><<generate1>><<person1>>
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	You enter the shop and start loading up a trolley with large bottles of water. Once done, you pretend to browse and wait. A talkative customer strikes up a conversation with the <<person>> behind the counter, giving you your chance. You push the trolley from the shop.
	<br><br>
	<<endevent>><<npc Robin>><<person1>>
	You keep pushing it all the way back to Robin. <<His>> eyes widen when <<he>> sees you. "I've never thought of that," <<he>> says. "You're so clever." You quietly leave the £2 <<he>> gave you on the counter.
	<<npcincr Robin love 1>><<glove>>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<endevent>>
	<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
	<br>
	<<link [[Leave|Beach]]>><</link>>
	<br>
<<else>>
	You enter the shop and start loading up a trolley with large bottles of water. Once done, you pretend to browse and wait. A talkative customer strikes up a conversation with the <<person>> behind the counter, giving you your chance. You push the trolley from the shop.
	<br><br>
	An arm grabs your shoulder. It's the <<personstop>> "You little shit. I'll teach you to steal from me."
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<link [[Next|Robin's Lemonade Steal Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>