<<set $outside to 0>><<set $location to "hospital">><<effects>>

You awaken on a hard metallic surface in a dark room. Dim lights along the walls switch on, illuminating a large and grimy room, the ceiling lost in the darkness above. Cages line one wall, full of golden eyes that stare out at you. The cage doors shift slightly, and a timer blinks into life "1:00, 0:59, 0:58..." Fighting back panic, you head to the door on the other side of the room, which appears to be the only way out.
<br><br>

Through the door and to the left there is a precipitous drop. You peer down and see dark running water; you can vaguely make out a wide tunnel leading away from the bottom. To the right is a long corridor. A door stands shut at the other end. In front of you is a small hole in the wall, which you might be able to squeeze through.
<br><br>

<<link [[Climb down to the water (0:01)|Abduction Hospital Water]]>><<set $molestationstart to 1>><<pass 1>><</link>>
<br>
<<link [[Head down the corridor (0:01)|Abduction Hospital Corridor]]>><<pass 1>><</link>>
<br>
<<link [[Squeeze through the hole (0:01)|Abduction Hospital Hole]]>><<pass 1>><</link>>
<br>