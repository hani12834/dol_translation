<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $scienceprogression gte 4 and $delinquency gte 1000>>
	Sirris smiles at you. "This part of the lesson is on human anatomy. I need you to remove your clothing, all of it. Don't be embarrassed, this is important for the lesson."
	<br><br>
	<<link [[Refuse|Science Refusal]]>><<detention 1>><</link>><<gdelinquency>>
	<br>
	<<if $uncomfortable.nude is false>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 4>><<npcincr Sirris dom 1>><<scienceskill>><<arousal 600>><</link>><<garousal>><<gscience>>
		<br>
	<<else>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 4>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><</link>><<gstress>><<gscience>>
		<br>
	<</if>>
<<elseif $scienceprogression gte 3 and $delinquency gte 600>>
	Sirris turns to the class. "Okay, can you quiet down please." The class shows <<him>> no acknowledgement. Sirris smiles at you. "This part of the lesson is on human anatomy. I need you to remove your clothing, all of it. Don't be embarrassed, this is important for the lesson."
	<br><br>
	<<link [[Refuse|Science Refusal]]>><<detention 1>><</link>><<gdelinquency>>
	<br>
	<<if $uncomfortable.nude is false>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 3>><<npcincr Sirris dom 1>><<scienceskill>><<arousal 600>><<set $scienceprogression to 4>><</link>><<garousal>><<gscience>>
		<br>
	<<else>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 3>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><<set $scienceprogression to 4>><</link>><<gstress>><<gscience>>
		<br>
	<</if>>
<<elseif $scienceprogression gte 2 and $delinquency gte 400>>
	Sirris smiles at you. "This part of the lesson is on human anatomy. I need you to remove your clothing, all of it. Don't be embarrassed, this is important for the lesson."
	<br><br>
	<<link [[Refuse|Science Refusal]]>><<detention 1>><</link>><<gdelinquency>>
	<br>
	<<if $uncomfortable.nude is false>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 2>><<npcincr Sirris dom 1>><<scienceskill>><<arousal 600>><<set $scienceprogression to 3>><</link>><<garousal>><<gscience>>
		<br>
	<<else>>
		<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 2>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><<set $scienceprogression to 3>><</link>><<gstress>><<gscience>>
		<br>
	<</if>>
<<elseif $scienceprogression gte 1 and $delinquency gte 10>>
	Sirris smiles at you. "I need you to undress for this part. Don't worry, you can keep your underwear on."
	<br>
	<<if !$worn.under_lower.type.includes("naked")>>
		<<link [[Refuse|Science Refusal]]>><<detention 1>><</link>><<gdelinquency>>
		<br>
		<<if $uncomfortable.underwear is false>>
			<<link [[Undress|Science Undress]]>><<upperstrip>><<lowerstrip>><<set $phase to 1>><<npcincr Sirris dom 1>><<scienceskill>><<arousal 600>><<set $scienceprogression to 2>><</link>><<garousal>><<gscience>>
			<br>
		<<else>>
			<<link [[Undress|Science Undress]]>><<upperstrip>><<lowerstrip>><<set $phase to 1>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><<set $scienceprogression to 2>><</link>><<gstress>><<gscience>>
			<br>
		<</if>>
	<<else>>
		<<if $uncomfortable.nude is false>>
			You speak clearly, making sure the whole class hear. "I'm not wearing any."
		<<else>>
			You speak as quietly as possible, making sure only Sirris can hear. "I'm... I'm not wearing any."
			<br>
		<</if>>
		<<if $delinquency gte 400>>
			<<He>> blushes slightly. "That's against school rules you know. I'm still going have to ask you to strip, it's important for the lesson."
			<br><br>
			<<link [[Refuse|Science Refusal]]>><<detention 1>><</link>><<gdelinquency>>
			<br>
			<<if $uncomfortable.nude is false>>
				<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 2>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><<arousal 300>><<set $scienceprogression to 2>><</link>><<garousal>><<gscience>>
				<br>
			<<else>>
				<<link [[Undress|Science Undress]]>><<schoolrep_naked>><<strip>><<set $phase to 2>><<npcincr Sirris dom 1>><<scienceskill>><<stress 1>><<trauma 1>><<set $scienceprogression to 2>><</link>><<gtrauma>><<gstress>><<gscience>>
				<br>
			<</if>>
		<<else>>
			<<He>> blushes slightly and averts <<his>> eyes from you. "Oh. T-take your seat please."
			<<gstress>><<gtrauma>>
			<br><br>
			<<link [[Sit back down|Science Lesson]]>><<stress 1>><<trauma 1>><<endevent>><</link>>
			<br>
			<<if $exhibitionism gte 75>>
				<<link [[Strip anyway|Science Undress]]>><<strip>><<schoolrep_naked>><<set $phase to 5>><<set $scienceprogression to 2>><<scienceskill>><</link>><<gscience>><<exhibitionist5>>
				<br>
			<</if>>
		<</if>>
	<</if>>
<<else>>
	Sirris smiles at you. "Don't worry, we're not going to do anything invasive." <<He>> turns to the class. "Okay, can you quiet down a bit please..."
	<br><br>

	<<He>> talks about various bones and organs, pointing to parts of your body to demonstrate their location. After a short while, <<he>> thanks you and sends you back to your seat.
	<<gscience>><<scienceskill>>
	<br><br>
	<<link [[Next|Science Lesson]]>><<endevent>><<set $scienceprogression to 1>><</link>>
	<br>
<</if>>