<<effects>>

<<if $submissive gte 1150>>
	"S-sorry," you say, afraid of hurting <<his>> feelings. "I'm not in the mood."
<<elseif $submissive lte 850>>
	"Not today," you say. "You shouldn't be so scared to ask."
<<else>>
	"I'm having a good time here," you say.
<</if>>
Kylar nods, but doesn't say anything in response.
<br><br>

<<kylaroptions>>