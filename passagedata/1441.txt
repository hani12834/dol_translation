<<set $outside to 1>><<set $location to "beach">><<effects>>

<<if $submissive gte 1150>>
"But I should be seen," you say. "Don't you think I'm beautiful?" You flare your arms at your sides and twirl. "Everyone should be allowed to look." You skip away.
<<elseif $submissive lte 850>>
You put your hands on your hips and pout. "Or what? You gonna make me?" You turn around, lean forwards and give your <<bottom>> a smack before sauntering away.
<<else>>
"No," you say. "I'm not gonna let you control me. You're welcome to join the fun though." You giggle and skip away.
<</if>>
<br><br>
<<endevent>>

You spend an hour running across the sand and splashing through waves. The sun feels warm and gentle against your bare skin. After a while though, the wind starts to pick up and the cold makes you shiver. You decide to get dressed for now. There's always another day.
<br><br>

<<link [[Next|Beach]]>><<clotheson>><</link>>
<br>