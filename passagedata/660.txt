<<set $outside to 1>><<set $location to "forest">><<effects>>
<<generate1>><<generate2>><<generate3>>
The wolves creep up to the cave. The bear's eyes open, and the wolves lunge at it.
<br><br>

<<if $wolfpackharmony + $wolfpackferocity + 20 gte $rng>>

The bear tries to stand, but it's too late. Several sets of jaws clamp down on it's body. It roars in pain and swipes at the attackers, but they jump back to safety.
<br><br>

Groggy and hurt, the bear stumbles away from the cave and leaves the deer carcass unguarded. The black wolf keeps the choice parts for itself. You find some berries growing nearby which serve you better.
<<stress -12>><<lstress>>
<br><br>

	The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
	<br><br>

<<wolfpackhuntoptions>>

<<else>>
The black wolf lunges, but the bear is too quick. It swipes the black wolf to the side, and the other wolves turn and flee. The black wolf follows, unhurt save for its pride.
<br><br>

<i>Higher harmony and ferocity increases the chance of a successful hunt.</i>
<br><br>

You run with the pack into the forest. There must be easier prey.
<<gathletics>><<athletics 6>><<physique 6>>
<br><br>

<<set $bus to "wolfcaveclearing">>
<<wolfhuntevents>>

<</if>>