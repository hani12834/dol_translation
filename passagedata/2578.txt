<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<set $dorenhonest to 1>>
You tell Doren that you've been attacked numerous times, and that you don't feel able to cope. Saying it out loud makes you feel dizzy. You don't go into details, but make it clear how trapped you feel.
<br><br>
You realise you're sat on the floor, being held up by Doren's arm. You don't remember falling. "It's okay <<lasscomma>> I've got you." <<He>> pulls a phone out of <<his>> pocket and calls someone. "Hello? Yes. I need someone to cover my class. Yes it's an emergency. No. Goodbye." <<He>> puts the phone away.
<br><br>
<<He>> helps you to your feet. "I want to take you somewhere they can help, okay?" <<He>> takes your hand and together you walk away from class. <<He>> leads you outside to <<his>> parked car. "Climb aboard. It's not a long trip."
<br><br>
<<link [[Climb aboard (0:05)|English Events Police]]>><<pass 5>><</link>>
<br>