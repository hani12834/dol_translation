<<set $outside to 0>><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "neck">>
	<<else>>
		<<shopbuy "neck" "reset">>
	<</if>>
<<else>>
	<<shopbuy "neck" "reset">>
<</if>>
<<clothingShop "neck">>
<br>

<<link [[Back to shop|Clothing Shop]]>><<unset $clothes_choice>><</link>>