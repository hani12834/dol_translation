<<widget "actionsorgasm">><<nobr>>

<<if $leftarm is 0>>
	Your left arm is free, but you can't stop the spasms.
	<br>
	<<if $leftactiondefault is "leftfold">>
		| <label><span class="brat">Fold</span> <<radiobutton "$leftaction" "leftfold" checked>></label>
	<<else>>
		| <label><span class="brat">Fold</span> <<radiobutton "$leftaction" "leftfold">></label>
	<</if>>
	<<if $leftactiondefault is "leftgrip">>
		| <label><span class="meek">Grip</span> <<radiobutton "$leftaction" "leftgrip" checked>></label>
	<<else>>
		| <label><span class="meek">Grip</span> <<radiobutton "$leftaction" "leftgrip">></label>
	<</if>>
	<br>
<<elseif $leftarm is "grappled">>
	Your left arm jerks against their grip.
	<br>
<<elseif $leftarm is "bound">>
	Your left arm jerks against its bonds.
	<br>
<</if>>

<<if $rightarm is 0>>
	<br>
	Your right arm is free, but you can't stop the spasms.
	<br>
	<<if $rightactiondefault is "rightfold">>
		| <label><span class="brat">Fold</span> <<radiobutton "$rightaction" "rightfold" checked>></label>
	<<else>>
		| <label><span class="brat">Fold</span> <<radiobutton "$rightaction" "rightfold">></label>
	<</if>>
	<<if $rightactiondefault is "rightgrip">>
		| <label><span class="meek">Grip</span> <<radiobutton "$rightaction" "rightgrip" checked>></label>
	<<else>>
		| <label><span class="meek">Grip</span> <<radiobutton "$rightaction" "rightgrip">></label>
	<</if>>
	<br>
<<elseif $rightarm is "grappled">>
	Your right arm jerks against their grip.
	<br>
<<elseif $rightarm is "bound">>
	Your right arm jerks against its bonds.
	<br>
<</if>>

<<if $mouthuse is 0>>
	<br>
	Your mouth is free, but involuntary moans and cries prevent speaking.
	<br>
	<<if $mouthactiondefault is "stifleorgasm">>
		| <label><span class="brat">Stifle</span> <<radiobutton "$mouthaction" "stifleorgasm" checked>></label>
	<<else>>
		| <label><span class="brat">Stifle</span> <<radiobutton "$mouthaction" "stifleorgasm">></label>
	<</if>>
	<<if $mouthactiondefault is "letoutorgasm">>
		| <label><span class="meek">Let it out</span> <<radiobutton "$mouthaction" "letoutorgasm" checked>></label>
	<<else>>
		| <label><span class="meek">Let it out</span> <<radiobutton "$mouthaction" "letoutorgasm">></label>
	<</if>>
	<br>
<</if>>

<</nobr>><</widget>>