<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<towelup>>

<<if $whitneymaths is "seat">>
	<<npc Whitney>><<person1>>
	You do the bare minimum of work, focusing instead on chatting with Whitney and <<his>> friends.<<npcincr Whitney love 1>>
	<<endevent>>
<<elseif $cool gte ($coolmax / 5 * 2)>>
	You do the bare minimum of work, focusing instead on trying to endear yourself to your classmates. You soon become the centre of attention.
<<elseif $cool gte ($coolmax / 5)>>
	You do the bare minimum of work, focusing instead on trying to endear yourself to your classmates.
<<else>>
	You do the bare minimum of work, focusing instead on trying to endear yourself to your classmates. Most avoid interacting with you.
<</if>>
<br><br>

<<pass 5>><<status 1>><<trauma -1>><<stress -1>>
<<schooleffects>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<trauma -1>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<pass 5>><<stress -1>>
	<<schooleffects>>
<<else>>
	<<set $phase to 1>>
<</if>>
<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
<<else>>
	<<set $phase to 1>>
<</if>>

<<if $phase is 1>>
	<<set $phase to 0>>
	The bell rings, signifying the end of the lesson. You
	<<storeon "mathsClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "mathsClassroom">>
		grab your coat and
	<</if>>
	leave the classroom.
	<br><br>
	<<if $time is ($hour * 60 + 57)>>
		<<pass 3>>
	<<elseif $time is ($hour * 60 + 58)>>
		<<pass 2>>
	<<elseif $time is ($hour * 60 + 59)>>
		<<pass 1>>
	<</if>>

	<<link [[Next|Hallways]]>><</link>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (8900 - ($allure * 2))>>
		<<eventsmaths>>
	<<else>>
		<<eventsmathssafe>>
	<</if>>
<</if>>