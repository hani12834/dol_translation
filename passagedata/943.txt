<<set $outside to 1>><<set $location to "farm">><<effects>>

You walk back down the tunnel. The sheer wall looks imposing, but you find it easier to navigate now that you have clothes and time to spare.
<br><br>

You make it to the top, and squeeze through the entrance, emerging near the meadow.
<br><br>

<<link [[Next|Meadow]]>><</link>>
<br>