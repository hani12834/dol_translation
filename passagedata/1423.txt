<<set $outside to 1>><<set $location to "beach">><<effects>>

<<if $phase is 0>>
	You join the conversation.
	<<if $allure gte 3000>>
		You quickly become the centre of attention.
	<<elseif $allure gte 2000>>
		You draw eyes your way with ease.
	<<elseif $allure gte 1000>>
		People acknowledge you when you speak, but otherwise leave you alone.
	<<else>>
		Dour as you are, people constantly talk over you, as if unaware of your presence.
	<</if>>
	<<if $daystate isnot "night">>
		The sun rises on the horizon and the remaining teenagers head home.
		<br><br>

		<<link [[Next|Beach]]>><<endevent>><</link>>
		<br>
	<<else>>
		<br><br>
		<<set $eventcheck to random(1, 10000)>>
		<<if $eventcheck gte (9900 - ($allure))>>
			<<generatey1>><<person1>>A <<person>> sits next to you and offers you a beverage.
			<br><br>
			<<link [[Drink (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<set $drunk += 60>><<set $phase to 1>><<status 1>><</link>><<gcool>><<lstress>>
			<br>
			<<link [[Just talk (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<set $phase to 1>><<status 1>><</link>><<gcool>><<lstress>>
			<br>
			<<link [[Leave|Beach]]>><<endevent>><</link>>
			<br><br>
		<<else>>
			<<link [[Continue (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<status 1>><</link>><<gcool>><<lstress>>
			<br>
			<<link [[Leave|Beach]]>><<endevent>><</link>>
			<br><br>
		<</if>>
	<</if>>
<<elseif $phase is 1>>
	<<if $rng gte 21>>
		You enjoy talking with the <<personstop>> <<He>> <<admires>> your body when <<he>> thinks you aren't looking.
		<br><br>
		<<link [[Drink (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<set $drunk += 60>><<set $phase to 1>><<status 1>><</link>><<gcool>><<lstress>>
		<br>
		<<link [[Just talk (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<set $phase to 1>><<status 1>><</link>><<gcool>><<lstress>>
		<br>
		<<link [[Leave|Beach]]>><<endevent>><</link>>
		<br><br>
	<<else>>
		You enjoy talking with the <<personstop>> <<He>> leans close and whispers, "Would you like to go somewhere private?"
		<br><br>

		<<link [[Yes|Beach Party Sex]]>><<set $phase to 0>><<set $sexstart to 1>><</link>><<promiscuous1>>
		<br>

		<<link [[No|Beach Party Chat]]>><<set $phase to 2>><<status 1>><</link>><<gcool>>
		<br><br>
	<</if>>
<<elseif $phase is 2>>
	<<if $rng gte 11>><<set $phase to 0>>
		<<He>> is disappointed, but politely says goodbye before leaving to look elsewhere.
		<br><br>
		<<link [[Continue (0:10)|Beach Party Chat]]>><<pass 10>><<stress -2>><<status 1>><<endevent>><</link>><<gcool>><<lstress>>
		<br>
		<<link [[Leave|Beach]]>><<endevent>><</link>>
		<br><br>
	<<else>>
		<<He>> seizes you by the throat, "Well, that isn't very polite of you. I think you need a lesson." <<He>> stands you up and starts forcing you away from the rest of the group.
		<br><br>
		<<link [[Continue|Beach Party Rape]]>><<set $molestationstart to 1>><<set $timer to 10>><</link>>
		<br><br>
	<</if>>
<</if>>