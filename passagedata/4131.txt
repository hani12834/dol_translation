<<if $fightstart is 1>>
<<set $fightstart to 0>>
<<npc Whitney>><<person1>><<maninit>> You tell Whitney you're not going with <<himstop>> "Fine," Whitney says, gesturing at Robin. They push Robin away, leaving you and the bully. "Time to teach you a lesson."
<br><br>
<<set $enemytrust -= 100>>
<<set $enemyanger += 200>>
<<npcidlegenitals>>

<</if>>

<<effects>>
<<effectsman>><<man>>

<<audience>>

<<stateman>>
<br><br>
<<actionsman>>

<<if $enemyhealth lte 0>>
	<span id="next"><<link [[Next|Robin Kiyoura canteen fight Finish]]>><</link>></span><<nexttext>>
<<elseif $enemyarousal gte $enemyarousalmax>>
	<span id="next"><<link [[Next|Robin Kiyoura canteen fight Finish]]>><</link>></span><<nexttext>>
<<elseif $pain gte 100 and $willpowerpain is 0>>
	<span id="next"><<link [[Next|Robin Kiyoura canteen fight Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link [[Next|Robin Kiyoura canteen fight]]>><</link>></span><<nexttext>>
<</if>>