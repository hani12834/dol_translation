<<effects>><<set $location to "sewers">><<set $outside to 0>>
You try to work your $worn.lower.name free from the sticky floor. You make progress, until you're able to free yourself with a tear.
<br><br>
<<set $worn.lower.integrity -= 20>>
<<if $worn.lower.integrity lte 0>>
	You feel a breeze. You look down, and see your clothes still stuck to the floor.
	<br><br>
	<<integritycheck>>
<<else>>
	The struggle left your clothes a little worse for wear.
	<br><br>
<</if>>
<<destinationsewers>>