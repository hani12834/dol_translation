<<set $outside to 0>><<set $location to "forest_shop">><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "upper">>
	<<else>>
		<<shopbuy "upper" "reset">>
	<</if>>
<<else>>
	<<shopbuy "upper" "reset">>
<</if>>
<<clothingShop "upper" "non-outfits">>
<br>

<<link [[Back to shop|Forest Shop]]>><<unset $clothes_choice>><</link>>