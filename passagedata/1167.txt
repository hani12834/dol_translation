<<effects>>
<<unset $farm_phase>>
<<npc Remy>><<person1>>
Remy leads you back to the field. <<He>> stops outside while the farmhands open the gate.
<br><br>

<<if $worn.face.type.includes("gag")>>
	<span class="lblue">Reaching behind your head, <<he>> unties and <span class="lblue">removes your $worn.face.name.</span>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
	<br><br>
<</if>>

<<if $livestock_obey gte 95>>
	"You've been such a good <<girl>>," Remy says. <<He>> holds out <<his>> hand, and a farmhand hands <<him>> a sheet-covered basket. Remy removes the sheet, revealing a number of large, juicy apples.

	<<if $hunger gte 1600>>
		Your heart skips a beat at the sight. You're so hungry, and they look so delicious. Your stomach growls.
	<<else>>
		They look so delicious. Certainly compared to the grass you've had to tolerate. Your stomach growls.
	<</if>>
	<br><br>

	"These are for you," Remy says, handing you the entire basket. <<if $cow gte 6 or $leftarm is "bound" and $rightarm is "bound">>You take the handle in your mouth.<</if>> "Good <<girls>> get treats. Enjoy."
	<br><br>

	<<if $cow gte 6 and $worn.neck.name isnot "cow bell">>
		<<if $worn.neck.name isnot "naked">>
			Just as you're about to crawl away, Remy holds you by your shoulders and removes the $worn.neck.name,
			"One other thing before I go. I got you a new bell, for being such a good <<girl>>," <<he>> says while securing it around your neck.
			<<set $worn.neck.type.push("broken")>>
			<<neckruined>>
		<<else>>
			Just as you're about to crawl away, Remy holds you by your shoulders and secures something around your neck.
			"One other thing before I go. I got you a new bell, for being such a good <<girl>>," <<he>> says before walking away.
		<</if>>
		You give your head a small shake, and it rings. The sound is oddly soothing.
		<<neckwear 8>>
		<br><br>
	<</if>>

	The gate shuts behind you.
	<br><br>

	<<link [[Eat the apples|Livestock Job Eat]]>><<stress -6>><<trauma -6>><<hunger -2000>><</link>><<lllhunger>><<ltrauma>><<lstress>>
	<br>
	<<link [[Save them for someone else|Livestock Job Save]]>><</link>>
	<br>
<<elseif $livestock_obey gte 51>>
	<<if $cow gte 6>>
		<<transform cow 1>><<npcincr Remy love 1>>
		"You've been a good <<girl>>," Remy says. <<He>> pulls a shiny red apple from <<his>> pocket. "This is for you." <<He>> holds it toward you.
		<br><br>

		You lean forward and take a bite. It's so sweet and delicious. The juice runs down your chin. Remy turns the fruit for you, and you lean forward again, and again, until just the core is left. Remy holds out <<his>> fingers, and you lick the juice off.
		<<lhunger>><<hunger -6>>
		<br><br>

		"There's more where that came from," Remy says as <<he>> ruffles your hair. "As long as you're a good <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>>." <<He>> pulls you into the field.
		<br><br>

		<<if $worn.neck.name isnot "cow bell">>
			<<if $worn.neck.name isnot "naked">>
				Just as you're about to crawl away, Remy holds you by your shoulders and removes the $worn.neck.name.
				"One other thing before I go. I got you a new bell, for being such a good <<girl>>," <<he>> says while securing it around your neck.
				<<set $worn.neck.type.push("broken")>>
				<<neckruined>>
			<<else>>
				Just as you're about to crawl away, Remy holds you by your shoulders and secures something around your neck.
				"One other thing before I go. I got you a new bell, for being such a good <<girl>>," <<he>> says before walking away.
			<</if>>
			You give your head a small shake, and it rings. The sound is oddly soothing.
			<<neckwear 8>>
			<br><br>
		<</if>>

		You lick fruit off your chin as the gate clanks shut behind you.
		<br><br>

		<<link [[Next|Livestock Field]]>><<endevent>><</link>>
		<br>

	<<elseif $leftarm is "bound" or $rightarm is "bound">>
		"You've been a good <<girl>>," Remy says. <<He>> pulls a shiny red apple from <<his>> pocket. "This is for you." <<He>> holds it towards you. Your arms are bound, so you can't reach for it. "I'll hold it while you eat," Remy says, as if reading your mind. "<<if $player.gender_appearance is "m">>Bulls<<else>>Cows<</if>> don't have hands."
		<br><br>
		<<link [[Eat|Livestock Job Eat One]]>><<npcincr Remy love 1>><<hunger -400>><<livestock_obey 1>><<transform cow 1>><</link>><<gobey>><<lhunger>>
		<br>
		<<link [[Refuse|Livestock Job Eat Refuse]]>><<livestock_obey -1>><</link>><<lobey>>
		<br>
	<<else>>
		"You've been a good <<girl>>," Remy says. <<He>> pulls a shiny red apple from <<his>> pocket. "This is for you." <<He>> holds it toward you, but when you reach out <<he>> pulls it back. "No no," <<he>> says. "<<if $player.gender_appearance is "m">>Bulls<<else>>Cows<</if>> don't have hands. I'll hold it for you as you eat."
		<br><br>

		<<link [[Eat|Livestock Job Eat One]]>><<npcincr Remy love 1>><<hunger -400>><<livestock_obey 1>><<transform cow 1>><</link>><<gobey>><<lhunger>>
		<br>
		<<link [[Refuse|Livestock Job Eat Refuse]]>><<livestock_obey -1>><</link>><<lobey>>
		<br>
	<</if>>
<<elseif $livestock_obey gte 21>>
	"You're a temperamental one," Remy says. <<He>> pulls a shiny red apple from <<his>> pocket. "This could be yours someday. If you behave yourself." <<He>> takes a bite.

	<<if $hunger gte 1600>>
		It looks so juicy. Your deprived stomach growls.
	<<else>>
		It looks so sweet, so tasty compared to the alternative.
	<</if>>
	<br><br>

	Remy pulls you the rest of the way into the field. The gate clanks shut behind you.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>
<<else>>
	"You're a temperamental one," Remy says, pulling your leash. "But you'll break. They always do." <<He>> shoves you through the gate. It clanks shut behind you.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>
<</if>>