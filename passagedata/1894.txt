<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

<<if $phase2 is 0>>

	<<if random(1, 2) is 2>>
	<<set $dockquizhome += 1>>

	You let your colleagues answer the questions. They write them down, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	"That's a point to the dockers," The owner says. You cheer with your colleagues.
	<<lstress>><<stress -1>>
	<br><br>

	<<else>>

	You let your colleagues answer the questions. They write them down, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	The owner reads your paper. "The dockers score zero." You grumble with your colleagues.
	<br><br>

	<</if>>

<<elseif $phase2 is 1>>

	<<if $english gte random(1, 1000)>>
	<<set $dockquizhome += 1>>

	You write down the answers, fold the paper and hand it to the pub owner. The sailors do likewise.
	<br><br>

	"That's a point to the dockers," The owner says. You cheer with your colleagues.
	<<lstress>><<stress -1>>
	<br><br>

	<<else>>

	<<if $english lte 300>>You're not confident, but you write down the answers, fold the paper and hand it to the pub owner.<<else>>You write down the answers, fold the paper and hand it to the pub owner.<</if>> The sailors do likewise.
	<br><br>

	The owner reads your paper. "The dockers score zero." You grumble with your colleagues. <<if $english gte 700>>You were sure you got it right.<</if>>
	<br><br>

	<</if>>

<<else>>

You convince your colleagues that you know your stuff, and write down nonsense answers. You fold the paper and hand it to the pub owner.
<br><br>

The owner reads your paper. "The dockers score zero." You grumble with your colleagues. What a surprise.
<br><br>

<</if>>
The owner reads the sailor's answers.
<<if random(1, 2) is 2>>
<<set $dockquizaway += 1>>
"That's a point to the sailors." A cheer erupts from their table.
<br><br>
<<else>>
"Sorry guys. No points." They groan.
<br><br>

<</if>>

<<link [[Next|Docks Pub Crawl Quiz]]>><</link>>
<br>