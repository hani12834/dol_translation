<<set $outside to 0>><<set $location to "strip_club">><<effects>>

<<if $phase is 0>>

You fulfil Darryl's order, but otherwise leave <<him>> be. After a short while <<he>> wordlessly stands and leaves you a tip.
<<tipreceive>>
<br><br>

<<elseif $phase is 1>>
<<npcincr Darryl love 2>>
You gently enquire about what is wrong. <<if $NPCName[$NPCNameList.indexOf("Darryl")].love lte 30>>Darryl seems surprised when <<he>> looks up at you, before looking back at <<his>> drink. "It's nothing. Thank you for asking though. Sorry, I need to get back to work." <<else>>Darryl smiles, but you see a sadness in <<his>> eyes. "I'm fine, just something I have to deal with. Thank you for asking though."<</if>> <<He>> stands up to leave.
<</if>>

<<tipreceive>>
<br><br>

<<if $rng gte 81>>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>

<<else>>

Before <<he>> can move away however, the patron sat beside <<him>> grabs <<him>> by the waist and begins running <<endevent>><<generate1>><<person1>> <<his>> hands all over Darryl's body.
<br><br>

You look to security, but they are busy dealing with a brawl on the other side of the club. Darryl is clearly distraught, but makes no attempt to resist, merely whispering, "Please, no."
<br><br>

<<link [[Do nothing|Strip Club Bartending Daryll Molest]]>><</link>>
<br>
	<<if $promiscuity gte 35>>
<<link [[Seduce the assailant to save Darryl|Strip Club Bartending Molestation]]>><<set $timer to 10>><<set $molestationstart to 1>><</link>><<promiscuous3>>
<br>
	<</if>>
<<link [[Hit the assailant over the head to save Darryl|Strip Club Bartending Molestation]]>><<set $timer to 10>><<set $phase to 1>><<attackstat>><<set $molestationstart to 1>><</link>>

<</if>>