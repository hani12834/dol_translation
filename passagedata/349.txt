<<set $outside to 1>><<set $location to "cabin">><<effects>>

You press your hand against <<his>> face and push <<him>> away.

<<if $NPCName[$NPCNameList.indexOf("Eden")].love - $NPCName[$NPCNameList.indexOf("Eden")].lust gte 1>>

"You wanna be like that?" <<he>> says. "Fine. Don't forget who looks after you though. You should show more respect." <<He>> swims to the shore and climbs out. <<He>> doesn't bother dressing before walking back to the cabin.
<br><br>

<<link [[Stay in the spring|Clearing Spring]]>><<endevent>><<pass 30>><<stress -6>><<set $phase to 1>><</link>><<lstress>>
<br>
<<link [[Get out|Eden Clearing]]>><<clotheson>><</link>>

<<else>>

<<He>> swats your arm away and pulls you into <<himstop>>
<br><br>

<<link [[Next|Clearing Spring Sex]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>