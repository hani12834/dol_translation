<<if $enemyhealth lte 0>>

	You push <<him>> to the ground, giving you a chance to escape. <<tearful>> you shove <<his>> friends aside and flee to safety.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<elseif $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>

	"You... stupid..." <<he>> pants, leaning against a locker. <<His>> friends are so bemused by <<his>> state that they don't notice you slip away. <<tearful>> you escape to safety.<<llust>><<npcincr Whitney lust -20>>
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<elseif $alarm is 1 and $rescue is 1>>

	Your cry echoes down the corridor. "Crybaby," Whitney says. "We'll get you next time." <<He>> and <<his>> friends leave. <<tearful>> you struggle to your feet.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<else>>

	You're too battered and bruised to resist.
	<br><br>
	<<clotheson>>
	<<endcombat>>

	<<npc Whitney>><<person1>>

	<<link [[Next|Bully Panties]]>><</link>>

<</if>>