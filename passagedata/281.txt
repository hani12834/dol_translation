<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You are in your cell in the asylum. The walls and floor are padded. There's a window far above.
<<if $asylumstate is "sleep">>
	The door is locked. You're supposed to be in bed.
<<elseif $hour is 6>>
	An alarm buzzes through the building.
<</if>>
<br><br>
<<roomoptions>>
<<if $hour isnot 6>>
	<<link [[Bed|Asylum Bed]]>><</link>>
	<br><br>
<<else>>
	You can't sleep until the alarm turns off.
	<br><br>
<</if>>
<<if $leftarm is "bound" and $rightarm is "bound">>
	A padded cupboard containing your clothes protrudes from the wall, but you can't reach it with your arms bound.
	<br>
<<else>>
	A padded cupboard containing your clothes protrudes from the wall.
	<br>
	<<link [[Open|Asylum Wardrobe]]>><</link>>
	<br>
<</if>>
<br>
<<if $asylumstate isnot "sleep">>
	<<link [[Leave|Asylum]]>><</link>>
	<br>
<</if>>
<br>
<<link [[Settings|Asylum Settings]]>><</link>>
<br><br>