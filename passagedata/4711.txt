<<set $lock to 300>>

The door is locked tight.
<br><br>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Break in (0:05)|Elk Compound Interior]]>><<pass 5>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
	<</if>>

You successfully pick the lock and enter the building.
<<set $compoundcentre to 1>><<set $compoundalarm += 1>>

<<if $skulduggery lt 400>>
<<skulduggeryskilluse>>
<<else>>
<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>