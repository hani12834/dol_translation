<<effects>>

You sit at the kitchen table with Alex's <<if $pronoun is "m">>father<<else>>mother<</if>>. "Surprised a townie can be so helpful-"
<br>
"<<if $pronoun is "m">>Dad<<else>>Mum<</if>>-" Alex interrupts.
<br>
"More surprised by how cute you are-"
<br>
"<<if $pronoun is "m">>Dad!<<else>>Mum!<</if>>" Alex says again, slamming a mug of tea in front of the older <<if $pronoun is "m">>man<<else>>woman<</if>>."
<br>
"Sorry," <<he>> chuckles. "Thanks for the tea."
<br><br>

<<link [[Next|Farm Stage 6 6]]>><</link>>
<br>