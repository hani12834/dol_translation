<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

	<<beastejaculation>>
	<<npc Remy>><<person1>>
	The audience hoot with laughter as the centaur backs away from you.
	<br><br>
	"Good boy," Remy says, leaning down and patting his neck. <<He>> looks down at you. "Take it back to the others. Let's hope it's smart enough to understand the lesson."
	<br><br>

	The farmhands release your shoulders and pull you to your feet. <<tearful>> you're pushed along the small lane to the field. The gate clanks shut behind you.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Livestock Field]]>><</link>>

<<else>>
	<<npc Remy>><<person1>>
	The centaur backs away from you. "I-I'm just not in the mood," he stutters, trying to save face. He flinches as his master grasps his hair.
	<br><br>

	"Fine," Remy says, looking down at you. "Next time you might come off a lot worse. Let's hope you're smart enough to understand the lesson."

	The farmhands release your shoulders and pull you to your feet. <<tearful>> you're pushed along the small lane to the field. The gate clanks shut behind you.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Livestock Field]]>><</link>>

<</if>>