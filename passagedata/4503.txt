<<set $outside to 0>><<set $location to "underground">><<effects>>

You pick at the loose stone beneath the mattress.
<<set $undergroundescape += 1>>
<<if $undergroundescape gte 19>>
	It'll be a squeeze, but you think you can fit.
	<br><br>
	<<link [[Escape|Underground Escape]]>><</link>>
	
<<elseif $undergroundescape gte 18 and $physiquesize lte 12000>>
	It'll be a squeeze, but you think you can fit.
	<br><br>
	<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
	
<<elseif $undergroundescape gte 17 and $physiquesize lte 10000>>
	It'll be a squeeze, but you think you can fit.
	<br><br>
	<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
	
<<elseif $undergroundescape gte 16 and $physiquesize lte 6000>>
	It'll be a squeeze, but you think you can fit.
	<br><br>
	<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
	
<<elseif $undergroundescape gte 15>>
	The hole needs to be just a bit bigger and you'll be able to fit through.
	
<<elseif $undergroundescape gte 12>>
	There's definitely a way out here, it just needs to be bigger.
	
<<elseif $undergroundescape gte 8>>
	The hole grows wider.
	
<<elseif $undergroundescape gte 4>>
	Some of the cracks grow into a small hole.
	
<<else>>
	You make the cracks a bit bigger.
	
<</if>>
<br><br>
<<link [[Stop|Underground Cell]]>><</link>>
<br>