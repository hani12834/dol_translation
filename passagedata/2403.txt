<<set $outside to 1>><<set $location to "park">><<effects>><<set $bus to "park">>
<<if $submissive gte 1150>>
	"I-it's for my science project," you say.
<<elseif $submissive lte 850>>
	"I need it to win the science fair," you say.
<<else>>
	"I need lichen for my science project," you say.
<</if>>
<br><br>
"Is that right?" <<he>> says. "Looks like you're defacing a statue to me." <<He>> raises <<his>> baton and rubs <<his>> palm with it.
<br><br>
"<<person1>>The <<girl>>'s doing no such thing," a <<if $pronoun is "m">>man's<<else>>woman's<</if>> voice says from behind you. <<He>> walks around the statue and glares at the <<person2>><<personstop>> "And you know it."
<br><br>
The <<person>> seems less confident. "This your <<girl>> then? Tell <<phim>> to get down from there." <<He>> sheathes <<his>> baton and turns away.
<br><br>
<<person1>>The newcomer offers an arm and helps you climb down. "Did I hear you right," <<he>> says. "A scientist in the making?" <<He>> smiles. "I'm Avery. I bet you're thirsty after that. Would you like to get a drink? I know this cute little place."
<br><br>
<<link [[Accept|Park Lichen Accept]]>><</link>>
<br>
<<link [[Refuse|Park Lichen Refuse]]>><</link>>
<br>