<<set $outside to 1>><<set $location to "beach">><<effects>>

<<if $robinlemonadeintro isnot 1>><<set $robinlemonadeintro to 1>>
<<npc Robin>><<person1>>You see Robin stood behind <<his>> lemonade stand. <<He>> waves when <<he>> sees you.
<br><br>

"Hey," <<he>> says as you draw close. <<He>> beams. "It's nice to see you. I want you to try my lemonade." <<He>> mixes together water, lemon juice and sugar and hands you the glass. "On the house." It's very sweet.
<br><br>

<<else>>
<<npc Robin>><<person1>>You see Robin stood behind <<his>> lemonade stand. <<He>> waves when <<he>> sees you.
<br><br>

<</if>>
<<endevent>>
<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
<br>
<<link [[Leave|Beach]]>><<endevent>><</link>>
<br>