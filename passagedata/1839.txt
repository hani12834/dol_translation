<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

The <<person1>><<person>> gets closer. You press one leg into the side of the boat, making the vessel rock. They stumble forwards. "Don't you da-" <<he>> manages before you give the boat another shove, this time turning it over and sending all three of you overboard.
<br><br>

You swim back to shore. "<<pShe>> capsized it on purpose!" the <<person>> says to the amused dockers who help you from the water. Most think it was an accident, but some believe the pair.
<br><br>
<<set $upperwet to 200>><<set $lowerwet to 200>><<set $underlowerwet to 200>><<set $underupperwet to 200>>
<<endevent>>
<<dockoptions>>